<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        <div class="banners banners-1 m-b-20">
            <div class="owl-carousel carousel-1">
                <div class="item"><a href=""><img alt="img" src="img/banner-f-1.jpg" class=""></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-2.jpg" class=""></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-3.jpg" class=""></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-4.jpg" class=""></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-5.jpg" class=""></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-6.jpg" class=""></a>
                </div>
            </div>
        </div>
        <div class="offers white m-b-20">
            <ul class="idTabs tab-offer">
                <li>
                    <a href="#idTab1">
                        <div><i class="material-icons orange-text">fingerprint</i>Popular</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab2">
                        <div><i class="material-icons blue-text">laptop</i>Electronics</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab3">
                        <div><i class="material-icons red-text">wc</i>Fashion</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab4">
                        <div><i class="material-icons green-text">directions_run</i>Health & Beauty</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab5">
                        <div><i class="material-icons yellow-text">restaurant_menu</i>Dining</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab6">
                        <div><i class="material-icons mag-text">shopping_cart</i>Groceries</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab7">
                        <div><i class="material-icons cyan-text">home</i>Home & Living</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab8">
                        <div><i class="material-icons purple-text">place</i>Travel</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab9">
                        <div><i class="material-icons orange-text">directions_bike</i>Sports & Outdoors</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab10">
                        <div><i class="material-icons cyan-dark-text">directions_car</i>Automotive</div>
                    </a>
                </li>
            </ul>
            <div class="tabsbody">
                <div id="idTab1">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab2" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab3" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab4" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab5" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab6" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab7" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab8" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab9" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="idTab10" style="display:none;">
                    <div class="row no-gutters row-coupons">
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-9.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-10.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-11.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-12.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-7.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="col text-center ">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-8.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-16 green-text">10 coupons</div>
                            </a>
                        </div>
                    </div>
                </div>
                <a href="" class="view-all">View all stores</a> </div>
        </div>
        <section class="top-deals box m-b-20">
            <div class="d-flex justify-content-between align-items-center m-b-10">
                <h2 class="f-18 f-c w-400 m-0">Top Deals</h2>
                <div><a href="#" class="btn orange btn-sm f-12">View All</a>
                </div>
            </div>
            <div class="carousel-2-container">
            <div class="owl-carousel carousel-2">
                <div class="item item-deal">
                    <div class="deal-item">
                        <a href="#" class="deal-logo"><img alt="img" src="img/l-1.png">
                        </a>
                        <figure> <img alt="img" src="img/bg-1.png">
                            <figcaption> RM95 for PETRONAS Gift Card (Reloadable) worth RM100 </figcaption>
                        </figure>
                        <div class="deal-item-info">
                            <div class="time">Limited Time Only</div>
                            <div class="deal-code">
                                <div class="no-code">No Code required</div>
                            </div>
                            <a href="" class="btn btn-u green">Up To 18.0% Cashback</a>
                            <div class="old-deal">Was up to 0.75% Cashback</div>
                            <a href="" class="all-deals">View All Lazada Deals</a>
                        </div>
                    </div>
                </div>
                <div class="item item-deal">
                    <div class="deal-item">
                        <a href="#" class="deal-logo"><img alt="img" src="img/l-1.png">
                        </a>
                        <figure> <img alt="img" src="img/bg-1.png">
                            <figcaption> RM95 for PETRONAS Gift Card (Reloadable) worth RM100 </figcaption>
                        </figure>
                        <div class="deal-item-info">
                            <div class="time">Limited Time Only</div>
                            <div class="deal-code">
                                <div class="copy-code">
                                    <div class="code-cont" id="ccode1">COUPONCODE</div>
                                    <button class="code-copy-action btn blue" data-clipboard-target="#ccode1">Copy</button>
                                </div>
                            </div>
                            <a href="" class="btn btn-u green">Up To 18.0% Cashback</a>
                            <div class="old-deal">Was up to 0.75% Cashback</div>
                            <a href="" class="all-deals">View All Lazada Deals</a>
                        </div>
                    </div>
                </div>
                <div class="item item-deal">
                    <div class="deal-item">
                        <a href="#" class="deal-logo"><img alt="img" src="img/l-1.png">
                        </a>
                        <figure> <img alt="img" src="img/bg-1.png">
                            <figcaption> RM95 for PETRONAS Gift Card (Reloadable) worth RM100 </figcaption>
                        </figure>
                        <div class="deal-item-info">
                            <div class="time">Limited Time Only</div>
                            <div class="deal-code">
                                <div class="no-code">No Code required</div>
                            </div>
                            <a href="" class="btn btn-u green">Up To 18.0% Cashback</a>
                            <div class="old-deal">Was up to 0.75% Cashback</div>
                            <a href="" class="all-deals">View All Lazada Deals</a>
                        </div>
                    </div>
                </div>
                <div class="item item-deal">
                    <div class="deal-item">
                        <a href="#" class="deal-logo"><img alt="img" src="img/l-1.png">
                        </a>
                        <figure> <img alt="img" src="img/bg-1.png">
                            <figcaption> RM95 for PETRONAS Gift Card (Reloadable) worth RM100 </figcaption>
                        </figure>
                        <div class="deal-item-info">
                            <div class="time">Limited Time Only</div>
                            <div class="deal-code">
                                <div class="no-code">No Code required</div>
                            </div>
                            <a href="" class="btn btn-u green">Up To 18.0% Cashback</a>
                            <div class="old-deal">Was up to 0.75% Cashback</div>
                            <a href="" class="all-deals">View All Lazada Deals</a>
                        </div>
                    </div>
                </div>
                <div class="item item-deal">
                    <div class="deal-item">
                        <a href="#" class="deal-logo"><img alt="img" src="img/l-1.png">
                        </a>
                        <figure> <img alt="img" src="img/bg-1.png">
                            <figcaption> RM95 for PETRONAS Gift Card (Reloadable) worth RM100 </figcaption>
                        </figure>
                        <div class="deal-item-info">
                            <div class="time">Limited Time Only</div>
                            <div class="deal-code">
                                <div class="no-code">No Code required</div>
                            </div>
                            <a href="" class="btn btn-u green">Up To 18.0% Cashback</a>
                            <div class="old-deal">Was up to 0.75% Cashback</div>
                            <a href="" class="all-deals">View All Lazada Deals</a>
                        </div>
                    </div>
                </div>
                <div class="item item-deal">
                    <div class="deal-item">
                        <a href="#" class="deal-logo"><img alt="img" src="img/l-1.png">
                        </a>
                        <figure> <img alt="img" src="img/bg-1.png">
                            <figcaption> RM95 for PETRONAS Gift Card (Reloadable) worth RM100 </figcaption>
                        </figure>
                        <div class="deal-item-info">
                            <div class="time">Limited Time Only</div>
                            <div class="deal-code">
                                <div class="no-code">No Code required</div>
                            </div>
                            <a href="" class="btn btn-u green">Up To 18.0% Cashback</a>
                            <div class="old-deal">Was up to 0.75% Cashback</div>
                            <a href="" class="all-deals">View All Lazada Deals</a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <section class="deal-of-month row no-gutters m-b-20">
            <div class="deal-title text-center">
                <div>
                    <h3 class="f-32 f-c w-400">Deals of the <br>
                    December</h3>
                    <div class="time red-text f-20"><i class="material-icons f-24">watch_later</i> <span id="cdtimer">19 : 52 : 38</span>
                    </div>
                    <a href="" class="btn blue f-20">View all</a>
                </div>
            </div>
            <div class="deal-month-carousel col">
                <div class="owl-carousel carousel-3">
                    <?php for($i=0;$i<20;$i++){ ?>
                    <div class="item ">
                        <a href="" class="monthly-deal-item">
                            <figure><img alt="img" src="img/i-1.png">
                            </figure>
                            <div class="f-14 w-700 m-b-5">Laptop Acessories</div>
                            <div class="f-16 blue-text m-b-5">10% Off - From $199</div>
                            <div class="f-14 grey-text">Software, USB Gadgets </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <script>
                    function pad( d ) {
                        return ( d < 10 ) ? '0' + d.toString() : d.toString();
                    }
                    // Set the date we're counting down to
                    var countDownDate = new Date( "Dec 15, 2018 15:37:25" ).getTime();

                    // Update the count down every 1 second
                    var x = setInterval( function () {

                        // Get todays date and time
                        var now = new Date().getTime();

                        // Find the distance between now an the count down date
                        var distance = countDownDate - now;

                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor( distance / ( 1000 * 60 * 60 * 24 ) );
                        var hours = Math.floor( ( distance % ( 1000 * 60 * 60 * 24 ) ) / ( 1000 * 60 * 60 ) );
                        var minutes = Math.floor( ( distance % ( 1000 * 60 * 60 ) ) / ( 1000 * 60 ) );
                        var seconds = Math.floor( ( distance % ( 1000 * 60 ) ) / 1000 );

                        // Display the result in the element with id="demo"
                        document.getElementById( "cdtimer" ).innerHTML = pad( hours ) + " : " +
                            pad( minutes ) + " : " + pad( seconds );

                        // If the count down is finished, write some text 
                        if ( distance < 0 ) {
                            clearInterval( x );
                            document.getElementById( "demo" ).innerHTML = "EXPIRED";
                        }
                    }, 1000 );
                </script>
            </div>
            <a href="" class="col-auto col-banner">
                <div class="col-banner-inner pad-5"><img alt="img" src="img/038189.jpg" class="img-fluid">
                </div>
            </a>
        </section>
        <div class="banner-shop m-b-20">
            <div class="owl-carousel carousel-b">
                <div class="item"> <a href=""><img alt="img" src="img/banner-f-5.jpg" class="img-full"></a> </div>
                <div class="item"> <a href=""><img alt="img" src="img/banner-f-6.jpg" class="img-full"></a> </div>
                <div class="item"> <a href=""><img alt="img" src="img/banner-f-4.jpg" class="img-full"></a> </div>
                <div class="item"> <a href=""><img alt="img" src="img/banner-f-3.jpg" class="img-full"></a> </div>
                <div class="item"> <a href=""><img alt="img" src="img/banner-f-2.jpg" class="img-full"></a> </div>
                <div class="item"> <a href=""><img alt="img" src="img/banner-f-1.jpg" class="img-full"></a> </div>
            </div>
        </div>
        <section class="featured-brands box m-b-20">
            <h2 class="f-18 f-c w-400 m-t-0 m-b-5">Featured Brands</h2>
            <div class="owl-carousel carousel-4">
                <div class="item"><a href=""><img alt="img" src="img/banner-f-1.jpg" class="img-full"></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-2.jpg" class="img-full"></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-3.jpg" class="img-full"></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-4.jpg" class="img-full"></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-5.jpg" class="img-full"></a>
                </div>
                <div class="item"><a href=""><img alt="img" src="img/banner-f-6.jpg" class="img-full"></a>
                </div>
            </div>
        </section>

        <section class="offers-smartphone m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Top Selling<br>
                        Mobiles</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-5 nav-min">
                        <?php for($i=0;$i<20;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5 line-1">Apple iPhone 6s (32 GB) some example of quite long text here</div>
                                <div class="f-16 blue-text m-b-5 line-1"> The front camera is a 5MP unit with f/2.2 aperture and two field of view modes</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode0<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode0<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="offers-1 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Super Savers</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode1<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode1<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-2 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Discounts for You</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode2<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode2<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-3 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Trending Offers</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode3<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode3<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-4 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Big Brands on <br>Bigger Discounts</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode4<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode4<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-5 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Household & Home Improvement</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode5<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode5<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-6 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Minimum 70% Off</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode6<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode6<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-7 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Offers for You</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode7<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode7<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-8 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Fashion Essentials</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode8<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode8<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Home Audio</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode9<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode9<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Fulfilled by Flipkart</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode10<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode10<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Headphones & Speakers</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode11<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode11<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Great Mattress Collection</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode12<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode12<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Deals on Kitchen Appliances</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode13<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode13<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Best Sellers</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode14<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode14<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Deals on Small Appliances</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode15<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode15<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-9 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Best of Laptop Accessories</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode16<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode16<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="offers-0 m-b-20">
            <div class="d-flex">
                <div class="deal-title text-center">
                    <div>
                        <h3 class="f-32 f-c w-400">Recently <br>
                        Viewed</h3>
                        <a href="" class="btn blue f-20">View all</a> </div>
                </div>
                <div class="deal-carousel">
                    <div class="owl-carousel carousel-6 nav-min">
                        <?php for($i=0;$i<9;$i++){ ?>
                        <div class="item">
                            <div class="monthly-deal-item">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                                <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                <div class="coupon-info">
                                    <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text">info</i></a> </div>
                                    <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode17<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode17<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                    <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
     <?php include('includes/lang-list.php'); ?>
     <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>