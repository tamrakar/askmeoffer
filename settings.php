<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Settings</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab d-flex xs-tab-2">
                <li><a href="">Account Summary</a></li>
                <li><a href="">Activity</a></li>
                <li><a href="">Payments</a></li>
                <li><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li class="current"><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                <div class="row">
                <div class="col-md-auto">
                    <ul class="list-bordered border user-side-menu">
                        <li><a href="settings.php" class="current">General</a></li>
                        <li><a href="settings-security.php">Reset password</a></li>
                        <li><a href="settings-payments.php">Payment</a></li>
                        <li><a href="settings-favourites.php">Favourites</a></li>
                        <li><a href="settings-instore.php">In-store</a></li>
                        <li><a href="settings-thirdpartyapps.php">Third-party apps</a></li>
                    </ul>
                </div>
                <div class="col-md">
                   <div class="m-l-20 sm-m-l-0 sm-m-t-20">
                    <h1 class="f-22 blue-text m-t-0">General account settings</h1>
                        <div class="settings-list">
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                               
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Name</strong>
                                </div>
                                <div class="col-md">Ashish Tamrakar
                                    <div class="details collapse" id="stng-1">
                                        <div class="m-t-20">
                                            <div class="m-b-10"><input type="text" class="input" size="50" value="Ashish">
                                            </div>
                                            <div class="m-b-10"><input type="text" class="input" size="50" value="Tamrakar">
                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-1" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-1" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Email</strong>
                                </div>
                                <div class="col-md">asis.mailme@gmail.com
                                    <div class="details collapse" id="stng-2">
                                        <div class="m-t-20">
                                            <div class="m-b-10"><input type="email" class="input" size="50" placeholder="new email">
                                            </div>
                                            <div class="m-b-10"><input type="email" class="input" size="50" placeholder="confirm email">
                                            </div>
                                            <div class="m-b-10"><input type="password" class="input" size="50" placeholder="password">
                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-2" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-2" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Username</strong>
                                </div>
                                <div class="col-md">aTamrakar
                                    <div class="details collapse" id="stng-3">
                                        <div class="m-t-20">
                                            <div class="m-b-10"><input type="text" class="input" size="50" value="aTamrakar">
                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-3" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-3" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Account type</strong>
                                </div>
                                <div class="col-md">Basic - <a href="#">get more out of askmeoffer with Premium membership</a>
                                    <div class="details collapse" id="stng-4">
                                        <div class="m-t-20">
                                            <p>You currently have a Basic askmeoffer account. It's free to upgrade to askmeoffer Premium, where you can take advantage of great features such as Faster Paying stores, comprehensive askmeoffer support customer service and bespoke promotions, simply let us retain the first £5 of your annual cashback earnings.</p>
                                            <a href="">Compare account types</a>
                                            <div class="row m-b-10 m-t-20 text-center">
                                                <div class="col-md-3 col-6">
                                                    <div class="m-b-10"><i class="fas f-50 fa-certificate yellow-text"></i>
                                                    </div>
                                                    <p>Bespoke promotions and loyalty bonuses</p>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="m-b-10">
                                                        <span class="fa-layers fa-fw  f-50">
                                                            <i class="fas fa-bullhorn" data-fa-transform="shrink-8"></i>
                                                            <i class="fas fa-ban red-text"></i>
                                                          </span>
                                                    </div>
                                                    <p>Website free of sponsored advertising</p>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="m-b-10">
                                                        <span class="fa-layers fa-fw f-50 green-text">
                                                            <i class="fas fa-fast-forward " data-fa-transform="shrink-8"></i>
                                                            <i class="far fa-circle"></i>
                                                            </span>
                                                    </div>
                                                    <p>Get your cashback quicker using Faster Paying stores</p>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="m-b-10"><i class="fas f-50 fa-comments blue-text"></i>
                                                    </div>
                                                    <p>Premium askmeoffer support including new Live Chat and Request Call services</p>
                                                </div>
                                            </div>

                                            <button class="btn blue">Upggrade to premium now</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-4" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-4" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Gender</strong>
                                </div>
                                <div class="col-md">Male
                                    <div class="details collapse" id="stng-5">
                                        <div class="m-t-20">
                                            <div class="m-b-10">
                                                <label class="d-inline-flex align-items-center m-r-20"><input type="radio" name="gender" class="m-r-5" value="male" checked> Male</label>
                                                <label class="d-inline-flex align-items-center m-r-20"><input type="radio" name="gender" class="m-r-5" value="female"> Female</label>
                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-5" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-5" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">D.O.B</strong>
                                </div>
                                <div class="col-md">24-4-1980
                                    <div class="details collapse" id="stng-6">
                                        <div class="m-t-20">
                                            <div class="m-b-10">
                                                <input type="text" class="input m-r-5" size="10" value="" placeholder="DD">
                                                <input type="text" class="input m-r-5" size="10" value="" placeholder="MM">
                                                <input type="text" class="input m-r-5" size="10" value="" placeholder="YYYY">
                                                <div><strong>E.g. 24 4 1980</strong>
                                                </div>
                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-6" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-6" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Postcode</strong>
                                </div>
                                <div class="col-md">44600
                                    <div class="details collapse" id="stng-7">
                                        <div class="m-t-20">
                                            <div class="m-b-10">
                                                <input type="text" class="input m-r-5" size="50" value="" placeholder="postal code">

                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-7" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-7" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Picture</strong>
                                </div>
                                <div class="col-md"><img src="img/settings-add-your-photo.png" alt="">
                                    <div class="details collapse" id="stng-8">
                                        <div class="m-t-20">
                                            <div class="m-b-10">
                                                <label class="btn green">Upload new profile <input type="file" class="d-none" ></label>
                                                <div>Tip: For best results, upload an image that is even in width and height. If not, the avatar may appear skewed.</div>
                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-8" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-8" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>