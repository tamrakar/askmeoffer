<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="sgin-body rounded white border pad-30 m-b-20 m-t-10">
            <div class="row f-12 align-items-center no-gutters m-b-5 justify-content-between">
                <div class="col-auto blue-text">Welcome</div>
                <div class="col-auto blue-text">Account type</div>
                <div class="col-auto">Your details</div>
                
                <div class="col-auto">Stores</div>
            </div>
            
            <div class="m-b-20">
                <div class="row align-items-center no-gutters">
                    <div class="col-auto">
                        <i class="fas fa-check-circle blue-text"></i>
                    </div>
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="fas fa-circle blue-text"></i>
                    </div>
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="far fa-circle blue-text"></i>
                    </div>
                    
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="far fa-circle blue-text"></i>
                    </div>

                </div>
            </div>
            <form action="your-details.php" method="post">

            <div class="text-center">
                <h1 class="m-b-20 blue-text">Select your account type </h1>
                
                <div class="acc-typ-cont">
                    <div class="row">
                        <div class="col-md m-b-20">
                            <div class="h-100 white acc-typ-optn d-flex flex-column">
                                <div class="pad-20"><h2 class="f-22 blue-text m-0">askmeoffer</h2>
                                <div class="acc-typ orange-text bold">Premium</div>
                                <div class="bold green-text"> $5 of your annual cashback earning</div></div>
                                <hr class="m-0">
                                <div class="pad-20 f-12"><p> Enjoy cashback uplift when you withdraw your earnings as an Amazon.co.uk Gift Card. </p>
                                <p>askmeoffer Rewards – earn twice as much as basic members! </p>
                                <p>Site free of advertisements </p>
                                <p>Access all askmeoffer Support features, including ‘live chat’ and ‘request call’ </p>
                                <p>Get paid quicker by 1000+ ‘faster paying’ Stores </p></div>
                                <hr class="m-0">

                               <div class="pad-20 m-auto"> <input type="radio" name="account-type"> Selected</div></div>

                        </div>
                        <div class="col-md m-b-20">
                            <div class=" h-100 white acc-typ-optn d-flex flex-column">
                                <div class="pad-20"><h2 class="f-22 blue-text m-0">askmeoffer</h2>
                                <div class="acc-typ bold grey-text">&nbsp;</div>
                                <div class="bold"> Supported by Stores ads</div></div>
                                <hr class="m-0">
                                <div class="pad-20 f-12"><p>Enjoy cashback uplift when you withdraw your earnings as an Amazon.co.uk Gift Card. </p>
                                </div>
                                <hr class="m-0" style="margin-top: auto !important;">

                               <div class="pad-20"> <input type="radio" name="account-type"> Selected</div></div>

                        </div>
                    </div>
                </div>
            </div>
            
            <hr>
            <div class="text-right">
                <button type="submit" class="btn blue">Next step <span class="fas fa-angle-right m-l-5"></span></button>
            </div>
</form>
        </div>
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>