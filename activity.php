<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Activity</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab xs-tab-2 d-flex">
                <li><a href="">Account Summary</a></li>
                <li class="current"><a href="">Activity</a></li>
                <li><a href="">Payments</a></li>
                <li><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                <div class="row">
                <div class="col-md-auto sm-m-b-10">
                    <ul class="list-bordered border user-side-menu">
                        <li><a href="" class="current">Online & High Street</a></li>
                        <li><a href="">Claims</a></li>
                        <li><a href="">ClickSnap</a></li>
                        <li><a href="">Rewards</a></li>
                        <li><a href="">Compare</a></li>
                    </ul>
                </div>
                <div class="col">
                    <!--<div class="border pad-20 m-b-20">
                        
                        <div class="text-center">
                        <div class="f-24 blue-text">You've not yet made any tracked purchases</div>
                        <div class="m-b-20">To start earning cashback why not check out our latest offers</div>
                        <a href="" class="btn blue">View offers</a>
                        </div>
                    </div>-->
                    <div class="grey-lighter-2 pad-15 f-16 m-b-20">
                       <i class="fas fa-info-circle m-r-5 m-l-5 blue-text"></i> Need help? Common questions related to purchases
                    </div>
                    <hr>
                    <div class="">
                        
                        <div class="bold collapsed m-b-10" data-target="#details-1" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> It sounds too good to be true, are there any hidden costs?
                        </div>
                        <div class="details f-12 collapse" id="details-1">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askmeoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askmeoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>
                        <div class="bold collapsed m-b-10" data-target="#details-2" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> Can I browse offers before I sign up?
                        </div>
                        <div class="details f-12 collapse" id="details-2">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askmeoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askmeoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-3" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> Is it safe and secure?
                        </div>
                        <div class="details f-12 collapse" id="details-3">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askmeoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askmeoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-4" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> How much money can I earn?
                        </div>
                        <div class="details f-12 collapse" id="details-4">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askmeoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askmeoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-5" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> How do I get paid my cashback?
                        </div>
                        <div class="details f-12 collapse" id="details-5">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askmeoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askmeoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                         <div class="bold collapsed m-b-10" data-target="#details-6" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> What is in-store cashback?
                        </div>
                        <div class="details f-12 collapse" id="details-6">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askmeoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askmeoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-7" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> Can I save on my supermarket shopping?
                        </div>
                        <div class="details f-12 collapse" id="details-7">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askmeoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askmeoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                                             
                    </div>
                </div>
                </div>
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>