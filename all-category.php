<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Category</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid m-b-20">

        <div class="offers-0 ">
            
            <div class="no-gutters row">
                <div class="deal-title deal-title-sm text-center">
                    <div>
                        <h3 class="f-16 f-c w-300">Top offers of the week</h3>
                        <a href="" class="btn blue f-12">View all</a> </div>
                </div>
                <div class="weekly-offer-carousel">
                    <div class="owl-carousel weekly-offer">
                        <?php for($i=0;$i<5;$i++){ ?>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        
        <h1 class="f-c f-22 w-400 m-t-0 m-t-15">Top Categories</h1>
        
        <div class="row all-category-list">
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-red-500">palette</i>              <span class="ctgr-item-name d-flex align-items-center justify-content-center">Arts & Crafts</span>             <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-pink-500">toys</i>                <span class="ctgr-item-name d-flex align-items-center justify-content-center">Bags & Cases</span>              <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-purple-500">face</i>              <span class="ctgr-item-name d-flex align-items-center justify-content-center">Beauty & Care</span>             <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-indigo-500">library_books</i>     <span class="ctgr-item-name d-flex align-items-center justify-content-center">Experiences & Courses</span>     <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-blue-500">wc</i>                  <span class="ctgr-item-name d-flex align-items-center justify-content-center">Fashion & Clothing</span>        <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-cyan-500">local_cafe</i>          <span class="ctgr-item-name d-flex align-items-center justify-content-center">Food & Drink</span>              <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-teal-500">format_paint</i>        <span class="ctgr-item-name d-flex align-items-center justify-content-center">Furniture</span>                 <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-green-500">tv</i>                 <span class="ctgr-item-name d-flex align-items-center justify-content-center">Gadgets & Electronics</span>     <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-lime-500">card_giftcard</i>       <span class="ctgr-item-name d-flex align-items-center justify-content-center">Gifts</span>                     <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-amber-500">directions_run</i>     <span class="ctgr-item-name d-flex align-items-center justify-content-center">Health & Fitness</span>          <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-brown-500">home</i>               <span class="ctgr-item-name d-flex align-items-center justify-content-center">Home & Garden</span>             <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-blue-grey-500">kitchen</i>        <span class="ctgr-item-name d-flex align-items-center justify-content-center">Home Appliances & Kitchen</span> <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-orange-500">monetization_on</i>   <span class="ctgr-item-name d-flex align-items-center justify-content-center">Jewellery</span>                 <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-light-green-500">laptop</i>       <span class="ctgr-item-name d-flex align-items-center justify-content-center">Laptops & Computers</span>       <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-light-blue-500">person</i>        <span class="ctgr-item-name d-flex align-items-center justify-content-center">Men’s Fashion</span>             <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-yellow-500">directions_car</i>    <span class="ctgr-item-name d-flex align-items-center justify-content-center">Motoring</span>                  <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-red-a700">work</i>                <span class="ctgr-item-name d-flex align-items-center justify-content-center">Office</span>                    <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-pink-a700">format_paint</i>       <span class="ctgr-item-name d-flex align-items-center justify-content-center">Painting & Decorating</span>     <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-purple-a700">pets</i>             <span class="ctgr-item-name d-flex align-items-center justify-content-center">Pets</span>                      <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-indigo-200">restaurant_menu</i>   <span class="ctgr-item-name d-flex align-items-center justify-content-center">Restaurant Deals</span>          <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-blue-700">vpn_key</i>             <span class="ctgr-item-name d-flex align-items-center justify-content-center">Services</span>                  <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-teal-a700">directions_bike</i>    <span class="ctgr-item-name d-flex align-items-center justify-content-center">Sports & Outdoors</span>         <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-green-300">toys</i>               <span class="ctgr-item-name d-flex align-items-center justify-content-center">Toys & Games</span>              <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-lime-a700">place</i>              <span class="ctgr-item-name d-flex align-items-center justify-content-center">Travel & Holiday</span>          <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2 m-b-10"><a href="" class="ctgr-item border white rounded pad-15 d-block text-center"><i class="material-icons f-50 color-amber-a700">pregnant_woman</i>    <span class="ctgr-item-name d-flex align-items-center justify-content-center">Women’s Fashion</span>           <span class="ctgr-deal">Deals: <span class="orange-text">20</span></span><span class="ctgr-cupn">Coupons: <span class="orange-text">50</span></span><span class="btn blue d-block">View Deals</span></a></div>
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
     <?php include('includes/lang-list.php'); ?>
     <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>