<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Offers</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid m-b-20">
        <div class="white border ">
           <div class="pad-15">
           <h1 class="m-0 f-c f-26 blue-text w-400">Get the best Deals and Offers !</h1>
           <p class="m-0">ravelling is a wonderful experience and there's an explorer in all of us who shouldn't
be lost at any cost. Now, avail some splendid discounts and get Bookings.</p>
         </div>
        </div>   
            <div class=" white border border-top-0 m-b-15">
                  <div class="offer-search-bar ">
                      <div class="offer-search-bar-inner" id="toggle-1">
                        
                         <div class="row align-items-center justify-content-between">
                          <div class="col-auto">
                              <button data-target="#bycategory" class="btn white border m-r-5 collapsed" data-toggle="collapse">By category <i class="fas fa-angle-down grey-text"></i></button>
                              <button data-target="#bymerchant" class="btn white border collapsed" data-toggle="collapse">By merchant <i class="fas fa-angle-down grey-text"></i></button>
                          </div>
                            <div class="col-12 col-lg  md-order-last md-m-t-10">
                                  <div class="row f-c text-uppercase">
                                <div class="col-xs-6 col-sm"><a href="" class="btn blue d-block white-text">Latest Deals</a></div>
                                <div class="col-xs-6 col-sm"><a href="" class="btn red d-block white-text">Coupons</a></div>
                                <div class="col-xs-6 col-sm"><a href="" class="btn orange d-block white-text">Free Stuff</a></div>
                                <div class="col-xs-6 col-sm"><a href="" class="btn green d-block white-text">Deals Of THE dAY</a></div>
                            </div>
                          </div>
                          <div class="col-auto d-none justify-content-end d-md-flex align-items-center">
                              <label class="m-b-0 m-r-5">Sort by</label>
                              <select name="" id="" class="input">
                                <option>Relevance</option>
                                <option>Featured</option>
                                <option>Price: Low to high</option>
                                <option>Price: High to low</option>
                                <option>Avg. Customer review    </option>
                            </select>
                          </div>
                          </div>
                            <div >
                                <div id="bycategory" class="collapse" data-parent="#toggle-1">
                                    <div class="m-t-15">
                                        <ul class="idTabs tab-offer border ">
                                            <li>
                                                <a href="#idTab1">
                                                    <div><i class="material-icons orange-text">apps</i>All</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab2">
                                                    <div><i class="material-icons blue-text">laptop</i>Electronics</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab3">
                                                    <div><i class="material-icons red-text">wc</i>Fashion</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab4">
                                                    <div><i class="material-icons green-text">directions_run</i>Health & Beauty</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab5">
                                                    <div><i class="material-icons yellow-text">restaurant_menu</i>Dining</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab6">
                                                    <div><i class="material-icons mag-text">shopping_cart</i>Groceries</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab7">
                                                    <div><i class="material-icons cyan-text">home</i>Home & Living</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab8">
                                                    <div><i class="material-icons purple-text">place</i>Travel</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab9">
                                                    <div><i class="material-icons orange-text">directions_bike</i>Sports & Outdoors</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab10">
                                                    <div><i class="material-icons cyan-dark-text">directions_car</i>Automotive</div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                             <div class="card">
                  <div id="bymerchant" class="collapse" data-parent="#toggle-1">
                      <form action="?" class="search-group m-t-15">
                            <div class="row align-items-center"><i class="material-icons col-auto f-26">search</i>
                                <input type="text" class="col" placeholder="Search by merchant">
                                <button class="col-auto f-26"><i class="material-icons">keyboard_arrow_right</i></button>
                            </div>
                        </form>
                  </div>
                 </div>
                      </div>
                  </div>
            </div>
                <div class="row">
                   <?php for($i=0;$i<12;$i++){ ?>
                    <div class="col-lg-3 col-xl-2 col-md-4 col-sm-6 col-xs-12 col-offer-item m-b-10">
                        <div class="offer-item white text-center border pad-15">
                            <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                            </figure>
                            <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                            <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                            <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                            <div class="coupon-info">
                                <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-placement="top" data-original-title="Popover title">info</i></a> </div>
                                <div class="deal-code">
                                        <div class="copy-code">
                                            <div class="code-cont" id="ccode0<?=$i;?>">COUPONCODE<?=$i;?>
                                            </div>
                                            <button class="code-copy-action btn green" data-clipboard-target="#ccode0<?=$i;?>">Copy</button>
                                        </div>
                                    </div>
                                <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-2 col-md-4 col-sm-6 col-xs-12 col-offer-item m-b-10">
                        <div class="offer-item white text-center border pad-15">
                            <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                            </figure>
                            <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                            <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                            <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                            <div class="coupon-info">
                                <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-placement="top" data-original-title="Popover title">info</i></a> </div>
                                <div class="deal-code">
                                    <div class="no-code">No Code required</div>
                                </div>
                                <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/l-1.png" height="18"></span> <a href="" class="f-12 blue-text">View all offers</a> </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                
                <nav>
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true"><i class="fas fa-angle-left"></i></span>
        <span class="sr-only">Previous</span>
      </a>
    </li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">4</a></li>
    <li class="page-item"><a class="page-link" href="#">5</a></li>
    <li class="page-item"><a class="page-link" href="#">6</a></li>
    <li class="page-item"><a class="page-link" href="#">7</a></li>
    <li class="page-item"><a class="page-link" href="#">8</a></li>
    <li class="page-item"><a class="page-link" href="#">9</a></li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true"><i class="fas fa-angle-right"></i></span>
        <span class="sr-only">Next</span>
      </a>
    </li>
  </ul>
</nav>
                
            
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
     <?php include('includes/lang-list.php'); ?>
     <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>