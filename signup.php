<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        <div class="sgin-hdr text-center m-b-20"><h1 class="f-32 blue-text w-400 f-c m-0">Join askmeoffer today &amp; start earning</h1>
        <div class="f-16">Already a member? <a href="">Sign in</a></div></div>
        <div class="white rounded sgin-body border m-b-20">
            <div class="row">
                <div class="col-md-5">
                    <div class="pad-30 border-right h-100"><div class="m-b-15"><input type="text" class="input w-100" placeholder="first name"></div>
                    <div class="m-b-15"><input type="text" class="input w-100" placeholder="surname "></div>
                    <div class="m-b-15"><input type="text" class="input w-100" placeholder="email"></div>
                    <div class="m-b-15"><input type="password" class="input w-100" placeholder="password"></div>
                    <div class="m-b-15">
                        <div class="row">
                            <div class="col-auto">
                                <input type="checkbox">
                            </div>
                            <div class="col">
                                <p>I have read, understand and agree to the <a href="#">askemoffer Account Holder Agreement</a> and acknowledge <a href="#">askemoffer's Privacy Policy</a></p>
                                <p>By clicking Join askemoffer Free, I agree to receive marketing emails from askemoffer. I understand I can unsubscribe at any time in my account settings.</p>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn orange f-18 d-block w-100" value="Join askmeoffer free">
                    <hr class="m-t-20 m-b-10">
                    <div class="text-center"><div class=" m-b-5">Or sign in with</div>

                        <a href="" class="btn red d-block m-b-10"><i class="fab fa-google-plus-g m-r-5"></i>Google</a>
                        <a href="" class="btn blue d-block"><i class="fab fa-facebook-f m-r-5"></i> Facebook</a>
                       <!-- <a href="" class="btn cyan"><i class="fab fa-paypal m-r-5"></i> Paypal</a>-->
                    </div>
                    
                    </div>
                    
                </div>
                <div class="col-md-7">
                    <div class="pad-30">
                        <div class="f-22 f-c blue-text m-b-20">Need to know more?</div>
                        
                        <div class="bold collapsed m-b-10" data-target="#details-1" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> It sounds too good to be true, are there any hidden costs?
                        </div>
                        <div class="details f-12 collapse" id="details-1">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askemoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askemoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>
                        <div class="bold collapsed m-b-10" data-target="#details-2" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> Can I browse offers before I sign up?
                        </div>
                        <div class="details f-12 collapse" id="details-2">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askemoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askemoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-3" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> Is it safe and secure?
                        </div>
                        <div class="details f-12 collapse" id="details-3">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askemoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askemoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-4" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> How much money can I earn?
                        </div>
                        <div class="details f-12 collapse" id="details-4">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askemoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askemoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-5" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> How do I get paid my cashback?
                        </div>
                        <div class="details f-12 collapse" id="details-5">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askemoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askemoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                         <div class="bold collapsed m-b-10" data-target="#details-6" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> What is in-store cashback?
                        </div>
                        <div class="details f-12 collapse" id="details-6">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askemoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askemoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                        <div class="bold collapsed m-b-10" data-target="#details-7" data-toggle="collapse">
                           <i class="fas fa-angle-down blue-text m-r-5"></i> Can I save on my supermarket shopping?
                        </div>
                        <div class="details f-12 collapse" id="details-7">
                            <p> As you might expect, you’re not the first person to question how we operate a viable business model by paying people to shop.
                            </p>
                            <p>Let’s set your mind at rest. First we can guarantee there are no hidden costs associated with signing up to askemoffer.
                            </p>
                            <p>To pay the bills, we operate a two-tier membership. Our basic membership is completely free to use and is funded by sponsored advertising paid for by our stores. We also offer a higher tier of membership, askemoffer Premium, which includes extra features in exchange for £5 of your annual cashback earnings. During the sign-up process you’ll be offered the opportunity to choose which membership you want.
                            </p>
                        </div> 
                        <hr>  
                                             
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>