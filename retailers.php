<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="sgin-body rounded white border pad-30 m-b-20 m-t-10">
            <div class="row f-12 align-items-center no-gutters m-b-5 justify-content-between">
                <div class="col-auto blue-text">Welcome</div>
                <div class="col-auto blue-text">Account type</div>
                <div class="col-auto blue-text">Your details</div>
                
                <div class="col-auto">Stores</div>
            </div>
            
            <div class="m-b-20">
                <div class="row align-items-center no-gutters">
                    <div class="col-auto">
                        <i class="fas fa-check-circle blue-text"></i>
                    </div>
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="fas fa-check-circle blue-text"></i>
                    </div>
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="fas fa-check-circle blue-text"></i>
                    </div>
                    
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="fas fa-circle blue-text"></i>
                    </div>

                </div>
            </div>
            
            <div class="text-center">
                <h1 class="m-b-0 blue-text">Pick your favourite Stores</h1>
                <p>Select your favourite stores from our range of cateogries and  we'll will show you more offer from the brands you love, in addition to adding these to your bookmarks for later.</p>
                
                <ul class="white border tabs tab-filter filter xs-tab-2" id="portfolio-filter">
                <li>
                    <a href="#all" rel="all"><div><i class="material-icons orange-text">fingerprint</i>All</div></a>
                </li>
                <li>
                    <a href="#" rel="type-1">  <div><i class="material-icons blue-text">laptop</i>Electronics</div></a>
                </li>
                <li>
                    <a href="#" rel="type-2">  <div><i class="material-icons red-text">wc</i>Fashion</div></a>
                </li>
                <li>
                    <a href="#" rel="type-3">  <div><i class="material-icons green-text">directions_run</i>Health & Beauty</div></a>
                </li>
                <li>
                    <a href="#" rel="type-4">  <div><i class="material-icons yellow-text">restaurant_menu</i>Dining</div></a>
                </li>
                <li>
                    <a href="#" rel="type-5">  <div><i class="material-icons mag-text">shopping_cart</i>Groceries</div></a>
                </li>
                <li>
                    <a href="#" rel="type-6">  <div><i class="material-icons cyan-text">home</i>Home & Living</div></a>
                </li>
                <li>
                    <a href="#" rel="type-7">  <div><i class="material-icons purple-text">place</i>Travel</div></a>
                </li>
                <li>
                    <a href="#" rel="type-8">  <div><i class="material-icons orange-text">directions_bike</i>Sports & Outdoors</div></a>
                </li>
                <li>
                    <a href="#" rel="type-9">  <div><i class="material-icons cyan-dark-text">directions_car</i>Automotive</div></a>
                </li>
            </ul>    
                <div class="border border-top-0 white pad-20">
                    <ul class="row justify-content-center filtr-container clearfix" id="portfolio-list">
                       <?php for($i=0;$i<3;$i++){ ?>
                        <li class="col-md-3 col-lg-2 filtr-item type-1">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/flipkart_store.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        <li class="col-md-3 col-lg-2 filtr-item type-2">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/gadgetsnow_store.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        <li class="col-md-3 col-lg-2 filtr-item type-3 type-1">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/amazon_store.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        <li class="col-md-3 col-lg-2 filtr-item type-4">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/ebay_store.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                         <li class="col-md-3 col-lg-2 filtr-item type-5 type-2 type-1">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/cool-winks-coupons.1492404662.jpg" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        <li class="col-md-3 col-lg-2 filtr-item type-6">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/jabong.1502716950.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        <li class="col-md-3 col-lg-2 filtr-item type-7">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/amazon_store.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        <li class="col-md-3 col-lg-2 filtr-item type-8">
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/gadgetsnow_store.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        <li class="col-md-3 col-lg-2 filtr-item type-9" >
                           <label class="border pik-brnd d-flex justify-content-center align-items-center">
                            <span class="text-center d-block"><img src="img/flipkart_store.png" alt="">
                            <input type="checkbox" class="invisible"></span>
                            </label>
                        </li>
                        
                        
                         <?php } ?>
                    </ul>
                </div>
                
            </div>
            <hr>
            <div class="text-right">
               
                <button class="btn blue m-l-20">Finish <span class="fas fa-check m-l-5"></span></button>
            </div>
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>