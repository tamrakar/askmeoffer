<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab xs-tab-2 d-flex">
                <li class="current"><a href="">Account Summary</a></li>
                <li><a href="">Activity</a></li>
                <li><a href="">Payments</a></li>
                <li><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                
                <div class="row m-b-40">
                    <div class="col-md-4">
                        <h4 class="m-0 f-16">Payable cashback</h4>
                        <hr class="m-t-10">
                        <span class="f-32 align-middle green-text">£0.00 </span><a href="" class="btn btn-sm blue">Pay me</a>
                        <div class="m-t-10">
                            <a href="#">Payment history</a> | <a href="#">Annual summary</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 class="m-0 f-16">Tracked cashback</h4>
                        <hr class="m-t-10">
                        <span class="f-32 blue-text">£0.00 </span>
                        <div class="m-t-10">
                            <a href="#">Activity</a> | <a href="#">Missing a transaction?</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 class="m-0 f-16">Reward bonus</h4>
                        <hr class="m-t-10">
                        <div class="f-11 l-1-0 m-b-5">Earn up to</div>
                        <span class="f-32 l-1-0 green-text">£70.00</span> <span class="green-text f-12">extra in rewards bonuses every year</span>
                        <div class="m-t-10">
                            <a href="#">Opt in to askmeoffer rewards</a>
                        </div>
                    </div>
                    
                </div>
                <h4 class="m-0 f-16">Notifications</h4>
                <hr class="m-t-10">
                <div class="border pad-20">
                   <div class="text-center f-42 grey-lighter-text">
                       <i class="fas fa-fw fa-gamepad"></i>
                       <i class="fas fa-fw fa-beer"></i>
                       <i class="fas fa-fw fa-suitcase"></i>
                       <i class="fa fa-fw fa-female"></i>
                       <i class="fas fa-fw fa-bed"></i>
                   </div>
                    <div class="text-center">You don't have any activity on your askmeoffer account yet. Browse our offers to find some great cashback deals.</div>
                </div>
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>