<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Settings-instore</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab d-flex xs-tab-2">
                <li><a href="">Account Summary</a></li>
                <li><a href="">Activity</a></li>
                <li><a href="">Payments</a></li>
                <li><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li class="current"><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                <div class="row">
                <div class="col-md-auto">
                   <ul class="list-bordered border user-side-menu">
                        <li><a href="settings.php" class="">General</a></li>
                        <li><a href="settings-security.php">Reset password</a></li>
                        <li><a href="settings-payments.php">Payment</a></li>
                        <li><a href="settings-favourites.php">Favourites</a></li>
                        <li><a href="settings-instore.php" class="current">In-store</a></li>
                        <li><a href="settings-thirdpartyapps.php">Third-party apps</a></li>
                    </ul>
                </div>
                <div class="col-md">
                   <div class="m-l-20 sm-m-l-0 sm-m-t-20">
                   <div class="row"><div class="col-md-6">
                    <h1 class="f-22 blue-text m-t-0">In-store cashback</h1>
                    <div class="settings-list">
                        <p>Register one or more of these cards</p>
                        <hr>
                        <div class="cred-crds m-b-10">
                            <img src="img/card-logo-visa.svg" width="50" alt="">
                            <img src="img/card-logo-mastercard.svg" width="50" alt="">
                            <img src="img/card-logo-maestro.svg" width="50" alt="">
                        </div>
                        <div class="row m-b-10 align-items-center">
                            <div class="col">Card number:</div>
                            <div class="col"><input type="text" class="input"></div>
                        </div>
                        <div class="row m-b-10 align-items-center">
                            <div class="col">Expiry date:</div>
                            <div class="col d-flex align-items-center">
                                <select name="" id="" class="input"><option value="">02</option></select>
                                <span class="m-r-10 m-l-10">/</span>
                                <select name="" id="" class="input"><option value="">02</option></select>
                            </div>
                        </div>
                        <div class="row m-b-10 align-items-center">
                            <div class="col">Card nickname:</div>
                            <div class="col"><input type="text" class="input"></div>
                        </div>
                        <button class="btn red">register card</button>
                        <hr>
                        <div class="m-t-20 f-12">
                            <h4 class="f-12 m-0">Important information</h4>
<p>Only your card number and expiry is registered, your security code or PIN is not required and so it's completely secure.</p>
                        </div>
                    </div>
                    </div></div>
                    </div>
                </div>
                </div>
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>