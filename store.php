<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Store Detail</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        <section class="offers-0 ">
            <div class="no-gutters row">
                <div class="deal-title deal-title-sm text-center">
                    <div>
                        <h3 class="f-16 f-c w-300">Top offers of the week</h3>
                        <a href="" class="btn blue f-12">View all</a> </div>
                </div>
                <div class="weekly-offer-carousel">
                    <div class="owl-carousel weekly-offer">
                        <?php for($i=0;$i<5;$i++){ ?>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-1.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-2.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-3.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-4.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-5.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="" class="coupon-item">
                                <figure><img alt="img" src="img/l-6.png">
                                </figure>
                                <div class="f-12">Up to 18.0% Cashback</div>
                                <div class="f-12 green-text">10 coupons</div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="store-details d-flex align-items-center m-b-20 justify-content-center">
            <div class="video-wrapper">
                <!--<video class="store-video" autoplay loop>
                    <source type="video/mp4" src="video/new-revised.mp4">
                </video> -->
                <div class="store-video">
                <iframe  width="560" height="315" src="https://www.youtube.com/embed/1DPDiM-MLmg?rel=0&amp;controls=0&amp;showinfo=0;autoplay=1;loop=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="breadcrumb"> <a href="#">Home</a> / <a href="#">Store</a> / <a href="#">Flipkart</a> </div>
            <div class="store-btn-grp"><a href="" class="btn red">Exclusive</a> <a href="" class="btn purple">Featured</a> <a href="" class="btn orange">Faster Paying</a> <a href="" class="btn green">Increased CashBack</a>
            </div>
            <div class="store-info">
                <div class="store-logo m-b-10 row justify-content-center"> <a href="" class="col-auto"><img alt="img" src="img/fk-logo_9fddff.png"></a> 
                
                    <div class="col-auto ">
                        <div class="pad-5"><div class="stars-all yellow-text f-21 l-1-0 m-l-5 m-r-5">
                            <i class="material-icons">star</i>
                            <i class="material-icons">star</i>
                            <i class="material-icons">star</i>
                            <i class="material-icons">star_half</i>
                            <i class="material-icons">star_border</i>
                        </div>
                        <div class="rating-meta-all w-400 f-12">
                            29 Ratings • 7 Reviews
                        </div></div>
                    </div>
                
                
                </div>
                <div class="store-desc f-16"> Flipkart Offers: Upto 60% Off TVs, Appliances & more + Extra 10% Off + Upto 6% ASKMEOFFER Rewards </div>
                <div class="m-t-10 w-700">WHAT ARE ASKMEOFFER REWARDS?</div>
                <a href="#" class="btn orange btn-activate activate-trigger" data-status="<?=$_GET['status']; ?>">ACTIVATE REWARDS</a>
                <div class="cashback-stats">
                    <div class="stat-item d-flex justify-content-between">
                        <div><i class="material-icons" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="Popover title">info</i> Cashback / Rewards Tracking Speed</div>
                        <div>2days</div>
                    </div>
                    <div class="stat-item d-flex justify-content-between">
                        <div><i class="material-icons" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="Popover title">info</i> CashBack / Rewards Claim Time</div>
                        <div>30days</div>
                    </div>
                    <div class="stat-item d-flex justify-content-between">
                        <div><i class="material-icons" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="Popover title">info</i> CashBack / Rewards Reliablity</div>
                        <div>100y</div>
                    </div>
                </div>
                <div class="store-coupon-btns">
                    <a href="#" class="btn green store-coupon-jump"><strong>100 COUPONS</strong> <br> VIEW ALL</a>
                    <a href="#" class="btn cyan-dark store-deal-jump"><strong>100000 PRODUCT DEALS</strong><br> VIEW ALL</a>
                </div>

                <div class="reward-rates-container toggle-container pop-float-wrapper">
                    <a href="#" class="white-text toggle-trigger">Show Rewards Rates <i class="fas fa-angle-down"></i></a>
                    <div class="pop-float pop text-left toggle-body">
                        <button type="button" class="close">×</button>
                        <div class=" pop-body ">
                            <ul class="list-bordered">
                                <?php for($i=0;$i<10;$i++){ ?>
                                <li>
                                    <div class="row m-t-15">
                                        <div class="col-auto f-25 l-1-1">7.50%</div>
                                        <div class="col-auto toggle-container">
                                            <span class="toggle-trigger-fixed m-r-5 m-l-5" data-pop-target="rewardRateTerms"><i class="material-icons grey-text f-17">info</i></span>
                                        </div>
                                        <div class="col">
                                            <p>CashKaro Rewards on Clothing, Footwear, Watches, Bags, Wallets, Belts, Fashion Accessories, Gold & Fashion Jewellery, Sports & Fitness, Sunglasses & Eyewear on both APP & Desktop/Mobile WEBSITE</p>
                                        </div>
                                    </div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="product-terms" data-toggle="modal" data-target="#terms"><i class="material-icons">sort</i> Terms to Earn Extra Cashback</a>
        </div>

        <div class="white border pad-20 text-center m-b-10">
            <div>
                <h2 class="m-0 f-c blue-text f-28 w-400">Hurray we have 10000+ Active Flipkart Coupons</h2>
            </div>
            <div class="f-16 bold">Our Exclusive Coupons</div>
        </div>


        <div class="row">
            <div class="col">
                <div class="border white m-b-20">
                    <ul class="idTabs tab-offer store-coupon-content">
                        <li>
                            <a href="#idTab1">
                                <div><i class="material-icons orange-text">apps</i>All</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab2">
                                <div><i class="material-icons blue-text">laptop</i>Electronics</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab3">
                                <div><i class="material-icons red-text">wc</i>Fashion</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab4">
                                <div><i class="material-icons green-text">directions_run</i>Health & Beauty</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab5">
                                <div><i class="material-icons yellow-text">restaurant_menu</i>Dining</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab6">
                                <div><i class="material-icons mag-text">shopping_cart</i>Groceries</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab7">
                                <div><i class="material-icons cyan-text">home</i>Home & Living</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab8">
                                <div><i class="material-icons purple-text">place</i>Travel</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab9">
                                <div><i class="material-icons orange-text">directions_bike</i>Sports & Outdoors</div>
                            </a>
                        </li>
                        <li>
                            <a href="#idTab10">
                                <div><i class="material-icons cyan-dark-text">directions_car</i>Automotive</div>
                            </a>
                        </li>
                    </ul>
                    <div class="pad-15">
                       <div class="sort-bar text-right m-b-15">
                        <div class="d-inline-flex align-items-center"><label class="m-b-0 m-r-10">Sort by</label> 
                        <select name="" id="" class="input">
                            <option>Relevance</option>
                            <option>Featured</option>
                            <option>Price: Low to high</option>
                            <option>Price: High to low</option>
                            <option>Avg. Customer review    </option>
                        </select></div>
                        </div>
                        <?php for($i=0;$i<10;$i++){ ?>
                        <div class="white border store-coupon-item m-b-10 pad-15">
                            <div class="row">
                                <div class="col-md-auto m-b-10">
                                    <div class="grey-lighter-2 rounded text-center pad-20 f-12 l-1-0 m-r-10">
                                        <div class="f-30">77%</div>
                                        <div>Cashback</div>
                                    </div>
                                </div>
                                <div class="col-md col-12">
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="coupon-meta f-12 d-md-flex sm-m-b-10">
                                                <div class="m-r-20"><i class="fas fa-check green-text"></i> Verified 3 hours ago</div>
                                                <div><i class="fas fa-user green-text"></i> 357 People Used Today</div>
                                            </div>
                                            <h2 class="coupon-title m-0 l-1-3 ">Flat 75% Off on Select Products</h2>
                                            <div class="green-text bold d-md-flex align-items-center sm-m-t-10"><i class="fas fa-money-bill-alt f-20 m-r-5"></i> Upto 7.5% Rewards From CouponDunia <span tabindex="0" class="f-12 blue-text m-l-5 cpop" data-container="body" data-toggle="popover" data-toggle="popover" data-placement="top" data-html="true" data-content="<p>Rewards from CouponDunia is real money that is added to your CouponDunia account after you transact at Flipkart.</p><a href='#hiw' class='bold' data-toggle='modal'>How this works?</a>">(What's this?)</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <div class="bold m-b-5"><i class="fas fa-certificate orange-text"></i> editors choice</div>
                                            <a href="" class="btn green d-block bold">Show Coupon</a>
                                        </div>
                                    </div>
                                    <hr class="m-t-15 m-b-15">
                                    <div class="row">
                                        <div class="col-md sm-m-b-10">
                                            <ul class="f-12 grey-text m-0 ">
                                                <li>Flat 75% off or more on apparel, accessories, appliances and much more.</li>
                                                <li>Applicable on select products.</li>
                                                <li>No coupon code is required.</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="" class="btn grey-lighter-2 grey-text d-block"><i class="far fa-comment-alt"></i> 1 comment</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="text-center  coupons-pagination paginate m-t-15"><span class="paging_no_page first_page">&nbsp;</span> <span class="paging_no_page previous_page">&nbsp; </span> &nbsp; <span class="paging_selected_page paginate_active">1</span> <a href="javascript:void(0);" class="paging_page pages" id="2">2</a> &nbsp;<a href="javascript:void(0);" id="2" class="paging_page next_page">&nbsp;</a> <a href="javascript:void(0);" id="2" class="paging_page last_page">&nbsp;</a>
                        </div>
                    </div>
                </div>
                
                
            </div>
            <div class="col sidebar order-first m-b-10">
                <div class="white border pad-15 m-b-10"><a href="" class="btn blue d-block"><i class="fas fa-user-plus m-r-5"></i>Invite friend &amp; earn 100 Each</a>

                    <div class="sidebar-store-info">
                        <figure class="text-center pad-15"><img alt="img" class="img-fluid" src="img/flipkart.1507100422.png">
                        </figure>
                        <div class="text-center rounded">
                            <div class="stars-all yellow-text f-21 l-1-0 m-l-5 m-r-5">
                                <i class="material-icons">star</i>
                                <i class="material-icons">star</i>
                                <i class="material-icons">star</i>
                                <i class="material-icons">star_half</i>
                                <i class="material-icons">star_border</i>
                            </div>
                            <div class="rating-meta-all w-400 f-12">
                                29 Ratings • 7 Reviews
                            </div>
                        </div>
                        <hr>
                        <div class="row  green-text m-b-10">
                            <div class="col-auto"><i class="fas fa-money-bill-alt f-20 m-l-10"></i>
                            </div>
                            <div class="col bold">
                                Upto 10% cashback from couponDunia
                            </div>
                        </div>
                        <a href="#" class="btn orange btn-activate activate-trigger" data-status="notlogged">ACTIVATE REWARDS</a>

                    </div>
                </div>

                <a href="" class="btn green d-block btn-share m-b-10">
                    <div class="d-inline-flex align-items-center m-t-10 m-b-10 f-16 l-1-0"><i class="fas fa-bullhorn m-r-15 f-23"></i>
                        <div class="text-left "><strong>SHARE A COUPON</strong><br>
                            <small>Spread the saving with everyone</small>
                        </div>
                    </div>
                </a>

                <div class="white border pad-15 m-b-10">
                    <h3 class="f-18 f-c w-400 m-t-0 m-b-20">Cashback Stats</h3>

                    <div class="stat-list">
                        <div class="stat-item f-19">
                            <div class="f-14">Average Tracking Speed</div>
                            <div class="blue-text">Upto 2 days <i class="material-icons f-22" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="Popover title">info_outline</i>
                            </div>
                        </div>
                        <div class="stat-item f-19">
                            <div class="f-14">Average Payment Speed</div>
                            <div class="blue-text">4-12 weeks <i class="material-icons f-22" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="Popover title">info_outline</i>
                            </div>
                        </div>
                        <div class="stat-item f-19">
                            <div class="f-14">Average Tracking Reliability</div>
                            <div class="blue-text">98.00 % <i class="material-icons f-22" data-toggle="popover" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="Popover title">info_outline</i>
                            </div>
                        </div>
                    </div>
                 </div>
                <div class="white border pad-15 m-b-10">
                    <h3 class="f-18 f-c w-400 m-t-0">Useful Tips</h3>
                    <p>CD Rewards will be based on the Final Sale Amount minus PhonePe/Flipkart Cashback &amp; Shipping Charges. CD Rewards can be transferred to PayTM; or for making a mobile phone or DTH recharge. It cannot be transferred to a bank account. CD reward queries should be directed to CouponDunia. Please do not contact Flipkart. No CD rewards for bulk buyers.</p>
                </div>
                <div class="white border pad-15 m-b-10">
                    <h3 class="f-18 f-c w-400 m-t-0 m-b-10">Similar Stores</h3>
                    <div class="smilar-store-list f-12">
                        <div class="row">
                            <div class="col-6">
                                <a href="" class="similar-store-item">
                                    <figure class="img-thumbnail"><img alt="img" src="img/l-4.png" class="img-full">
                                    </figure>
                                    Hermo</a>
                            </div>
                            <div class="col-6">
                                <a href="" class="similar-store-item">
                                    <figure class="img-thumbnail"><img alt="img" src="img/l-5.png" class="img-full">
                                    </figure>
                                    fave</a>
                            </div>
                            <div class="col-6">
                                <a href="" class="similar-store-item">
                                    <figure class="img-thumbnail"><img alt="img" src="img/l-6.png" class="img-full">
                                    </figure>
                                    Zalora</a>
                            </div>
                            <div class="col-6">
                                <a href="" class="similar-store-item">
                                    <figure class="img-thumbnail"><img alt="img" src="img/l-7.png" class="img-full">
                                    </figure>
                                    agoda</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="white border pad-15 m-b-10">
                    <h3 class="f-18 f-c w-400 m-t-0 m-b-10">Mobiwik cashback offer rates</h3>
                    <?php for($i=0;$i<5;$i++){ ?>
                    <a href="" class="row offer-rate-item black-text hover-no-deco">
                        <div class="col-auto">
                            <div class="orange rounded text-center pad-10 f-12 l-1-0 white-text">
                                <div class="f-20">77%</div>
                                <div>Cashback</div>
                            </div>
                        </div>
                        <div class="col">
                            <h5 class="f-13 m-0">On new user transaction</h5>
                            <div class="f-12 l-1-3"> This rate is applicable to new users transactions</div>
                        </div>
                    </a>
                    <hr class="m-t-10 m-b-10">
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="white border pad-20 text-center m-b-10">
            <div>
                <h2 class="m-0 f-c blue-text f-28 w-400">Hurray we have 10000+ Active Flipkart Product Deals</h2>
            </div>
            <div class="f-16 bold">Our Exclusive Product Deals</div>
        </div>

        <form action="?" class="white pad-10 f-16 border border-bottom-0 store-product-content">
            <div class="row">
                <div class="col"><input type="text" class="input w-100" placeholder="Search for any products from flipcart.com">
                </div>
                <div class="col-auto"><button type="submit" class="btn orange"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>


        <div class="white border m-b-20 ">
            <ul class="idTabs tab-offer">
                <li>
                    <a href="#idTab1">
                        <div><i class="material-icons orange-text">apps</i>All</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab2">
                        <div><i class="material-icons blue-text">laptop</i>Electronics</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab3">
                        <div><i class="material-icons red-text">wc</i>Fashion</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab4">
                        <div><i class="material-icons green-text">directions_run</i>Health & Beauty</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab5">
                        <div><i class="material-icons yellow-text">restaurant_menu</i>Dining</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab6">
                        <div><i class="material-icons mag-text">shopping_cart</i>Groceries</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab7">
                        <div><i class="material-icons cyan-text">home</i>Home & Living</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab8">
                        <div><i class="material-icons purple-text">place</i>Travel</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab9">
                        <div><i class="material-icons orange-text">directions_bike</i>Sports & Outdoors</div>
                    </a>
                </li>
                <li>
                    <a href="#idTab10">
                        <div><i class="material-icons cyan-dark-text">directions_car</i>Automotive</div>
                    </a>
                </li>
            </ul>


            <div class="pad-15">
               <div class="sort-bar text-right m-b-15">
                        <div class="d-inline-flex align-items-center"><label class="m-b-0 m-r-10">Sort by</label> 
                        <select name="" id="" class="input">
                            <option>Relevance</option>
                            <option>Featured</option>
                            <option>Price: Low to high</option>
                            <option>Price: High to low</option>
                            <option>Avg. Customer review    </option>
                        </select></div>
                        </div>
                <div class="row">
                    <?php for($i=0;$i<10;$i++){ ?>
                    <div class="col-auto col-product m-b-10">
                        <div class="coupon-deal-item store-product-item border">
                            <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                            </figure>
                            <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                            <div class="f-16 blue-text m-b-5">12MP | 5MP Camera</div>
                            <div class="d-flex m-b-5 justify-content-center align-items-center"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                            <div class="coupon-info">
                                <div class="d-flex f-14 line-sep justify-content-between align-items-center"> <span class="blue-text">+ Earn Rs.75 Cashback</span> <a href="" class="m-r-10"><i class="material-icons grey-text" data-toggle="popover" data-placement="top" data-trigger="hover" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="Popover title">info</i></a> </div>
                                <div class="deal-code">
                                    <div class="copy-code">
                                        <div class="code-cont" id="ccode0<?=$i;?>">COUPONCODE<?=$i;?>
                                        </div>
                                        <button class="code-copy-action btn green" data-clipboard-target="#ccode0<?=$i;?>">Copy</button>
                                    </div>
                                </div>
                                <div class="d-flex line-sep justify-content-between align-items-center"> <span><img alt="img" src="img/fk-logo-2.jpg" height="18"></span> <a href="" class="f-12 blue-text">check all prices</a> </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="text-center"><a href="" class="btn blue f-20 ">Load more products <i class="far fa-plus-square"></i></a>
                </div>
            </div>

        </div>
        <div class="m-b-20">
            <div class="border white border store-bottom-content m-b-20">
                <ul class="idTabs tabs">
                    <li>
                        <a href="#idTab1">
                            <div class="d-flex align-items-center"><i class="material-icons green-text m-r-10">stars</i>Store Review</div>
                        </a>
                    </li>
                    <li>
                        <a href="#idTab2">
                            <div class="d-flex align-items-center"><i class="material-icons mag-text m-r-10">help</i>Flipcart shopping guide</div>
                        </a>
                    </li>
                </ul>
                <div class="tabs-contents m-b-20">

                    <div id="idTab1" class="pad-20">
                        <h3 class="f-c f-22">Store Review</h3>
                         <div class="row m-b-20">
                            <div class="col">
                                <div class="row align-items-center m-b-20">
                                    <div class="col-auto">
                                        <div class="rating-score-all text-center">
                                            <div class="f-30 w-300 l-1-1 green-text">3.9</div>
                                            <div>out of 5</div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="stars-all yellow-text f-23 l-1-0 m-b-5">
                                            <i class="material-icons">star</i>
                                            <i class="material-icons">star</i>
                                            <i class="material-icons">star</i>
                                            <i class="material-icons">star_half</i>
                                            <i class="material-icons">star_border</i>
                                        </div>
                                        <div class="rating-meta-all">
                                            29 Ratings &#8226; 7 Reviews
                                        </div>
                                    </div>

                                </div>
                                <div class="rating-bars-list m-b-20 m-r-20">
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">5 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">4 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">3 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">2 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">1 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-auto">
                                <div class="well text-center h-100 d-flex align-items-center">
                                    <div class="m-l-10 m-r-10">
                                        <h4>Have you used this product?</h4>
                                        <p>Rate & review this product and help other customers</p>
                                        <a href="#addReview" data-target="#addReview" data-toggle="modal" class="btn blue">Write a product review</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="f-18 f-c w-400 m-0">User Reviews</h4>

                        <div class="grey-light-text f-12">Showing 7 of 7 Reviews</div>
                        <hr>
                        <?php for($i=0;$i<7;$i++){ ?>
                        <div class="user-review-item">
                            <div class="d-flex align-items-center m-b-10">
                                <div class="user-stars yellow-text f-21 l-1-0">
                                    <i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i>
                                </div>
                                <div class="user-rating-value">5/5</div>
                                <h4 class="review-title m-0">No regrets.</h4>
                            </div>

                            <div class="review-body">
                                <p>By MySmartPrice User</p>
                                <p>It's slim and handy. Its performance is excellent, and it also has a very soft touchscreen.</p>
                            </div>
                        </div>
                        <?php }?>
                    </div>

                    <div id="idTab2">

                        <div class="store-shoppinguide">
                            <div class="pad-20 d-flex align-items-center blue white-text store-shoppinguide-header">
                                <div class="store-guide-logo m-r-20">
                                    <img alt="img" src="img/fk-logo-2.jpg">
                                </div>
                                <div class="l-1-0 f-28 f-c">Flipcart Shopping Guide</div>

                                <div class="store-guide-author ml-auto">
                                    <div class="d-flex align-items-center">
                                        <figure class="store-guide-author-img"><img alt="img" src="https://randomuser.me/api/portraits/women/81.jpg">
                                        </figure>

                                        <div>
                                            <div class="f-12">Written by Category Specialist</div>
                                            <strong>Adam Hale</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pad-20 white m-b-20"><img alt="img" src="img/flipcart.png" class="img-full">
                            </div>

                            <div class="shopping-guide-title blue white-text pad-20">
                                <h4 class="m-0 l-1-0 f-26 w-400 f-c">How to Save at Flipcart Today</h4>
                            </div>
                            <div class="shopping-guide-body pad-20 white m-b-20">
                                <h3>What Can I Use a Marks and Spencer Voucher for?</h3>
                                <p>With a store as large as Marks and Spencer, you can bet there is plenty of opportunities to use your vouchers and offers! Check out just how you can save below:</p>
                                <ul>
                                    <li>
                                        <h4>Contemporary Classics</h4>
                                        <p>M&S is known for its high quality and classic style options for the whole family and is often the first choice for those looking for wardrobe staples like shirts, socks or smart shoes, and using one of our voucher codes is sure to put a spring in your step.</p>
                                    </li>

                                    <li>
                                        <h4>Bits, Sips, and other Culinary Delights</h4>
                                        <p>There’s a huge selection of food to order on the Marks and Spencer site. Check out their wow-your-guests party food, Sunday roasts to order and personalised cakes.</p>
                                    </li>

                                    <li>
                                        <h4>Floral Joys and Glorious Gifts</h4>
                                        <p>Marks and Spencer has a huge range of gifts for everyone. Shop gifts by interest, such as gardening or baking, by occasion, or by birthday age – ideal when you’re looking for that special something, and even better when there’s a voucher code too.</p>
                                    </li>

                                    <li>
                                        <h4>Decking Out your Home</h4>
                                        <p> Whether it’s time for new furniture or some smaller home décor purchases, M&S has it all covered. With our range of offers and vouchers, you can kit out your home for less.</p>
                                    </li>

                                    <li>
                                        <h4>Preening and Pampering</h4>
                                        <p>On top of their own brand ranges, Marks and Spencer offers a range of top skincare, makeup, haircare and toiletry brands such as Stila, L’Occitane, and Philip Kingsley. Next time you’re looking to pamper yourself, check out our codes first for a potential bargain.</p>
                                    </li>
                                </ul>

                                <h4>The M&S Seasonal Sales</h4>
                                <p>Marks and Spencer do frequent seasonal sales, slashing prices across their departments in their Easter, Summer and Winter sale events. These events discount products across all their departments, making them some of the best times to save! That’s not all though, they also have speciality events such as their Home Event where they cut the prices of nearly all their products in their homeware and furniture departments. So, roll your sleeves up and get ready to find some bargains with our top sale tips:</p>

                                <ol>
                                    <li>
                                        <h4>Shop Summer, Buy Winter</h4>
                                        <p>Just because it’s a summer sale doesn’t mean that winter coats won’t be on sale! By buying products off-season, you save yourself more money than if you bought it during the winter sale.</p>
                                    </li>

                                    <li>
                                        <h4>Make a List, and Stick To It</h4>
                                        <p>It’s very easy to get caught up with all the discounted items, especially with some of them being marked down by over 50% off. So, it’s best to make a list of what you need beforehand and stick to it, that way you don’t end up getting side-tracked and spending more than you should do.</p>
                                    </li>

                                    <li>
                                        <h4>Avoid Trends</h4>
                                        <p>Don’t buy something at discount for the sake of it. Avoid crazy trend-led pieces, and stick to the classics you can dress up or down, and what you know you’ll get good wear out of.</p>
                                    </li>

                                    <li>
                                        <h4>Stay Informed</h4>
                                        <p>Sign up to newsletters to be the first to know when the sales hit. The early bird catches the worm.</p>
                                    </li>
                                </ol>

                                <h4>How to Save at M&S</h4>
                                <p>Always paying attention to their customers’ needs, M&S created a wide range of time-limited seasonal discounts that come back throughout the year. Here are a few examples on how to save even more at Marks & Spencer:</p>

                                <ol>
                                    <li>
                                        <h4>Free Delivery</h4>
                                        <p>The famous department store offers free standard delivery on orders over £50, helping you to save on those dreaded delivery fees. Plus, delivery is free when you buy flowers, plants, food and wine gifts, and most furniture.</p>
                                    </li>

                                    <li>
                                        <h4>Click-and-Collect</h4> Click-and-collect allows you to use online vouchers that may not be available to you when checking out directly at the store.</li>

                                    <li>
                                        <h4>Sparks Loyalty Scheme</h4> Register for their loyalty card for free, and get points on your purchases. These points are then later converted into reward vouchers.</li>

                                    <li>
                                        <h4>Fshion Multibuys</h4> When you bundle together multiple items you receive fantastic deals. So, you will save on the higher price of buying clothing items individually and also build up your wardrobe.</li>

                                    <li>
                                        <h4>Get a £5 off M&S Voucher from Oxfam</h4> Give away M&S clothes to Oxfam to receive an M&S reward in exchange. It’s a win-win for everyone involved.</li>
                                </ol>
                            </div>


                            <div class="shopping-guide-title blue white-text pad-20">
                                <h4 class="m-0 l-1-0 f-26 w-400 f-c">How to Redeem Marks and Spencer Discount Codes</h4>
                            </div>
                            <div class="shopping-guide-body pad-20  white m-b-20">
                                <p>To redeem your code, take the following steps:</p>

                                <ol>
                                    <li>Browse the M&S website and add items to your basket</li>
                                    <li>Proceed to checkout</li>
                                    <li>Copy & paste a relevant code into the ‘Add Promotion Code' box and click the 'Apply' button, before confirming the payment</li>
                                    <li>Receive your M&S order at a bargain low price</li>
                                </ol>

                                <h4>Add promotion code</h4>
                                <form action="?" class="promotion-code f-16">
                                    <input type="text" class="input">
                                    <button class="btn blue">Apply</button>
                                </form>


                            </div>

                            <div class="shopping-guide-title blue white-text pad-20">
                                <h4 class="m-0 l-1-0 f-26 w-400 f-c">Our Popular Marks and Spencer Vouchers and Codes</h4>
                            </div>
                            <div class="shopping-guide-body pad-20  white m-b-20">
                                <p>Marks and Spencer pride themselves on providing great value for money and strive to give customers low prices wherever possible. If there is a deal to be had, you will find it right here on MyVoucherCodes. Here are a few of the best promo codes and M&S deals we’ve had in the past:</p>

                                <ul class="w-700">
                                    <li>20% off Clothing, Home and Beauty</li>
                                    <li>10% off first orders</li>
                                    <li>£5 off Food When you Spend £35</li>
                                    <li>20% off the Outlet on Orders Over £25 at Marks and Spencer - M&S</li>
                                    <li>Save up to 50% on champagne at Marks and Spencer</li>
                                </ul>


                            </div>
                            <div class="shopping-guide-title blue white-text pad-20">
                                <h4 class="m-0 l-1-0 f-26 w-400 f-c">FAQs</h4>
                            </div>
                            <div class="shopping-guide-body pad-20  white m-b-20">
                                <h4>Where Is My Voucher Code?</h4>
                                <p>Your voucher code should pop up in a separate box for you to copy & paste. If you have clicked a deal, you might not need a specific code but you should have been redirected to the relevant page.</p>
                                <h4>Why Isn’t My Voucher Code Working?</h4>
                                <p>If your code isn’t working, you’ve probably clicked on one that has expired! Here are the possible explanations for a non-functional voucher:
                                </p>
                                <ol>
                                    <li>The voucher is out of date or has expired</li>
                                    <li>The code is valid only on a specific range of products or a specific item</li>
                                    <li>The code doesn’t meet the store’s requirements (It could be that the discount was only valid for new customers, or for the first 100 customers, etc,…)</li>
                                </ol>
                                <h4>Can I Use M&S Voucher Codes and Offers at the Same Time?</h4>
                                <p>Unless specified, it is quite unlikely that you will be able to combine a code and a generic discount or offer. But we never said you can’t try!</p>
                                <h4>How Long Does an M&S Code Last for?</h4>
                                <p>It generally varies a lot. Some codes will last until the end of the year whilst others will only last for a week. Always check the terms & conditions to make sure the code you picked is indeed valid.</p>
                                <h4>What is Sparks?</h4>
                                <p>Sparks is Marks and Spencer’s own loyalty scheme program that enables you to collect points that go towards reward vouchers every time you make a purchase.</p>
                                <h4>Can I Return My Item if I’ve Used a Code at Checkout?</h4>
                                <p>Yes, you will then get refunded for the amount of money you have effectively paid.</p>
                                <h4>M&S Black Friday Season</h4>
                                <p>With the growing popularity of Black Friday year after year, Marks & Spencer pushes the bar higher and works harder to offer its customers the best possible discounts. See for yourself on our Black Friday page.</p>
                                <h4>What are the Delivery Options at Marks?</h4>
                                <p>There are a variety of delivery options with Marks, next day delivery is available for orders made as late 10pm, standard delivery can be organised and will take up to 5 working days, or get delivery on an appointed day. To collect in store, order by 8pm on the previous day.</p>
                                <h4>And How Do Returns and Refunds Work?</h4>
                                <p>Excluding furniture, and sale items - which are only refundable within 14 days of sale, M&S' goodwill refunds mean you can get your money back 35 days after purchase.</p>


                            </div>
                            <div class="shopping-guide-title blue white-text pad-20">
                                <h4 class="m-0 l-1-0 f-26 w-400 f-c">Contact</h4>
                            </div>
                            <div class="shopping-guide-body pad-20  white m-b-20">
                                <div class="d-flex">
                                    <div>Address<br> Marks and Spencer Group plc<br> Waterside House<br> 35 North Wharf Road<br> London W2 1NW<br>
                                    </div>
                                    <div class="m-l-50">
                                        Telephone<br> 0333 014 8555
                                    </div>
                                </div>
                            </div>

                            <div class="shopping-guide-title blue white-text pad-20">
                                <h4 class="m-0 l-1-0 f-26 w-400 f-c">Social Links</h4>
                            </div>
                            <div class="shopping-guide-body pad-20  white m-b-20 ">
                                <div class="store-social-links f-26 d-flex align-items-center">
                                    <a href=""><i class="material-icons green-text">home</i></a>
                                    <a href=""><i class="material-icons yellow-text">email</i></a>
                                    <a href="" class="f-22"><i class="socicon-facebook blue-text"></i></a>
                                    <a href=""><i class="socicon-twitter cyan-text"></i></a>
                                    <a href="" class="f-30"><i class="socicon-googleplus red-text"></i></a>
                                    <a href=""><i class="socicon-youtube red-text"></i></a>
                                    <a href=""><i class="socicon-pinterest red-text"></i></a>
                                    <a href=""><i class="socicon-apple"></i></a>
                                    <a href=""><i class="socicon-wikipedia black-text"></i></a>
                                </div>
                            </div>
                            <div class="shopping-guide-body pad-20  white m-b-20">
                                <h3>Archived Marks and Spencer Discount Codes</h3>
                                <table class="table table-border">
                                    <thead>
                                        <tr>
                                            <td>Issued</td>
                                            <td>Code Description</td>
                                            <td>Code</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>31/12/2017</td>
                                            <td>Up to 50% off Men's Coats</td>
                                            <td>*******</td>
                                        </tr>
                                        <tr>
                                            <td>31/12/2017</td>
                                            <td>Up to 50% off Men's Coats</td>
                                            <td>*******</td>
                                        </tr>
                                        <tr>
                                            <td>31/12/2017</td>
                                            <td>Up to 50% off Men's Coats</td>
                                            <td>*******</td>
                                        </tr>
                                        <tr>
                                            <td>31/12/2017</td>
                                            <td>Up to 50% off Men's Coats</td>
                                            <td>*******</td>
                                        </tr>
                                        <tr>
                                            <td>31/12/2017</td>
                                            <td>Up to 50% off Men's Coats</td>
                                            <td>*******</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>




                        </div>

                        
                    </div>

                </div>
                
                
                

            </div>
            <div class="store-descriptions pad-20 m-b-20">
                            <h3 class="f-18 f-c w-400">About Flipkart</h3>
                            <p>Founded by Sachin Bansal and Binny Bansal in 2007, Flipkart is one of the first online shopping sites to have a major breakthrough in India. With its primary focus on customer experience and satisfaction, the Bangalore-based company has won over customers in a very short period of time. Flipkart started with selling books in 2007 and slowly expanded its inventory to accommodate mobiles, electronics, fashion, home appliances, kitchen essentials, baby products, automobile parts, sports equipment and more. Flipkart is the largest e-commerce company in India in terms of Gross Merchandise Value. After establishing its authority in the Indian e-commerce segment, the online store has made significant gains in the fashion side of online shopping with important acquisitions of Myntra and Jabong. The Flipkart-Myntra deal which happened in 2014 was estimated at $300 million, making it one of the biggest online acquisition deals in the recent times. Flipkart coupons available from time to time help you save more on online purchases at the online store. The biggest sale of Flipkart is the Big Billion Day Sale which had its inception in 2014. The sale which usually happens in the month of October, just before Diwali season draws thousands of online shoppers looking for products at steal deal prices. The 2016 Flipkart Big Billion Days was a huge success where the company raked in big numbers and comprehensively beat Amazon India. With a huge assortment of products, Flipkart offers good discounts on many of the products on the site. You can make use of Flipkart coupon code to get added discounts on purchases. Additionally, download the Flipkart app to get exclusive app-only offers and deals. The Flipkart app offer gives more savings on purchases at Flipkart during online sales and festive times. Also, the Flipkart app is one of the most downloaded online shopping apps from the Apple store and Google Play store. Flipkart is looking to raise a fresh funding round with Wal-Mart likely to pick up a small stake in the company. The battle for supremacy in the e-commerce segment in India is only getting fiercer and Flipkart with its newly loaded ammo looks all set to take on the likes of Amazon, Snapdeal and Shopclues. However, all is good for online shoppers looking for big discounts and exciting offers. In addition to getting discounts using Flipkart promo code, users can visit Flipkart through CashKaro to get additional rewards. These rewards can be converted into Flipkart Gift Vouchers that can be used for further shopping. It is time to go shopping crazy!</p>

                            <hr>

                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/uc4gDH6wX4Y" allowfullscreen></iframe>
                            </div>
                        </div>
        </div>
    </div>
    <?php include('includes/footer.php'); ?>
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>

    <div class="activate-reward unsigned">
        <button class="close white-text">&times;</button>
        <div class="text-center blue  white-text pad-15"><a href="#" class="yellow-text"> Existing User Sign In <i class="fas fa-chevron-right"></i></a>
            <div class="f-30 f-c l-1-2 m-t-20 m-b-30">1 step from earning upto 7.50% <br> CashKaro Rewards on every Flipkart order</div>
            <a href="" class="btn orange btn-activate">JOIN TO ACTIVATE REWARDS</a>

            <div class="f-16 w-700 m-t-20">Why do I need to join?</div>
        </div>

        <div class="activate-login-benifits">
            <ul class="list-bordered">

                <li>
                    <div class="row m-t-15 m-b-15">
                        <div class="col-auto">
                            <i class="material-icons l-1-0 red-text f-30 m-r-10 m-l-10">account_balance_wallet</i>
                        </div>
                        <div class="col">
                            <h5 class="f-15 m-0 red-text w-400">LOGIN, SO WE KNOW WHO TO PAY</h5> Join CashKaro or Login so Rewards can automatically be added to your CashKaro account</div>
                    </div>
                </li>
                <li>
                    <div class="row m-t-15 m-b-15">
                        <div class="col-auto">
                            <i class="material-icons l-1-0 blue-text f-30 m-r-10 m-l-10">local_offer</i>
                        </div>
                        <div class="col">
                            <h5 class="f-15 m-0 blue-text w-400">GET PAID TO SHOP</h5> We actually pay you Cash Back every time you shop at our 1500+ partner sites</div>
                    </div>
                </li>
                <li>
                    <div class="row m-t-15 m-b-15">
                        <div class="col-auto">
                            <i class="material-icons l-1-0 green-text f-30 m-r-10 m-l-10">monetization_on</i>
                        </div>
                        <div class="col">
                            <h5 class="f-15 m-0 green-text w-400">NOTHING TALKS LIKE HARD CASH</h5> Our Rewards is Real Money – Transfer to your Bank Account or take Shopping vouchers</div>
                    </div>
                </li>
                <li>
                    <div class="row m-t-15 m-b-15">
                        <div class="col-auto">
                            <i class="material-icons l-1-0 mag-text f-30 m-r-10 m-l-10">check_circle</i>
                        </div>
                        <div class="col">
                            <h5 class="f-15 m-0 mag-text w-400">BEST DEAL GUARANTEE</h5> Find a better deal online, and we shall pay the difference</div>
                    </div>
                </li>
            </ul>

            <div class="m-t-25"><a href="" class="btn orange btn-activate">JOIN TO ACTIVATE REWARDS</a>
                <a href="" class="btn grey-light btn-activate">CONTINUE &amp; LOSE REWARDS</a>
            </div>


        </div>

    </div>

    <div class="activate-reward signed">
        <button class="close">&times;</button>
        <div class="text-center pad-20 grey-lighter-2">
            <img src="img/thumbs-up.svg" width="150" height="150" class="m-b-20" alt="">
            <h2 class="m-0 f-25">Rewards Activated.</h2>
            <div class="f-16 m-t-5">Shop at Flipkart & Earn
                <div class="orange-text">Upto 7.50% CashKaro Rewards</div>
            </div>
        </div>
        <div class="activate-login-benifits ">
            <a href="" class="btn orange btn-activate">VISIT store</a>
            <h3 class="f-18 blue-text m-t-20 text-center w-400 collapsed" data-toggle="collapse" data-target="#collapse1">Important Steps to ensure your Rewards Tracks <i class="fas fa-angle-down "></i></h3>
            <div class="list-important-steps">
                <div class="collapse" id="collapse1">
                    <ul class="list-star">
                        <li><i class="fas fa-star"></i> Add products to your Flipkart cart only AFTER clicking out from CashKaro & shop in same session</li>
                        <li><i class="fas fa-star"></i> Rewards will be paid for transactions on Flipkart Mobile App, Mobile site and Desktop site</li>
                        <li><i class="fas fa-star"></i> Rewards can only be redeemed via Gift Vouchers</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="toggle-body pop-fixed pop-float-left pop" id="rewardRateTerms">
        <div class="pop-title">
            TERMS & CONDITIONS
            <button type="button" class="close">×</button>
        </div>
        <div class="pop-body">
            <h5>store Specific Terms</h5>
            <ul class="f-12">
                <li>Add products to your Flipkart cart only AFTER clicking out from CashKaro & shop in same session</li>
                <li>Rewards will be paid for transactions on Flipkart Mobile App, Mobile site and Desktop site</li>
                <li>Rewards can only be redeemed via Gift Vouchers</li>
                <li>No Rewards are paid if there if your complete order is cancelled due to any reason</li>
            </ul>
            <h5>General Rewards Terms</h5>
            <ul class="f-12">
                <li>To ensure your Rewards tracks, do not visit any other coupon or price comparison site after clicking-out from CashKaro</li>
                <li>Only use Coupon Codes mentioned on CashKaro.com</li>
                <li>For all Rewards queries, please email contact@cashkaro.com. Do not contact the store</li>
                <li>Your Rewards will be added in Pending Status within 72 hours of completing the Transaction</li>
                <li>Your Rewards will get Confirmed within 90 days from Transaction Date</li>
                <li>If your Rewards does not track automatically, please add a Missing Rewards ticket within 10 days from Transaction Date</li>
            </ul>
        </div>
    </div>
    <div class="modal fade " id="hiw" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header justify-content-center">
                    <h5 class="modal-title f-23 f-c l-1-0 text-center blue-text m-auto">Earn Cash Every Time You Shop!</h5>
                        <button type="button" class="close clos-abs" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
                </div>
                <div class="modal-body">
                    
                    <div class="row m-t-20 m-b-10 text-center align-items-center f-12 green-text">
                        <div class="col"><div><i class="f-30 fas fa-shopping-cart"></i></div></div>
                        <div class="col-auto"><i class="fas f-16 fa-angle-right"></i></div>
                        <div class="col"><div><i class="f-30 fas fa-money-bill-alt"></i></div></div>
                            <div class="col-auto"><i class="fas f-16 fa-angle-right"></i></div>
                        <div class="col"><div><i class="f-30 far fa-user-circle"></i></div></div>
                            <div class="col-auto"><i class="fas f-16 fa-angle-right"></i></div>
                        <div class="col"><div><i class="f-30 far fa-building"></i></div></div>
                    </div> 
                    <div class="row m-b-20 text-center align-items-center f-12">
                        <div class="col">Shop via CouponDunia</div>
                        <div class="col">Store pays us commission</div>
                        <div class="col">We pay you cash</div>
                        <div class="col">Withdraw cash to bank/paytm/GVs</div>
                    </div>                                         
                   <hr>
                    <div class="f-20 text-center m-b-10">It's that simple!</div>
                    <div class="text-center m-b-10"><a href="" class="btn blue">Sign up now</a>
                    <a href="" class="btn blue">Back to offers</a></div>
                    
                    <div class="text-center"><a href="">More about how it works</a></div>
                </div>
            </div>
        </div>
    </div>   
                
    <div class="modal fade " id="terms" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header justify-content-center">
                    <h5 class="modal-title f-23 f-c l-1-0 text-center blue-text m-auto">THINGS YOU MUST KNOW TO EARN EXTRA CASHBACK</h5>
                    <button type="button" class="close clos-abs" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                


                </div>
                <div class="modal-body pad-0 terms-body">

                    <div class="pad-20">
                        <h3 class="text-center f-25 f-c w-400 m-b-20">Terms</h3>
                        <div class="">
                            <div class="m-l-20"><ul class="list">
                                <li>Reward Credits are not applicable on bulk orders</li>
                                <li> Reward Credit is not available on VAT, Delivery and any Administrative charges. <p></p>
                                </li><li>Some of the reward rates vary for new &amp; existing Flipkart users. </li>
                                <li>New Flipkart customer is defined as the 1st order/transaction done by a customer from Flipkart.com in the last 6 months.</li><li>Sitaphal Rewards won't be available on orders paid with PhonePe amount.</li>
                            </ul></div>

                            <p>Please note : The Payment is subject to Tracking Confirmation by the store. Cashback/ Rewards will not be paid on Bulk Orders. This service is meant for Individual/ Personal use only.</p>
                        </div>

                    </div>

                    <div class="grey-lighter-2 pad-20">

                        <h3 class="text-center f-25 f-c w-400 m-b-20">Steps to Earn Extra Cashback</h3>

                        <div class="row justify-content-center text-center">

                            <div class="col-md-8 d-flex">
                                <div class="col">
                                    <div class="circle f-20 green white-text mr-auto ml-auto m-b-10">
                                        <div class="fas fa-mouse-pointer"></div>
                                    </div>
                                    Click
                                </div>
                                <div class="col">
                                    <div class="circle f-20 green white-text mr-auto ml-auto m-b-10">
                                        <div class="fas fa-globe"></div>
                                    </div>
                                    Visit store
                                </div>
                                <div class="col">
                                    <div class="circle f-20 green white-text mr-auto ml-auto m-b-10">
                                        <div class="fas fa-shopping-cart"></div>
                                    </div>
                                    Purchase
                                </div>
                                <div class="col">
                                    <div class="circle f-20 green white-text mr-auto ml-auto m-b-10">
                                        <div class="fas fa-rupee-sign"></div>
                                    </div>
                                    Get Paid
                                </div>
                            </div>

                        </div>
                        <div class="text-center f-12 m-t-20">Note:- Sitaphal.com pay you the Extra Cashback/ Rewards, not the store.</div>

                    </div>
                    <div class="pad-20">
                        <h3 class="text-center f-25 f-c w-400 m-b-20">Instructions</h3>
                        <ul class="list-unstyled f-16">
                            <li class=" red-text m-b-10">
                                <div class="circle-stroke red-border f-40 m-r-5"><i class="fas fa-ban f-23"></i>
                                </div> Do not use Ad blockers/ browser extensions that would block tracking your orders</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-sign-in-alt f-23"></i>
                                </div> Please login, Click our Links to visit the store & Place order</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-shopping-cart f-23"></i>
                                </div> Add products to cart only after you visit the store (click out) from Sitaphal.com</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-gift f-23"></i>
                                </div> Do not use Gift Voucher or Wallet Amount or Loyalty Points to place your order</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-clone f-23"></i>
                                </div> After Click out, place order in one session without visiting other websites</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-mouse-pointer f-23"></i>
                                </div> You must do a Click out from Sitaphal everytime you place a new order</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-credit-card f-23"></i>
                                </div> If payment fails in first attempt, please empty cart & click out again & place order</li>
                        </ul>
                    </div>



                    <div class="blue white-text text-center pad-20">
                        <h3 class="text-center f-25 f-c w-400 m-b-20">Tracking & Payment Information</h3>


                        <div class="col-md-8 mr-auto ml-auto">
                            <div class="row">
                                <div class="col">
                                    <div class="f-18 d-flex align-items-center justify-content-center"><i class="material-icons f-26 l-1-0 m-r-5">av_timer</i>1 day </div>
                                    <div>Tracking Speed</div>
                                </div>
                                <div class="col">
                                    <div class="f-18 d-flex align-items-center justify-content-center"><i class="material-icons f-26 l-1-0 m-r-5">date_range</i>4 - 9 weeks </div>
                                    <div>Estimated Payment</div>
                                </div>
                                <div class="col">
                                    <div class="f-18 d-flex align-items-center justify-content-center"><i class="material-icons f-26 l-1-0 m-r-5">history</i>1 day </div>
                                    <div>Tracking Reliability</div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <a href="">Cahback FAQs</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade " id="addReview" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header justify-content-center">
                    <h5 class="modal-title f-16 f-c w-400 l-1-0 text-center blue-text m-auto">Add a review</h5>
                    <button type="button" class="close clos-abs" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                


                </div>
                <div class="modal-body pad-0 terms-body">
                    <div class="pad-20">
                        <div class="m-b-10">Adding rating as <strong>Spongebob</strong></div>
                        <div class="m-b-10"><input type="text" placeholder="Review title" class="input w-100"></div>
                        <div class="m-b-10">
                            <div class="star-rater f-32">
                                <label class="material-icons">star<input type="radio" name="rate" value="5"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="4"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="3"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="2"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="1"></label>
                            </div>
                        </div>
                        <div class="m-b-10"><textarea class="input w-100" rows="5" placeholder="Insert your rewiew here"></textarea></div>
                        
                        <button class="btn blue">Submit Rating</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    
</body>

</html>