<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab d-flex xs-tab-2">
                <li><a href="">Account Summary</a></li>
                <li><a href="">Activity</a></li>
                <li><a href="">Payments</a></li>
                <li class="current"><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                
                <div class="row m-b-40">
                    <div class="col-md-3">
                        <h4 class="m-0 f-16">Total earned</h4>
                        <hr class="m-t-10 m-b-10">
                        <span class="f-32">£0.00 </span>
                        
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-0 f-16">People referred</h4>
                        <hr class="m-t-10 m-b-10">
                        <span class="f-32">0 </span>
                        
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-0 f-16">Total clicks</h4>
                        <hr class="m-t-10 m-b-10">
                        
                        <span class="f-32">0</span> 
                        
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-0 f-16">Total shared</h4>
                        <hr class="m-t-10 m-b-10">
                       
                        <span class="f-32">£0.00</span> 
                        
                    </div>
                    
                </div>
                
                <h2 class="f-16 blue-text">What do you want to refer?</h2>
                <div class="m-b-20 border-bottom">
                   <ul class="tab-tab idTabs d-flex tab-inline">
                    <li><a href="#idTab1">askmeoffer</a></li>
                    <li><a href="#idTab2">A store on askmeoffer</a></li>
                </ul></div>
                <div id="idTab1"></div>
                <div id="idTab2">
                    <input type="text" class="input m-b-20" size="50" placeholder="Start typing a store name ie. ASOS...">
                </div>
                <div class="collapse show generateLink"><h3 class="f-14 m-b-5 m-t-0">How much would you like to share with your friends?</h3>
                <div>
                  <div id="slider"></div>
                </div></div>
                <a href=".generateLink" data-toggle="collapse" class="btn blue m-b-20">Generate your referal link</a>
                
                <div class="collapse generateLink">
                    <div class="pad-20 border rounded d-inline-block"><h3 class="f-16 m-t-0">How do you want to share us?</h3>
                    <div class="row">
                        <div class="col-auto">
                            <h4 class="blue-text">Share this link</h4>
                            Your referral link is
                            <input type="text" class="input d-block" size="45" value="#">
                        </div>
                        <div class="col-auto text-center d-flex align-items-center">
                            <div class="m-r-20 m-l-20">or</div>
                        </div>
                        <div class="col-auto text-center">
                            <h4 class="blue-text m-r-20 m-l-20">Share via your social networks</h4>
                            <a href="" class="btn cyan m-b-10 d-block">Share on twitter</a>
                            <a href="" class="btn blue d-block">Share on facebook</a>
                        </div>
                    </div></div>
                </div>
                
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script>
  $( function() {
        var tooltip = $('<div class="popover ref-popover bs-popover-top"><div class="arrow"></div><div class="popover-body"><div class="ref-value"><div class="row"><div class="col">You get<div class="you-get f-22 bold">$<span>10</span></div></div><div class="col-auto"><div class="vr"></div></div><div class="col">They get<div class="they-get f-22 bold">$<span>0</span></div></div></div></div></div></div>');
        $("#slider").slider({
            value: 0,
            min: 0,
            max: 10,
            step: 0.25,
            slide: function(event, ui) {
                tooltip.find('.they-get>span').text(ui.value);
                tooltip.find('.you-get>span').text(10-ui.value);
            },
            change: function(event, ui) {}
        }).find(".ui-slider-handle").append(tooltip);
  } );
  </script>
</body>

</html>