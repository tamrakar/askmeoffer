<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        <div class="white border pad-15">
         
                   <div class="row justify-content-between m-b-15">
                    <div class="col"><h1 class=" m-0 f-22 d-inline-block  w-400 f-c">Exclusive</h1> <span class="f-14">(Showing 1 – 14 products of 14 products)</span></div>          
                   
                    
                    <div class="col-auto d-inline-flex align-items-center"><label class="m-b-0 m-r-10">Sort by</label> 
                        <select name="" id="" class="input">
                            <option>Relevance</option>
                            <option>Price: Low to high</option>
                            <option>Price: High to low</option>
                            <option>Avg. Customer review    </option>
                        </select></div>
                     </div> 
                      
                       <div class="row">
                        <?php for($i=0;$i<30;$i++){ ?>
                        <div class="col-6 col-lg-2 col-product m-b-10">
                            <div class="coupon-deal-item store-product-item border">
                                <figure><a href="#"><img alt="img" src="img/i-4.png"></a>
                                </figure>
                                
                                <div class="coupon-info">
                                <div class="f-14 w-700 m-b-5 line-1 "><a href="#">Apple iPhone 6s (32 GB) Apple iPhone 6s (32 GB) Apple iPhone 6s (32 GB)</a></div>
                                
                                <div class="d-flex justify-content-center align-items-center line-sep"> <span class="f-14">Now </span><span class="f-17 m-r-15 m-l-5 w-700">₹20.999</span><span class="grey-text f-14 text-strike">₹40.999</span> </div>
                                
                                
                                

                                    <div class="d-flex line-sep justify-content-between align-items-center"> 
                                    <span><img alt="img" src="img/fk-logo-2.jpg" height="18"></span> 
                                    <span><span class="material-icons yellow-text f-22">star</span> <span class="align-middle">3.5</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        
                    </div>
                    
                    <nav aria-label="Page navigation example">
                      <ul class="pagination justify-content-center">
                        <li class="page-item">
                          <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                          </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                          <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                
                </div>
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
     <?php include('includes/lang-list.php'); ?>
     <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>