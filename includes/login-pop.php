<div class="modal fade " id="login-pop" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-signin" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-400 f-16 m-0">SIGN IN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            
            </div>
            
            <div class="row no-gutters align-items-center">
                <div class="col-12 col-lg-5 d-none d-md-block border-right">
                    <div class="pad-30 m-t-20 m-b-20">
                        <ul class="list-unstyled f-15 l-1-9">
                            <li><a href="" class="grey-text"><i class="fas fa-ticket-alt fa-fw orange-text m-r-5"></i>My Coupons</a></li>
                            <li><a href="" class="grey-text"><i class="fas fa-th fa-fw orange-text m-r-5"></i>My Coupons + Deals</a></li>
                            <li><a href="" class="grey-text"><i class="fas fa-star fa-fw orange-text m-r-5"></i>My Wishlists</a></li>
                            <li><a href="" class="grey-text"><i class="fas fa-heart fa-fw orange-text m-r-5"></i>My Favorites </a></li>
                        </ul>
                        <hr>
                        
                        <ul class="list-unstyled f-12 l-1-9">
                        <li><a href="" class="grey-text">Account / DashBoard</a></li>
                        <li><a href="" class="grey-text">Deal Preferences</a></li>
                        <li><a href="" class="grey-text">Redeem Gift Card</a></li>
                        <li><a href="" class="grey-text">Refer Friends, Get $10*</a></li>
                        <li><a href="" class="grey-text">Not a Member? Sign Up</a></li>
</ul>
                    </div>
                </div>
                <div class="col-12 col-lg-7">
                    <div class="pad-30">
                    <div class="m-b-10"><input type="text" class="input w-100" placeholder="Enter email"></div>
                    <div class="m-b-10"><input type="password" class="input w-100" placeholder="Your password"></div>
                    
                    <div class="row f-12 m-b-10">
                        <div class="col">
                            <label class="d-flex align-items-center">Keep me signed in <input type="checkbox" class="m-l-10"></label>
                        </div>
                        <div class="col text-right">
                            <a href="">Forgot password</a>
                        </div>
                    </div>
                    
                    <button type="submit" class="btn orange d-block w-100 f-16">Sign In</button>
                    
                    
                    <div class="text-center m-t-10  f-12">
                        New to askmeoffer? <a href="#">SIGN UP</a>
                    </div>
                    <hr>
                       <div class="text-center"><div class=" m-b-5">Or sign in with</div>

                        <a href="" class="btn red d-block m-b-10 f-16"><i class="fab fa-google-plus-g m-r-5"></i>Google</a>
                        <a href="" class="btn blue d-block f-16"><i class="fab fa-facebook-f m-r-5"></i>Facebook</a>
                    </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
</div>