<div class="modal fade " id="language-switch" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-400 f-16 m-0">Choose language</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            
            </div>
            <div class="modal-body">
                <ul class="lang-list">
                    <li><a href="/wiki/Afrikaans" title="Afrikaans">Afrikaans</a>
                    </li>
                    <li><a href="/wiki/Albanian_language" title="Albanian language">Albanian</a>
                    </li>
                    <li><a href="/wiki/Amharic" title="Amharic">Amharic</a>
                    </li>
                    <li><a href="/wiki/Arabic" title="Arabic">Arabic</a>
                    </li>
                    <li><a href="/wiki/Armenian_language" title="Armenian language">Armenian</a>
                    </li>
                    <li><a href="/wiki/Azerbaijani_language" title="Azerbaijani language">Azerbaijani</a>
                    </li>
                    <li><a href="/wiki/Basque_language" title="Basque language">Basque</a>
                    </li>
                    <li><a href="/wiki/Belarusian_language" title="Belarusian language">Belarusian</a>
                    </li>
                    <li><a href="/wiki/Bengali_language" title="Bengali language">Bengali</a>
                    </li>
                    <li><a href="/wiki/Bosnian_language" title="Bosnian language">Bosnian</a>
                    </li>
                    <li><a href="/wiki/Bulgarian_language" title="Bulgarian language">Bulgarian</a>
                    </li>
                    <li><a href="/wiki/Burmese_language" title="Burmese language">Burmese</a>
                    </li>
                    <li><a href="/wiki/Catalan_language" title="Catalan language">Catalan</a>
                    </li>
                    <li><a href="/wiki/Cebuano_language" title="Cebuano language">Cebuano</a>
                    </li>
                    <li><a href="/wiki/Chewa_language" title="Chewa language">Chichewa</a>
                    </li>
                    <li><a href="/wiki/Standard_Chinese" title="Standard Chinese">Chinese</a>
                    </li>
                    <li><a href="/wiki/Corsican_language" title="Corsican language">Corsican</a>
                    </li>
                    <li><a href="/wiki/Croatian_language" title="Croatian language">Croatian</a>
                    </li>
                    <li><a href="/wiki/Czech_language" title="Czech language">Czech</a>
                    </li>
                    <li><a href="/wiki/Danish_language" title="Danish language">Danish</a>
                    </li>
                    <li><a href="/wiki/Dutch_language" title="Dutch language">Dutch</a>
                    </li>
                    <li><a href="/wiki/English_language" title="English language">English</a>
                    </li>
                    <li><a href="/wiki/Esperanto" title="Esperanto">Esperanto</a>
                    </li>
                    <li><a href="/wiki/Estonian_language" title="Estonian language">Estonian</a>
                    </li>
                    <li><a href="/wiki/Filipino_language" title="Filipino language">Filipino</a>
                    </li>
                    <li><a href="/wiki/Finnish_language" title="Finnish language">Finnish</a>
                    </li>
                    <li><a href="/wiki/French_language" title="French language">French</a>
                    </li>
                    <li><a href="/wiki/West_Frisian_language" title="West Frisian language">Frisian</a>
                    </li>
                    <li><a href="/wiki/Galician_language" title="Galician language">Galician</a>
                    </li>
                    <li><a href="/wiki/Georgian_language" title="Georgian language">Georgian</a>
                    </li>
                    <li><a href="/wiki/German_language" title="German language">German</a>
                    </li>
                    <li><a href="/wiki/Greek_language" title="Greek language">Greek</a>
                    </li>
                    <li><a href="/wiki/Gujarati_language" title="Gujarati language">Gujarati</a>
                    </li>
                    <li><a href="/wiki/Haitian_Creole" title="Haitian Creole">Haitian Creole</a>
                    </li>
                    <li><a href="/wiki/Hausa_language" title="Hausa language">Hausa</a>
                    </li>
                    <li><a href="/wiki/Hawaiian_language" title="Hawaiian language">Hawaiian</a>
                    </li>
                    <li><a href="/wiki/Hebrew_language" title="Hebrew language">Hebrew</a>
                    </li>
                    <li><a href="/wiki/Hindi" title="Hindi">Hindi</a>
                    </li>
                    <li><a href="/wiki/Hmong_language" title="Hmong language">Hmong</a>
                    </li>
                    <li><a href="/wiki/Hungarian_language" title="Hungarian language">Hungarian</a>
                    </li>
                    <li><a href="/wiki/Icelandic_language" title="Icelandic language">Icelandic</a>
                    </li>
                    <li><a href="/wiki/Igbo_language" title="Igbo language">Igbo</a>
                    </li>
                    <li><a href="/wiki/Indonesian_language" title="Indonesian language">Indonesian</a>
                    </li>
                    <li><a href="/wiki/Irish_language" title="Irish language">Irish</a>
                    </li>
                    <li><a href="/wiki/Italian_language" title="Italian language">Italian</a>
                    </li>
                    <li><a href="/wiki/Japanese_language" title="Japanese language">Japanese</a>
                    </li>
                    <li><a href="/wiki/Javanese_language" title="Javanese language">Javanese</a>
                    </li>
                    <li><a href="/wiki/Kannada" title="Kannada">Kannada</a>
                    </li>
                    <li><a href="/wiki/Kazakh_language" title="Kazakh language">Kazakh</a>
                    </li>
                    <li><a href="/wiki/Khmer_language" title="Khmer language">Khmer</a>
                    </li>
                    <li><a href="/wiki/Korean_language" title="Korean language">Korean</a>
                    </li>
                    <li><a href="/wiki/Northern_Kurdish" title="Northern Kurdish">Kurdish</a>
                    </li>
                    <li><a href="/wiki/Kyrgyz_language" title="Kyrgyz language">Kyrgyz</a>
                    </li>
                    <li><a href="/wiki/Lao_language" title="Lao language">Lao</a>
                    </li>
                    <li><a href="/wiki/Latin" title="Latin">Latin</a>
                    </li>
                    <li><a href="/wiki/Latvian_language" title="Latvian language">Latvian</a>
                    </li>
                    <li><a href="/wiki/Lithuanian_language" title="Lithuanian language">Lithuanian</a>
                    </li>
                    <li><a href="/wiki/Luxembourgish_language" class="mw-redirect" title="Luxembourgish language">Luxembourgish</a>
                    </li>
                    <li><a href="/wiki/Macedonian_language" title="Macedonian language">Macedonian</a>
                    </li>
                    <li><a href="/wiki/Malagasy_language" title="Malagasy language">Malagasy</a>
                    </li>
                    <li><a href="/wiki/Malay_language" title="Malay language">Malay</a>
                    </li>
                    <li><a href="/wiki/Malayalam" title="Malayalam">Malayalam</a>
                    </li>
                    <li><a href="/wiki/Maltese_language" title="Maltese language">Maltese</a>
                    </li>
                    <li><a href="/wiki/M%C4%81ori_language" title="Māori language">Maori</a>
                    </li>
                    <li><a href="/wiki/Marathi_language" title="Marathi language">Marathi</a>
                    </li>
                    <li><a href="/wiki/Mongolian_language" title="Mongolian language">Mongolian</a>
                    </li>
                    <li><a href="/wiki/Nepali_language" title="Nepali language">Nepali</a>
                    </li>
                    <li><a href="/wiki/Norwegian_language" title="Norwegian language">Norwegian</a>
                    </li>
                    <li><a href="/wiki/Pashto" title="Pashto">Pashto</a>
                    </li>
                    <li><a href="/wiki/Persian_language" title="Persian language">Persian</a>
                    </li>
                    <li><a href="/wiki/Polish_language" title="Polish language">Polish</a>
                    </li>
                    <li><a href="/wiki/Portuguese_language" title="Portuguese language">Portuguese</a>
                    </li>
                    <li><a href="/wiki/Punjabi_language" title="Punjabi language">Punjabi</a>
                    </li>
                    <li><a href="/wiki/Romanian_language" title="Romanian language">Romanian</a>
                    </li>
                    <li><a href="/wiki/Russian_language" title="Russian language">Russian</a>
                    </li>
                    <li><a href="/wiki/Samoan_language" title="Samoan language">Samoan</a>
                    </li>
                    <li><a href="/wiki/Scottish_Gaelic" title="Scottish Gaelic">Scots Gaelic</a>
                    </li>
                    <li><a href="/wiki/Serbian_language" title="Serbian language">Serbian</a>
                    </li>
                    <li><a href="/wiki/Sotho_language" title="Sotho language">Sesotho</a>
                    </li>
                    <li><a href="/wiki/Shona_language" title="Shona language">Shona</a>
                    </li>
                    <li><a href="/wiki/Sindhi_language" title="Sindhi language">Sindhi</a>
                    </li>
                    <li><a href="/wiki/Sinhalese_language" title="Sinhalese language">Sinhala</a>
                    </li>
                    <li><a href="/wiki/Slovak_language" title="Slovak language">Slovak</a>
                    </li>
                    <li><a href="/wiki/Slovene_language" title="Slovene language">Slovenian</a>
                    </li>
                    <li><a href="/wiki/Somali_language" title="Somali language">Somali</a>
                    </li>
                    <li><a href="/wiki/Spanish_language" title="Spanish language">Spanish</a>
                    </li>
                    <li><a href="/wiki/Sundanese_language" title="Sundanese language">Sundanese</a>
                    </li>
                    <li><a href="/wiki/Swahili_language" title="Swahili language">Swahili</a>
                    </li>
                    <li><a href="/wiki/Swedish_language" title="Swedish language">Swedish</a>
                    </li>
                    <li><a href="/wiki/Tajik_language" title="Tajik language">Tajik</a>
                    </li>
                    <li><a href="/wiki/Tamil_language" title="Tamil language">Tamil</a>
                    </li>
                    <li><a href="/wiki/Telugu_language" title="Telugu language">Telugu</a>
                    </li>
                    <li><a href="/wiki/Thai_language" title="Thai language">Thai</a>
                    </li>
                    <li><a href="/wiki/Turkish_language" title="Turkish language">Turkish</a>
                    </li>
                    <li><a href="/wiki/Ukrainian_language" title="Ukrainian language">Ukrainian</a>
                    </li>
                    <li><a href="/wiki/Urdu" title="Urdu">Urdu</a>
                    </li>
                    <li><a href="/wiki/Uzbek_language" title="Uzbek language">Uzbek</a>
                    </li>
                    <li><a href="/wiki/Vietnamese_language" title="Vietnamese language">Vietnamese</a>
                    </li>
                    <li><a href="/wiki/Welsh_language" title="Welsh language">Welsh</a>
                    </li>
                    <li><a href="/wiki/Xhosa_language" title="Xhosa language">Xhosa</a>
                    </li>
                    <li><a href="/wiki/Yiddish" title="Yiddish">Yiddish</a>
                    </li>
                    <li><a href="/wiki/Yoruba_language" title="Yoruba language">Yoruba</a>
                    </li>
                    <li><a href="/wiki/Zulu_language" title="Zulu language">Zulu</a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>