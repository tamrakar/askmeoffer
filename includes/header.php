<a href="#" class="goto-top"><div class="fas fa-angle-up"></div></a>
<header class="site-header">
    <div class="container-fluid">
        <div class="row align-items-center row-site-header">
            <div class="col-auto col-logo">
                <h1 class="logo"><a href=""><img alt="img"  src="img/logo.svg" width="180" height="22"></a></h1>
            </div>
            <div class="col-auto col-nav"> <a href="" class="nav-trigger"><i class="material-icons">menu</i></a>
                <nav class="site-nav">
                    <ul>
                        <li class="current"><a href=""><i class="material-icons mi-apps">apps</i>category</a>
                        
                        <ul>
                                    <li>
                                        <a href="" >
                                            <div><i class="material-icons orange-text">fingerprint</i>Popular</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons blue-text">laptop</i>Electronics</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons red-text">wc</i>Fashion</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons green-text">directions_run</i>Health &amp; Beauty</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons yellow-text">restaurant_menu</i>Dining</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons mag-text">shopping_cart</i>Groceries</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons cyan-text">home</i>Home &amp; Living</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons purple-text">place</i>Travel</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <div><i class="material-icons orange-text">directions_bike</i>Sports &amp; Outdoors</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div><i class="material-icons cyan-dark-text">directions_car</i>Automotive</div>
                                        </a>
                                    </li>
                        </ul>
                        
                        </li>
                        <li><a href=""><i class="material-icons mi-forum">favorite</i>top stores</a>
                        </li>
                        <li><a href=""><i class="material-icons mi-wb_iridescent">wb_iridescent</i>COUPONS</a>
                        </li>
                        <li><a href=""><i class="material-icons mi-swap_horiz">swap_horiz</i> compare</a>
                        </li>
                        <li><a href=""><i class="material-icons mi-star_rate">star</i> Offers</a>
                        </li>
                        <li><a href=""><i class="material-icons mi-star_rate">forum</i> Forum</a>
                        </li>
                        <li><a href=""><i class="material-icons mi-star_rate">remove_red_eye</i> Review</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col col-header-right ml-auto">
                <div class="d-flex f-13 m-b-5 align-items-center justify-content-end">
                    <div class="sec-menu-wrapper"><a href="" class="sec-nav-trigger">Menu <i class="material-icons">arrow_drop_down</i></a>
                        <ul class="d-flex nav-sec align-items-center justify-content-end">
                            <li><a href="">Refer & Earn</a>
                            </li>
                            <li><a href="">FAQ</a>
                            </li>
                            <li><a href="">How It Works</a>
                            </li>
                            <li><a href="">CashBack Calculator</a>
                            </li>
                            <li><a href="">24x7 Customer Care</a>
                            </li>
                            <li><a href="">Track cashback</a>
                            </li>
                        </ul>
                    </div>
                    <div class="notification-wrap"> <a href="" class="notification"><i class="material-icons">notifications</i></a>
                        <div class="notification-dropdown">
                            <div class="title">Notifications</div>
                            <div class="body not-logged"> <img src="img/notifications-illustration_72b0d6.svg" alt="" width="170" height="121">
                                <h4 class="f-17 m-t-20 m-b-0 blue-text">You are missing out!</h4>
                                <p>Sign in to view personalized notifications and offers.</p>
                                <a href="" class="btn blue">Login</a> </div>
                        </div>
                    </div>
                    <a href="" class="login-btn" data-toggle="modal" data-target="#login-pop"><i class="material-icons">person</i> Signup / Login</a>
                    <a href="" class="language-switch-trigger" data-toggle="modal" data-target="#language-switch">English <i class="fas fa-angle-down"></i></a>
                </div>
                <div class="row">
                    <div class="col col-search">
                        <div class="search">
                            <a href="" class="search-close"><i class="material-icons">close</i></a>
                            <div class="search-form-wrapper">
                                <form class="search-form-wrapper-inner">
                                    <input type="text" class="search-control" id="search" placeholder="Search for Products, Brands & More....">
                                    <button><i class="material-icons">search</i></button>
                                </form>
                            </div>


                            <div class="search-area-content">
                                <div class="search-area-content-wrapper">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h4 class="grey-light-text">TOP CASHBACK STORES</h4>
                                            <div class="top-store-lists">
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/flipkart.1507100422.png" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Flipkart</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/amazon.1502715465.png" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Amazon.in</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/sitaphal-3872-1-6-1439450574.jpg" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Nearbuy</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/jabong.1502716950.png" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Jabong</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/myntra.jpg" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Myntra</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/flipkart.1507100422.png" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Flipkart</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/amazon.1502715465.png" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Amazon.in</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/sitaphal-3872-1-6-1439450574.jpg" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Nearbuy</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/jabong.1502716950.png" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Jabong</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                                <a href="" class="white d-flex pad-10 align-items-center f-12 top-store-item m-b-10">
                                                    <figure class="m-r-20 m-l-10 text-center"><img src="img/myntra.jpg" width="130" alt="1">
                                                    </figure>
                                                    <div class="figcaption">
                                                        <div class="grey-light-text f-14">Myntra</div>
                                                        <div class="red-text">Upto 10% Rewards</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="m-l-20 hot-deals-list-wrap">
                                                <h4 class="grey-light-text">HOT DEALS</h4>
                                                <div class="hot-deals-list">
                                                    <?php for($i=0;$i<10;$i++){ ?>
                                                    <div class="hot-deal-item pad-20 d-block white m-b-10">
                                                        <div>Upto 80% Off</div>
                                                        <div class="red-text m-b-15">Earn Upto 10% Rewards</div>
                                                        <div class="m-b-20">Grand Gadget Days Sale Offer: Upto 80% Off + Extra Cashback</div>

                                                        <div class="row align-items-center">
                                                            <div class="col text-center">
                                                                <img src="img/fk-logo-2.jpg" height="30" alt="a">
                                                            </div>
                                                            <div class="col">
                                                                <a href="" class="btn green w-100">Shop Now</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hot-deal-item pad-20 d-block white m-b-10">
                                                        <div>Upto 80% Off</div>
                                                        <div class="red-text m-b-15">Earn Upto 10% Rewards</div>
                                                        <div class="m-b-20">Grand Gadget Days Sale Offer: Upto 80% Off + Extra Cashback</div>

                                                        <div class="row align-items-center">
                                                            <div class="col text-center">
                                                                <img src="img/myntra.jpg" height="30" alt="a">
                                                            </div>
                                                            <div class="col">
                                                                <a href="" class="btn green w-100">Shop Now</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="popular-links-wrap m-l-20 "><h4 class="grey-light-text">POPULAR LINKS</h4>
                                            <ul class="list-unstyled">
                                                <li><a href="">Flipkart Republic Day Sale</a></li>
                                                <li><a href="">Amazon Great Indian Sale</a></li>
                                                <li><a href="">BigBasket Republic Day Sale</a></li>
                                                <li><a href="">Shoppers Stop Sale</a></li>
                                                <li><a href="">TataCliq EOSS Sale: Upto 80% Off</a></li>
                                                <li><a href="">Shopclues 6th Anniversary Sale 2018</a></li>
                                            </ul>
                                            <hr>
                                            <ul class="list-unstyled">
<li><a href="">Goomo Domestic Flights Offers</a></li>
                                            <li><a href="">100% Cashback on TEA Order</a></li>
                                            <li><a href="">Flipkart Republic Day Mobile Offers</a></li>
                                            <li><a href="">Nykaa Republic Day Sale</a></li>
</ul>
                                       
  <hr>                                      
                                            <ul class="list-unstyled">
<li><a href="">Cashback Stores</a></li>
                                            <li><a href="">Today's Offers</a></li>
                                            <li><a href="">Flipkart Deal of the Day</a></li>
                                            <li><a href="">Snapdeal Deal of the Day</a></li>
                                            <li><a href="">Flipkart Active Offers</a></li>
</ul></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-auto col-location">
                        <div class="address"><span class="current-locale"><i class="material-icons">place</i></span> <div class="location"> <img alt="img" src="img/nl.png" class="flag"> <input type="text" class="location-search" value="Netherlands"></div>
                        <a href="" class="current-location">
                            <i class="material-icons">place</i> Current Location
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>