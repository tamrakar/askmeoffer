<footer class="site-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="f-18 w-400 blue-text text-uppercase">Help</h4>
                    <ul class="footer-links">
                        <li><a href="">FAQ</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4 class="f-18 w-400 blue-text text-uppercase">Flipkart</h4>
                    <ul class="footer-links">
                        <li><a href="">Contact Us</a>
                        </li>
                        <li><a href="">About Us</a>
                        </li>
                        <li><a href="">Careers</a>
                        </li>
                        <li><a href="">Cashback Stories</a>
                        </li>
                        <li><a href="">Press</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4 class="f-18 w-400 blue-text text-uppercase">Misc</h4>
                    <ul class="footer-links">
                        <li><a href="">Sitemap</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4 class="f-18 w-400 blue-text text-uppercase">others</h4>
                    <ul class="footer-links">
                        <li><a href="">24*7 Customer care</a>
                        </li>
                        <li><a href="">How it works</a>
                        </li>
                        <li><a href="">Refer and Earn</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-between align-items-center footer-terms">
                <div class="col f-12">Policies: Returns Policy | Terms of use | Security | Privacy | Infringement</div>
                <div class="col f-12 text-center">© Cashback India 2017</div>
                <div class="col text-right social-links"> <span class="f-12">Keep in touch :</span>
                    <a href="" class="fb"><i class="fab fa-facebook-f"></i></a>
                    <a href="" class="tw"><i class="fab fa-twitter"></i></a>
                    <a href="" class="gp"><i class="fab fa-google-plus-g"></i></a>
                    <a href="" class="yt"><i class="fab fa-youtube"></i></a>
                </div>
            </div>
            <div class="top-story f-14 w-700"> <span class="grey-text">Top Stories :</span> <span class="dark-text">Brand Directory</span> </div>
            <div class="cat-links"> <span>MOST SEARCHED FOR ON Askoffer:</span> <a href="">iphone X</a> | <a href="">VIVO V7 Plus</a> | <a href="">Oppo f5</a> | <a href="">Flipkart Coupons</a> | <a href="">Online Shopping Offers</a> | <a href="">Birthday Gifts</a> | <a href="">Wedding Gifts</a> | <a href="">Anniversary Gifts</a> | <a href="">New Year Gifts</a> | <a href="">Christmas Gifts</a> | <a href="">Housewarming Gifts</a> | <a href="">Gifts for Men</a> | <a href="">Gifts for Women</a> | <a href="">Gifts for Boys</a> | <a href="">Gifts for Girls</a> </div>
            <div class="cat-links"> <span>MOBILES:</span> <a href="#">4G Mobiles</a> | <a href="#">Smartphones</a> | <a href="#">HTC Mobile</a> | <a href="#">Honor Mobile</a> | <a href="#">Nokia Mobile</a> | <a href="#">Samsung Mobile</a> |<a href="#"> Mi Mobile</a> | <a href="#">iphone</a> | <a href="#">Oppo Mobile</a> | <a href="#">Vivo Mobile</a> | <a href="#">Lenovo Mobile</a> | <a href="#">Micromax Mobile</a> | <a href="#">Motorola Mobile</a> | <a href="#">iPhone se</a> </div>
            <div class="cat-links"> <span>HOME</span>: <a href="#">Diwali Store</a> | <a href="#">Bed</a> | <a href="#">Mattress</a> | <a href="#">Furniture</a> | <a href="#">Sofa Cum Bed</a> | <a href="#">Hookahs</a> | <a href="#">Dining Table</a> | <a href="#">Torch Light</a> | <a href="#">Alarm Clock</a> | <a href="#">Shoe Racks</a> | <a href="#">Curtains</a> | <a href="#">LED Bulbs</a> | <a href="#">Wall Decals/Stickers</a> | <a href="#">Showpieces</a> | <a href="#">Wall Clocks</a> </div>
            <div class="cat-links"><span>CAMERA:</span> <a href="#">Nikon</a> | <a href="#">Camera</a> | <a href="#">Canon</a> | <a href="#">Camera</a> | <a href="#">Sony Camera</a> | <a href="#">Canon DSLR</a> | <a href="#">Nikon DSLR</a> | <a href="#">DSLR Camera</a> | <a href="#">Camera Lens</a> | <a href="#">Camera Tripod</a> | <a href="#">Camera Bags</a> | <a href="#">Camera Accessories</a>
            </div>
            <div class="cat-links"><span>LAPTOPS:</span> <a href="#">Branded Laptops</a> <a href="#">Apple Laptops</a> <a href="#">Acer Laptops</a> <a href="#">Lenovo Laptops</a> <a href="#">Dell Laptops</a> <a href="#">Asus Laptops</a> <a href="#">HP Laptops</a> <a href="#">Gaming Laptops</a> <a href="#">2 in 1 Laptops</a> <a href="#">Business Laptops</a>
            </div>
            <div class="cat-links"><span>TVS:</span> <a href="#">Micromax 43 Inch TV</a> | <a href="#">VU 32 Inch TV</a> | <a href="#">Sanyo TV</a> | <a href="#">Vu TV</a> | <a href="#">40 inches TV</a> | <a href="#">32 inches TV</a> | <a href="#">Samsung TV</a> | <a href="#">Sony TV</a> | <a href="#">Panasonic TV</a> | <a href="#">LG TV</a> | <a href="#">Micromax TV</a> | <a href="#">Smart TV</a> | <a href="#">4K TV</a> | <a href="#">LED TV</a> | <a href="#">Curved TV</a>
            </div>
            <div class="cat-links"><span>LARGE APPLIANCES:</span> <a href="#">IFB Washing Machines</a> | <a href="#">LG Washing Machines</a> | <a href="#">LG Refrigerators</a> | <a href="#">Samsung Refrigerators</a> | <a href="#">Samsung Washing Machines</a> | <a href="#">Hitachi AC Whirlpool Washing Machines</a> | <a href="#">Blue Star AC</a> | <a href="#">Whirlpool Refrigerators</a> | <a href="#">Voltas AC</a>
            </div>
            <div class="cat-links"><span>MEN CLOTHING:</span> <a href="#">Men Shirts</a> | <a href="#">Men Jeans</a> | <a href="#">Men T-Shirts</a> | <a href="#">Men Suits</a> | <a href="#">Men Trousers</a> | <a href="#">Mens Blazers</a> | <a href="#">Levis</a> | <a href="#">Jeans</a> | <a href="#">Pepe Jeans</a> | <a href="#">Nike Jackets</a> | <a href="#">Nehru Jacket</a> | <a href="#">Jack and Jones</a> <a href="#">Pathani Suit</a> <a href="#">Linen Shirts</a> <a href="#">Jodhpuri suits</a>
            </div>
            <div class="cat-links"><span>WOMEN CLOTHING:</span> <a href="#">Kurtis</a> | <a href="#">Dresses</a> | <a href="#">Sarees</a> | <a href="#">Salwar Suits</a> | <a href="#">Lehenga Cholis</a> | <a href="#">Anarkali Suits</a> | <a href="#">Lehenga Sarees</a> | <a href="#">Blouses</a> | <a href="#">Bras</a> | <a href="#">Churidars</a> | <a href="#">Ethnic</a> | <a href="#">Wear Western</a> | <a href="#">Wear Formal</a> | <a href="#">Wear Party</a> | <a href="#">Wear Dresses Silk</a> | <a href="#">Sarees Maxi</a> | <a href="#">Dresses Leggings</a>
            </div>
            <div class="cat-links"><span>ELECTRONICS ACCESSORIES:</span> <a href="#">Pen Drives</a> | <a href="#">Memory Cards</a> | <a href="#">Speakers</a> | <a href="#">Headphones</a> | <a href="#">Hard disks</a> | <a href="#">Screen Guards</a> | <a href="#">Power Banks</a> | <a href="#">Mobile Covers</a> | <a href="#">Wifi Routers</a> | <a href="#">Dongle</a> | <a href="#">Printers</a> | <a href="#">Laptop Bags</a>
            </div>
            <div class="cat-links"><span>LIFESTYLE ACCESSORIES:</span> <a href="#">Watches</a> | <a href="#">Titan Watches</a> | <a href="#">Citizen Watches</a> | <a href="#">Emporio Armani Watches</a> | <a href="#">Seiko Watches</a> | <a href="#">Rado Watches</a> | <a href="#">Longines Watches</a> | <a href="#">Tag Heuer Watches</a> | <a href="#">Ray Ban Sunglasses</a> <a href="#">VIP Bags Belts</a> <a href="#">Tissot Watches</a> <a href="#">Michael Kors Watches</a> <a href="#">Treadmills</a> <a href="#">Protein Supplements</a>
            </div>
            <div class="cat-links"><span>FOOTWEAR:</span> <a href="#">Shoes</a> | <a href="#">Adidas Shoes</a> | <a href="#">Reebok Shoes</a> | <a href="#">Puma Shoes</a> | <a href="#">Bata Shoes</a> | <a href="#">Woodland Shoes</a> | <a href="#">Fila Shoes</a> | <a href="#">Boots</a> | <a href="#">Winter Boots</a> | <a href="#">Skechers Shoes</a> | <a href="#">Black Sandals</a> | <a href="#">Boat Shoes</a> | <a href="#">Basketball Shoes</a> | <a href="#">Ankle Boots</a> | <a href="#">Derby Shoes Cowboy</a> | <a href="#">Boots High</a> | <a href="#">Heel Boots</a> | <a href="#">Crocs Shoes</a>
            </div>
            <div class="cat-links"><span>JEWELLERY:</span> <a href="#">Jewellery</a> | <a href="#">Bangles</a> | <a href="#">Bracelets</a> | <a href="#">Anklets</a> | <a href="#">Rings</a> | <a href="#">Necklace</a> | <a href="#">Gold Earrings</a> | <a href="#">Diamond Earrings</a> | <a href="#">BlueStone Jewellery</a> | <a href="#">Gold Jewellery</a> | <a href="#">Diamond Jewellery</a> | <a href="#">Diamond Rings</a> | <a href="#">Diamond Necklaces</a>
            </div>
            <div class="cat-links"><span>BEAUTY PRODUCTS:</span> <a href="#">Lipstick</a> | <a href="#">Compact Powder</a> | <a href="#">Face Foundations</a> | <a href="#">Eye Liners</a> | <a href="#">Nail Polish</a> | <a href="#">Fragrances</a> | <a href="#">Mascaras</a> | <a href="#">Deodorants</a> | <a href="#">Perfumes</a>
            </div>
            <div class="footer-contact row no-gutters">
                <div class="col-md-6 footer-contact-item">
                    <h4 class="f-14 w-700 blue-text">Mail Us :</h4> Cashback India Private Limited,<br> Ozone Manay Tech Park,<br> #56/18 & 55/09, 7th Floor,<br> Garvebhavipalya, Hosur Road,<br> Bangalore - 560068,<br> Karnataka, India. </div>
                <div class="col-md-6 footer-contact-item">
                    <h4 class="f-14 w-700 blue-text">Registered office address :</h4> Cashback India Private Limited,<br> Ozone Manay Tech Park,<br> #56/18 & 55/09, 7th Floor,<br> Garvebhavipalya, Hosur Road,<br> Bangalore - 560068,<br> Karnataka, India.<br> CIN : U51109KA2012PTC066107<br> Telephone: 1800 208 9898 </div>
            </div>
            <div class="footer-infos">
                <h4>Online shopping in India at its best</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                <h4>Easy return policies and quick delivery</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                <h4>Cashback India Assured- Our promise to deliver quality products</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                <h4>Shop online with the best deals & offers</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                <p>Apart from the best deals & offers online, you also have the option to pay for your most favorite products using our EMI option. Now you need not refrain from purchasing that expensive mobile phone you always wished to own.</p>
                <h4>Purchase online referring to our easy buying guides</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                <h4>Every Order is a wish to be fulfilled - Ab Har Wish Hogi Poori</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
            </div>
        </div>
    </footer>