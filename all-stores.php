<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>All stores</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid m-b-20">
        <div class="white border ">
           <div class="pad-15">
           <h1 class="m-0 f-c f-26 blue-text w-400">All Online Shopping Websites in India</h1>
           <p>We have made to you a comprehensive list of Online Shopping Websites in India, Which is updated on Thursday, 08-February-2018. These are the most trusted e-commerce portal. You can buy anything for your need like Mobile, Laptop, Fridge, Washing Machine, Resturent Vouchers, Etc. You can even pay your utility bills like Water, electricity, Gas, Etc. Short these lists by clicking below tab.</p>
         </div>
        </div>   
            <div class="pad-15 white border border-top-0 m-b-15">
             <div class="sort-by-alpha f-12 m-b-15">
                    <div class="row gutters-xs">
                    <div class='col text-center'><a href='' class='btn btn-sm d-block white border'>0-9</a></div>
                    <?php
                       for ($i = 'a'; $i != 'aa'; $i++){ 
                            echo "<div class='col-2 col-md-1 col-lg text-center m-b-5'><a href='' class='btn btn-sm d-block white border'>$i</a></div>";
                       }
                    ?>
                    </div>
             </div>
 
                  <div class="store-search-bar ">
                      <div class="store-search-bar-inner" id="toggle-1">
                         <div class="row">
                          <div class="col">
                              <button data-target="#bycategory" class="btn white border m-r-5 collapsed" data-toggle="collapse">By category <i class="fas fa-angle-down m-l-5 grey-text"></i></button>
                              <button data-target="#bymerchant" class="btn white border collapsed" data-toggle="collapse">By merchant <i class="fas fa-angle-down m-l-5 grey-text"></i></button>
                          </div>
                          <div class="col d-none justify-content-end d-md-flex align-items-center">
                              <label class="m-b-0 m-r-10">Sort by</label>
                              <select name="" id="" class="input">
                                <option>Relevance</option>
                                <option>Featured</option>
                                <option>Price: Low to high</option>
                                <option>Price: High to low</option>
                                <option>Avg. Customer review    </option>
                            </select>
                          </div>
                          </div>
                            <div >
                                <div id="bycategory" class="collapse" data-parent="#toggle-1">
                                    <div class="m-t-15">
                                        <ul class="idTabs tab-offer border ">
                                            <li>
                                                <a href="#idTab1">
                                                    <div><i class="material-icons orange-text">apps</i>All</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab2">
                                                    <div><i class="material-icons blue-text">laptop</i>Electronics</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab3">
                                                    <div><i class="material-icons red-text">wc</i>Fashion</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab4">
                                                    <div><i class="material-icons green-text">directions_run</i>Health & Beauty</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab5">
                                                    <div><i class="material-icons yellow-text">restaurant_menu</i>Dining</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab6">
                                                    <div><i class="material-icons mag-text">shopping_cart</i>Groceries</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab7">
                                                    <div><i class="material-icons cyan-text">home</i>Home & Living</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab8">
                                                    <div><i class="material-icons purple-text">place</i>Travel</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab9">
                                                    <div><i class="material-icons orange-text">directions_bike</i>Sports & Outdoors</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#idTab10">
                                                    <div><i class="material-icons cyan-dark-text">directions_car</i>Automotive</div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                             <div class="card">
                  <div id="bymerchant" class="collapse" data-parent="#toggle-1">
                      <form action="?" class="search-group m-t-15">
                            <div class="row align-items-center"><i class="material-icons col-auto f-26">search</i>
                                <input type="text" class="col" placeholder="Search by merchant">
                                <button class="col-auto f-26"><i class="material-icons">keyboard_arrow_right</i></button>
                            </div>
                        </form>
                  </div>
                 </div>
                      </div>
                  </div>
            </div>
                <div class="row">
                   <?php for($i=0;$i<5;$i++){ ?>
                    <div class="col-sm-3 col-store-item m-b-10">
                        <a href="" class="text-center store-item d-block border hover-no-deco white rounded black-text">
                            <div class="pad-15"><img src="img/flipkart_store.png" height="35" alt=""></div>

                            <div class="pad-15 border-top"><div class="m-b-5">Earn upto</div>
                            <span class="btn blue f-20 f-c">10%</span>
                            <div class="m-t-5">rewards</div></div>
                            <div class="pad-10 border-top grey-lighter-3 "><i class="fas fa-tag orange-text fa-fw m-r-5"></i>3094 Flipkart Coupons</div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-store-item m-b-10">
                        <a href="" class="text-center store-item d-block border hover-no-deco white rounded black-text">
                            <div class="pad-15"><img src="img/myntra.jpg" height="35" alt=""></div>

                            <div class="pad-15 border-top"><div class="m-b-5">Earn upto</div>
                            <span class="btn blue f-20 f-c">10%</span>
                            <div class="m-t-5">rewards</div></div>
                            <div class="pad-10 border-top grey-lighter-3 "><i class="fas fa-tag orange-text fa-fw m-r-5"></i>3094 myntra Coupons</div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-store-item m-b-10">
                        <a href="" class="text-center store-item d-block border hover-no-deco white rounded black-text">
                            <div class="pad-15"><img src="img/ebay_store.png" height="35" alt=""></div>

                            <div class="pad-15 border-top"><div class="m-b-5">Earn upto</div>
                            <span class="btn blue f-20 f-c">10%</span>
                            <div class="m-t-5">rewards</div></div>
                            <div class="pad-10 border-top grey-lighter-3 "><i class="fas fa-tag orange-text fa-fw m-r-5"></i>3094 ebay Coupons</div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-store-item m-b-10">
                        <a href="" class="text-center store-item d-block border hover-no-deco white rounded black-text">
                            <div class="pad-15"><img src="img/amazon_store.png" height="35" alt=""></div>

                            <div class="pad-15 border-top"><div class="m-b-5">Earn upto</div>
                            <span class="btn blue f-20 f-c">10%</span>
                            <div class="m-t-5">rewards</div></div>
                            <div class="pad-10 border-top grey-lighter-3 "><i class="fas fa-tag orange-text fa-fw m-r-5"></i>3094 amazon Coupons</div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <div class="white pad-15 border">
                    <h2 class="f-c f-22 m-t-0 m-b-20 w-400">Top 10 Best Online Shopping Websites in India, February 2018</h2>
                    <table class="table f-13 table-bordered m-0">
                        <tbody>
                            <tr>
                                <th>S. No.</th>
                                <th>Online Shopping Website</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><a href="https://www.sitaphal.com/all/stores#flipkart">Flipkart</a>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><a href="https://www.sitaphal.com/all/stores#amazon">Amazon</a>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><a href="https://www.sitaphal.com/all/stores#snapdeal">Snapdeal</a>
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><a href="https://www.sitaphal.com/all/stores#ebay">Ebay</a>
                                </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><a href="https://www.sitaphal.com/all/stores#shopclues">Shopclues</a>
                                </td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td><a href="https://www.sitaphal.com/all/stores#paytm">Paytm</a>
                                </td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td><a href="https://www.sitaphal.com/all/stores#myntra">Myntra</a>
                                </td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td><a href="https://www.sitaphal.com/all/stores#jabong">Jabong</a>
                                </td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td><a href="https://www.sitaphal.com/all/stores#zivame">Zivame</a>
                                </td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td><a href="https://www.sitaphal.com/all/stores#freecharge">Freecharge</a>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="blue white-text text-center pad-20">
                    <h3 class="f-32 w-400 m-t-0 f-c m-b-10">Calculate Your Cashback</h3>
                    <div class="f-22 m-b-15 d-md-flex d-block text-center justify-content-center align-items-center"><span>If I spend</span>
                        <select name="" id="" class="input blue-border f-17 m-r-5 m-l-5">
                           <option value="">$50</option>
                           <option value="">$75</option>
                           <option value="">$100</option>
                            <option value="" selected>$200</option>
                        </select>  <span>at</span>
                        <select name="" id="" class="input blue-border f-17 m-r-5 m-l-5">
                            <option value="0">Dan Murphy's</option>
                            <option value="1">Woolworths</option>
                            <option value="2">Myer</option>
                            <option value="3">David Jones</option>
                            <option value="4">eBay</option>
                            <option value="5" selected="">Amazon</option>
                            <option value="6">Aliexpress</option>
                            <option value="7">Booking.com</option>
                            <option value="8">Expedia.com</option>
                            <option value="9">Hotels.com</option>
                            <option value="10">The Iconic</option>
                            <option value="11">Asos</option>
                            <option value="12">Lenovo</option>
                            <option value="13">The Good Guys</option>
                            <option value="14">Petbarn</option>
                            <option value="15">Menulog</option>
                            <option value="16">Microsoft</option>
                            <option value="17">Dell</option>
                            <option value="18">Chemist Warehouse</option>
                            <option value="19">Priceline Pharmacy</option>
                            <option value="20">Freedom</option>
                            <option value="21">BWS</option>
                            <option value="22">Net-a-porter</option>
                            <option value="23">Sony</option>
                            <option value="24">Cotton On</option>
                        </select> <span>I get</span> <span class="f-32 yellow-text m-r-10 m-l-10 l-1-0">$14.00*</span> <span>back</span></div>

                    <div class="f-12">*Cashback rates were updated on 18/10/2017 and are for illustrative purposes only.<br> See cashrewards.com.au for current rates and full stores list.</div>
                </div>
            
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
     <?php include('includes/lang-list.php'); ?>
     <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>