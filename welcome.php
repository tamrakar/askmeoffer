<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="sgin-body rounded white border pad-30 m-b-20 m-t-10">
            <div class="row f-12 align-items-center no-gutters m-b-5 justify-content-between">
                <div class="col-auto blue-text">Welcome</div>
                <div class="col-auto">Account type</div>
                <div class="col-auto">Your details</div>
                
                <div class="col-auto">Stores</div>
            </div>
            
            <div class="m-b-20">
                <div class="row align-items-center no-gutters">
                    <div class="col-auto">
                        <i class="fas fa-circle blue-text"></i>
                    </div>
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="far fa-circle blue-text"></i>
                    </div>
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="far fa-circle blue-text"></i>
                    </div>
                    
                    <div class="col"><hr class="blue-border m-0"></div>
                    <div class="col-auto">
                         <i class="far fa-circle blue-text"></i>
                    </div>

                </div>
            </div>
            
            <div class="text-center">
                <h1 class="m-b-0 blue-text">Welcome to askmeoffer, Jane</h1>
                <p>In less than 2 minutes we'll have you earning cashback both online and on the High Street</p>
                    <div class="m-b-20"><img src="img/usr-1.png" class="img-fluid" height="250" alt=""></div>
                <a href="account-type.php" class="btn blue">Let's get started</a>
            </div>
        </div>
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>