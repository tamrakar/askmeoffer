var clipboard = new Clipboard('.code-copy-action');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
});

$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
    
    $(".cpop").popover({ trigger: "manual" , html: true, animation:false})
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });
    
    $('.store-coupon-jump').on('click',function(event){
        $('html, body').animate({
            scrollTop: $('.store-coupon-content').offset().top-83
        },300,function(){
            $('.idTabs a[href="#idTab2"]').click();
        });
        event.preventDefault();
    });
    $('.store-deal-jump').on('click',function(event){
        $('html, body').animate({
            scrollTop: $('.store-product-content').offset().top-83
        },300,function(){
            $('.idTabs a[href="#idTab3"]').click();
        });
        event.preventDefault();
    });
    
    $('.goto-top').on('click',function(event){
        $('html, body').animate({
            scrollTop: 0
        },300);
        event.preventDefault();
    });
    function scrollCheck(){
        var top = window.pageYOffset || document.documentElement.scrollTop;
        
        if (top >= 200) {
            jQuery('html').addClass('scrolled');
        } else {
            jQuery('html').removeClass('scrolled');
        }
        
        if($('.tab-product').length>0){
            if (top >= 28) {
                jQuery('html').addClass('tab-product-top');
            } else {
                jQuery('html').removeClass('tab-product-top');
            }
        }
        
        if($('.fltg-prce-bar').length>0){
            if (top >= $('.product-section-group').offset().top - 30 - $('.site-header').height() - $('.scrollTabs').height()) {
                jQuery('html').addClass('fltg-prce-reached');
            } else {
                jQuery('html').removeClass('fltg-prce-reached');
            }
            
            
            if(top >= $('.product-section-group').height() + $('.site-header').height() + $('.scrollTabs').height() + $(window).height() - 90){
                jQuery('html').addClass('fltg-fotr-reached');
            }else{
                 jQuery('html').removeClass('fltg-fotr-reached');
            }
        }
        
        if($('.store-search-bar').length>0){
            if(top >= $('.store-search-bar').offset().top) {
                jQuery('html').addClass('stor-srch-bar-reached');
            }else{
                 jQuery('html').removeClass('stor-srch-bar-reached');
            }
        }
        
        if($('.offer-search-bar').length>0){
            if(top >= $('.offer-search-bar').offset().top) {
                jQuery('html').addClass('offr-srch-bar-reached');
            }else{
                 jQuery('html').removeClass('offr-srch-bar-reached');
            }
        }
    }
    
    $('.toggle-trigger').on('click',function(event){
        $(this).closest('.toggle-container').find('.toggle-body:first').toggleClass('open');
        event.preventDefault();
    });
    $('.toggle-body .close').on('click',function(event){
        $(this).closest('.toggle-body').removeClass('open');
        event.preventDefault();
    });
     window.onscroll = function () {
        scrollCheck();
    };
    $('.toggle-trigger-fixed').on('click',function(event){
        var t = $(this).data('pop-target');
        $('#'+ t).css({'top':$(this).offset().top,'left':$(this).offset().left}).toggleClass('open');
        event.preventDefault();
    });
    
    scrollCheck();
    
    $('.notification').on('click',function(event){
       $('.notification-wrap').toggleClass('notifiction-show') ;
        event.preventDefault();
    });
    
    $('body').click(function(event){
        if(!$(event.target).is('.notification-wrap,.notification-wrap *')){
            $('.notification-wrap').removeClass('notifiction-show') ;
        }
    });

    $('.carousel-1,.carousel-b,.carousel-2, .carousel-3, .carousel-4, .carousel-5, .carousel-6,.weekly-offer, .carousel-video').flickity({
        imagesLoaded: true,
        cellAlign: 'left',
        contain: true,
        cellSelector: '.item',
        groupCells: true,
        pageDots: false,
        wrapAround: true,
        draggable: false
    });
    
    $('.sec-nav-trigger').on('click',function(event){
        $('.sec-menu-wrapper').toggleClass('nav-show');
        event.preventDefault();
    });
    $('.nav-trigger').on('click',function(event){
        $('.col-nav').toggleClass('nav-show');
        event.preventDefault();
    });
    
    
    $('#search').on('focus',function(){
        $('body').addClass('search-focused');
    });
    
    $('.search-close').on('click',function(event){
        $('body').removeClass('search-focused');
        event.preventDefault();
    }); 
    
    $('.activate-trigger').on('click',function(event){
        console.log('activate reward');
        if( $(this).data('status') === "notlogged"){
            console.log('activate reward not logged');
            $('.activate-reward.unsigned').addClass('activate-shown');
        }
        if( $(this).data('status') === "logged"){
            console.log('activate reward logged');
            $('.activate-reward.signed').addClass('activate-shown');
        }
        event.preventDefault();
    });
    
    $('.activate-reward .close').on('click',function(event){
        $('.activate-reward').removeClass('activate-shown');
        event.preventDefault();
    });
    
    $('.pik-brnd').on('click',function(event){
        $(this).toggleClass('green-border');
        var checkBoxes = $(this).find('input');
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        event.preventDefault();
    });
    
    $('.location-search').on('focus',function(){
        $('.current-location').addClass('show');
    });
    
    $('.location-search').on('blur',function(){
        $('.current-location').removeClass('show');
    });

    // 1st carousel, main
    $('.carousel-main').flickity({ pageDots: false, prevNextButtons: false});
    // 2nd carousel, navigation
    $('.carousel-nav').flickity({
      asNavFor: '.carousel-main',
      contain: true,
      pageDots: false,
      prevNextButtons: false
    });
    
    if($('.tab-product').length>0){
        $('html').addClass('product-page');
    }
    
    $('.scrollTabs a, .btn-scroll').on('click',function(event){
        var trgt = $(this).attr('href');
        $(this).closest('li').siblings('li').removeClass('current');
        $(this).closest('li').addClass('current');
        var top = $(trgt).offset().top;
        $('html, body').animate({
            scrollTop: top -163
        },500);
        event.preventDefault();
    });
    
    
    $('.smooth-scroll').on('click',function(event){
        var trgt = $(this).attr('href');
        var top = $(trgt).offset().top;
        $('html, body').animate({
            scrollTop: top -83
        },500);
        event.preventDefault();
    });
    
    
    $('#portfolio-filter a').click(function(event){
        $(this).addClass('selected');
        $(this).closest('li').siblings('li').find('a').removeClass('selected');
       var trgt = $(this).attr('rel');
        if(trgt!=='all'){
             $('#portfolio-list .'+trgt).show();
            $('#portfolio-list .filtr-item:not(.'+trgt+')').hide();
        }else{
            $('#portfolio-list .filtr-item').show();
        }
       event.preventDefault();
    });
    
    
    $('.acc-typ-optn').on('click',function(){
        $(this).find('input[type=radio]').prop('checked','checked');
    });
    
    
    $('.star-rater .material-icons').click(function(){
        $(this).addClass('selected');
        $(this).siblings('.material-icons').removeClass('selected');
        
    })
    
});
