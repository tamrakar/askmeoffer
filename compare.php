<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Compare</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        <div class="blue pad-20 m-b-20">
            <div class="row align-items-center">
                <div class="col-auto f-c white-text text-uppercase">Compare Products</div>
                <div class="col">
                    <select name="" id="device-type" class="input w-100">
                        <option value="">Mobiles</option>
                    </select>
                </div>
                <div class="col">
                    <input type="text" class="input w-100" placeholder="Type product(1) Name">
                </div>
                <div class="col-auto  white-text">v/s</div>
                <div class="col">
                    <input type="text" class="input w-100" placeholder="Type product(2) Name">
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn green">Compare features</button>
                </div>
            </div>
        </div>
        
        <h3 class="f-c f-22 w-400 text-center">PRICE COMPARISON</h3>
        
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">POPULAR CATEGORIES</h3></div>
            <div class="col text-right"><a href="all-category.php" class="btn blue">View All Categories <i class="fas fa-angle-right"></i> </a></div>
            </div>
            
            
            <div class="row pop-cat-list">
               <?php for($i=0;$i<12;$i++){ ?>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-10">
                    <div class="border blue-border-hover mh-100 pad-15">
                        <div class="row align-items-center"><div class="col-7"><h4 class="f-18 f-c w-400"><a href="" class="black-text">Mobile Price List</a></h4>
                        <ul class="list list-angle f-12 m-0">
                        <li><a href="">Apple iPhones</a></li>
                        <li><a href="">Samsung Mobiles</a></li>
                        <li><a href="">Micromax Mobiles</a></li>
                        <li><a href="">Lenovo Mobiles</a></li>
                        </ul></div>
                        <div class="col-5 text-center">
                            <img src="img/11327-5-1.jpg" width="40">
                        </div></div>
                    </div>
                </div>
                <?php } ?>
            </div>
            
        </div>
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">Trending Stories & News</h3></div>
            
            </div>
            
            
            <div class="row news-list">
               <?php for($i=0;$i<6;$i++){ ?>
                <div class=" col-lg-4 col-md-6 col-sm-12 m-b-10">
                    <a href="" class="border d-block blue-border-hover mh-100 pad-15 hover-no-deco">
                        <div class="m-b-10"><img src="img/xiaomi-mi-mix-2s-specifications-revealed-in-a-new-.jpg" class="img-fluid" alt=""></div>
                        <div class="bold black-text f-13 news-title h-2e"><div class="h-2e-i">Xiaomi Mi Mix 2s Specifications Revealed In A New Leak</div></div>
                        <div class="f-12">14 Feb 2018</div>
                    </a>
                </div>
                <?php } ?>
            </div>
            
        </div>
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">Best Selling/Upcoming Mobiles as on February 15, 2018</h3></div>
            
            </div>
            
            
            <div class="row news-list">
               <?php for($i=0;$i<24;$i++){ ?>
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12 m-b-10">
                    <a href="" class="border d-block blue-border-hover mh-100 pad-15 hover-no-deco text-center">
                        <div class="m-b-20 m-t-10"><img src="img/i-2.png" height="120"  alt=""></div>
                        <div class="black-text f-12 h-2e d-flex align-items-center justify-content-center"><div class="h-2e-i">Samsung Galaxy S9 plus</div></div>
                    </a>
                </div>
                <?php } ?>
            </div>
            
        </div>
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">Best Selling TV as on February 15, 2018</h3></div>            
            </div>
            <div class="row news-list">
               <?php for($i=0;$i<6;$i++){ ?>
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12 m-b-10">
                    <a href="" class="border d-block blue-border-hover mh-100 pad-15 hover-no-deco text-center">
                        <div class="m-b-20 m-t-10"><img src="img/vu-32k160m-android-32-inch-hp-new.jpg" height="120"  alt=""></div>
                        <div class="black-text f-12 h-2e d-flex align-items-center justify-content-center"><div class="h-2e-i">VU 32K160M 32 Inch HD Ready Smart LED Television Television</div></div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">Best Selling ACs as on February 15, 2018</h3></div>            
            </div>
            <div class="row news-list">
               <?php for($i=0;$i<6;$i++){ ?>
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12 m-b-10">
                    <a href="" class="border d-block blue-border-hover mh-100 pad-15 hover-no-deco text-center">
                        <div class="m-b-20 m-t-10"><img src="img/Voltas-155-CY-1.2-Ton-5-Star-Split-AC-new-2017.png" height="120"  alt=""></div>
                        <div class="black-text f-12 h-2e d-flex align-items-center justify-content-center"><div class="h-2e-i">Voltas 155 CY 1.2 Ton 5 Star Split AC </div></div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">Best Selling Refrigerator as on February 15, 2018</h3></div>            
            </div>
            <div class="row news-list">
               <?php for($i=0;$i<6;$i++){ ?>
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12 m-b-10">
                    <a href="" class="border d-block blue-border-hover mh-100 pad-15 hover-no-deco text-center">
                        <div class="m-b-20 m-t-10"><img src="img/I292RPZL-new-2017.jpg" height="120"  alt=""></div>
                        <div class="black-text f-12 h-2e d-flex align-items-center justify-content-center"><div class="h-2e-i">LG GL I292RPZL Double Door 260 Litres Frost Free Refrigerator</div></div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">Best Selling Washing Machines as on February 15, 2018</h3></div>            
            </div>
            <div class="row news-list">
               <?php for($i=0;$i<6;$i++){ ?>
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12 m-b-10">
                    <a href="" class="border d-block blue-border-hover mh-100 pad-15 hover-no-deco text-center">
                        <div class="m-b-20 m-t-10"><img src="img/ref.jpg" height="120"  alt=""></div>
                        <div class="black-text f-12 h-2e d-flex align-items-center justify-content-center"><div class="h-2e-i">Bosch WAK24168IN 7 Kg Fully Automatic Front Loading Washing Machine</div></div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="border white pad-20 m-b-10">
            <div class="row align-items-center m-b-10">
            <div class="col"><h3 class="f-c f-18 m-0 w-400">Best Selling Laptops as on February 15, 2018</h3></div>            
            </div>
            <div class="row news-list">
               <?php for($i=0;$i<6;$i++){ ?>
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12 m-b-10">
                    <a href="" class="border d-block blue-border-hover mh-100 pad-15 hover-no-deco text-center">
                        <div class="m-b-20 m-t-10"><img src="img/apple-macbook-air-mqd32hn-a-hp-new.jpg" height="120"  alt=""></div>
                        <div class="black-text f-12 h-2e d-flex align-items-center justify-content-center"><div class="h-2e-i"> Apple MacBook Air MQD32HN/A</div></div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
     <?php include('includes/lang-list.php'); ?>
     <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>