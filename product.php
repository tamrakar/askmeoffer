<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Product</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>

    <div class="container-fluid">
        <div class="breadcrumb f-12 m-b-10">
            <div class="row">
                <div class="col"><a href="#">Mobile Phones</a> / Apple / Apple iPhone 7 Price in India</div>
                <div class="col text-right">Updated On: 6th February 2018</div>
            </div>
        </div>
        <div class="tab-product-container">
            <ul class="scrollTabs tab-product d-flex xs-tab-2 justify-content-around">
                <li class="current"><a href="#spec"><span class="fas fa-tachometer-alt orange-text f-15"></span>SPECIFICATIONS</a>
                </li>
                <li><a href="#procons"><span class="fas fa-balance-scale blue-text f-15"></span>PROS & CONS</a>
                </li>
                <li><a href="#review"><span class="fas fa-star yellow-text f-15"></span>REVIEWS</a>
                </li>
                <li><a href="#video"><span class="fas fa-video green-text f-15"></span>VIDEOS</a>
                </li>
                <li><a href="#pricehistory"><span class="fas fa-history purple-text f-15"></span>PRICE HISTORY</a>
                </li>
                <li><a href="#alt"><span class="fas fa-plus-square cyan-text f-15"></span>ALTERNATIVES</a>
                </li>
            </ul>
        </div>
        <div class="white border pad-20 m-b-20">
            <h1 class="f-22 w-400 blue-text">Apple iPhone 7 (Black, 32 GB)</h1>

            <div class="product-meta d-flex">
                <div><span class="f-20 w-700 grey-light-text"><i class="fas fa-circle-notch green-text"></i> 8.4</span> Expert Score</div>
                <div class="vr"></div>
                <div><span class="f-20 w-700 grey-light-text"><i class="fas fa-star yellow-text"></i> 4.4</span> 20147 Ratings</div>
                <div class="vr"></div>
                <div><span class="f-20 w-700 grey-light-text"><i class="fas fa-comments red-text"></i> 49</span> Questions & Answers</div>
            </div>
            <hr>
            <div>The Apple iPhone 7 features a 4.7-inch display. It is powered by a Apple A10 64-bit processor and 2GB RAM. Next variants of this mobile are iPhone 8 and iPhone X. The other variant of this mobile is iPhone 7 Plus. The iPhone 7 sports an 12MP rear camera with dual-tone LED flash and a 7MP front camera. The phone has 32GB of non-expandable internal storage. Connectivity features include Wi-Fi 802.11 a/b/g/n/ac, 4G LTE, Bluetooth 4.0, NFC, and reversible lightning port. The iPhone 7 comes with a non-removable Li-Po 1,960mAh battery. It runs on the iOS 10 operating system.</div>
            <hr>
            <div class="row">
                <div class="col-lg-auto">

                    <div class="product-image">
                        <img src="img/10208-70-1.webp" class="img-fluid" alt="">
                    </div>

                </div>
                <div class="col-md-auto d-none d-lg-block">
                    <div class="vr"></div>
                </div>
                <div class="col">
                    <div class="m-b-20">
                        <h3 class="f-18 f-c w-400 m-t-0 m-b-20">Key Specifications</h3>

                        <div class="row m-b-20">
                            <div class="col-auto">
                                <div class="bold f-12 m-b-5">COLOURS</div>
                                <div class="circle-em circle yellow  f-16 m-r-5"></div>
                                <div class="circle-em circle black f-16 m-r-5"></div>
                                <div class="circle-em circle grey-lighter f-16 m-r-5"></div>
                            </div>
                            <div class="col-auto">
                                <div class="vr"></div>
                            </div>
                            <div class="col-auto">
                                <div class="bold f-12 m-b-5">SIZES</div>
                                <div class="f-12">
                                    <a href="" class="btn btn-sm border blue blue-border l-1-0">32 GB</a> <a href="" class="btn btn-sm border grey-lighter-2 l-1-0">128 GB</a> <a href="" class="btn btn-sm border grey-lighter-2 l-1-0">256GB</a>
                                </div>
                            </div>
                        </div>

                        <div class="row row-specifications">
                            <div class="col-md-6 m-b-10"> <i class="material-icons cyan-text f-26 l-1-0 m-r-5">developer_board</i> A10 Fusion processor</div>
                            <div class="col-md-6 m-b-10"> <i class="material-icons red-text f-26 l-1-0 m-r-5">memory</i>2GB RAM</div>
                            <div class="col-md-6 m-b-10"> <i class="material-icons blue-text f-26 l-1-0 m-r-5">sd_storage</i>32GB internal storage</div>
                            <div class="col-md-6 m-b-10"> <i class="material-icons orange-text f-26 l-1-0 m-r-5">battery_charging_full</i>1960 mAh battery</div>
                            <div class="col-md-6 m-b-10"> <i class="material-icons purple-text f-26 l-1-0 m-r-5">camera_alt</i>12 M.Pixels rear & 7 M.Pixels selfie</div>
                            <div class="col-md-6 m-b-10"> <i class="material-icons green-text f-26 l-1-0 m-r-5">smartphone</i>4.7 inch screen</div>
                            <div class="col-md-6 m-b-10"> <i class="material-icons yellow-text f-26 l-1-0 m-r-5">sim_card</i>Single SIM (Nano SIM)</div>
                            <div class="col-md-6 m-b-10"> <i class="material-icons grey-text f-26 l-1-0 m-r-5">settings</i>iOS 10</div>
                        </div>
                    </div>
                    <hr>
                    <h3 class="f-18 f-c w-400 m-t-0 m-b-20">Apple iPhone 8 Plus 64 GB Price in India</h3>
                    <p>Apple iPhone 7 Price starts at Rs. 42,999. The lowest price of Apple iPhone 7 is at Amazon, which is 12% less than the cost of iPhone 7 at Croma (Rs. 49000). This phone is available in 32 GB, 128 GB, 256GB storage variants.</p>
                    <div class="row">
                        <div class="col-md-auto">
                            <div class="table-responsive"><table class="table border">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="align-middle">Price</th>
                                        <th class="align-middle">askmeoffer price</th>
                                        <th class="text-right">
                                            <select name="" id="demo" class="input">
                                                <option value="">Price High to Low</option>
                                                <option value="">Price Low to High</option>
                                                <option value="">Store rating</option>
                                            </select>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src="img/amazon_store.png" alt="" width="100">
                                        </td>
                                        <td class="f-20 text-strike">
                                            ₹42,999
                                        </td>
                                        <td class="f-20 green-text">
                                            ₹41,999
                                        </td>
                                        <td class="text-right">
                                            <a href="" class="btn blue">Go to store <i class="fas fa-angle-right fa-fw"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="img/gadgetsnow_store.png" alt="" width="100">
                                        </td>
                                        <td class="f-20 text-strike">
                                            ₹42,999
                                        </td>
                                        <td class="f-20 green-text">
                                            ₹41,999
                                        </td>
                                        <td class="text-right">
                                            <a href="" class="btn blue">Go to store <i class="fas fa-angle-right fa-fw"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="img/ebay_store.png" alt="" width="100">
                                        </td>
                                        <td class="f-20 text-strike">
                                            ₹42,999
                                        </td>
                                        <td class="f-20 green-text">
                                            ₹41,999
                                        </td>
                                        <td class="text-right">
                                            <a href="" class="btn blue">Go to store <i class="fas fa-angle-right fa-fw"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="img/flipkart_store.png" alt="" width="100">
                                        </td>
                                        <td class="f-20 text-strike">
                                            ₹42,999
                                        </td>
                                        <td class="f-20 green-text">
                                            ₹41,999
                                        </td>
                                        <td class="text-right">
                                            <a href="" class="btn blue">Go to store <i class="fas fa-angle-right fa-fw"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table></div>
                            <div class="m-b-10 text-center"><a href="#pricemore" class="btn-scroll btn blue">4 more stores <i class="fas fa-angle-down"></i></a></div>
                            <div class="border blue-border">
                                <div class="row align-items-center">
                                    <div class="col-auto d-none d-md-block">
                                        <div class="pad-20 blue white-text f-30 l-1-0"><i class="far fa-bell m-r-5 m-l-5"></i>
                                        </div>
                                    </div>
                                    <div class="col sm-m-l-15 sm-m-t-10">
                                        <div class="f-c f-20">Didn't find the right price?<span class="red-text"> Set price drop alert now</span>
                                        </div>
                                        <div>We'll notify you as soon as the price drops</div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="pad-15"><a href="" class="btn blue">Set alert</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="product-section-group">
                
                <div class="fltg-prce-bar">
                    <div class="border rounded white">
                        <div class="pad-15">
                            <div class="row m-b-10 f-20">
                                <div class="col"><img src="img/amazon_store.png" alt="">
                                </div>
                                <div class="col text-right"> ₹42,999</div>
                            </div>

                            <a href="" class="btn orange d-block bold">Go to store</a>
                            <hr class="m-t-15 m-b-15">
                            <div class="row m-t-10">
                                <div class="col"><img src="img/gadgetsnow_store.png" height="20" alt="">
                                </div>
                                <div class="col text-right"> <strong>₹42,999</strong>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col"><img src="img/ebay_store.png" height="20" alt="">
                                </div>
                                <div class="col text-right"> <strong>₹42,999</strong>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col"><img src="img/flipkart_store.png" height="20" alt="">
                                </div>
                                <div class="col text-right"> <strong>₹42,999</strong>
                                </div>
                            </div>
                        </div>
                        <a href="#pricemore" class="d-block pad-10 btn-scroll border-top text-center">View all stores <i class="fas fa-angle-right"> </i></a>
                    </div>

                </div>

                <div class="product-section" id="procons">
                    <h3 class="f-c f-22 m-t-20 m-b-20 w-400">Apple IPhone 7: Pros & Cons</h3>
                    <div class="d-inline-block border green-border rounded text-center m-b-20">
                        <div class="pad-20 l-1-0 f-50 green-text">8.4</div>
                        <div class="green white-text">MSP SCORE</div>
                    </div>

                    <div class="row">
                        <div class="col-md">
                            <div class="green-text f-20 m-b-10">
                                <i class="far fa-thumbs-up"></i> Good Stuff
                            </div>
                            <ul class="list">
                                <li>Good 12MP camera with OIS</li>
                                <li>First iPhone to feature water and dust resistance</li>
                                <li>Stereo speakers</li>
                                <li>Extremely powerful & efficient quad-core Apple A10 Fusion chipset</li>
                                <li>7MP secondary camera</li>
                                <li>Outstanding battery backup performance</li>
                            </ul>
                        </div>
                        <div class="col-auto">
                            <div class="vr"></div>
                        </div>
                        <div class="col-md">
                            <div class="red-text f-20 m-b-10">
                                <i class="far fa-thumbs-down"></i> Bad Stuff
                            </div>
                            <ul class="list">
                                <li>Dual-camera setup is absent</li>
                                <li>No 3.5mm audio jack</li>
                                <li>Wired music listening, and charging at the same time is not possible</li>
                            </ul>
                        </div>
                    </div>

                    <div class="text-center m-t-20">
                        <a href="" class="btn blue">READ FULL REVIEW</a>
                    </div>
                    <hr>
                    <h4 class="f-c f-22 m-t-20 m-b-20 w-400">Apple IPhone 7 Details</h4>
                    <div class="content-body">
                        <h4>First Waterproof iPhone</h4>
                        <p>The new generation Apple iPhones always bring with them a host of new features that makes them the most technological advanced smartphones devices in the market. The Apple iPhone 7 intends to do the same, starting with its IP67 rated water and dust resistant all-aluminium metal body that makes sure that dust and rain don't stop you from doing things on your iPhone 7. The iPhone 7 and iPhone 7 Plus are the iPhone devices to feature water and dust resistance.
                        </p>
                        <h4>Force-Touch Home Button</h4>
                        <p>The home button which doubles as a fingerprint scanner on the iPhone is now force-touch based button, meaning it does not press inside. Instead, it gives a sense of tap with the help of haptic engine which is a vibration motor.
                        </p>
                        <h4>3.5mm Audio Jack Is Gone</h4>
                        <p>The only disadvantage some may find is the omission of the 3.5mm audio jack, Apple iPhone 7 now uses the lightening connector to power the headphones, although Apple is providing lightening to 3.5mm connector with the device.
                        </p>
                        <h4>The New Jet Black</h4>
                        <p>Talking about the new colour variants, the Apple iPhone 7 now comes in two more colour options jet black and black while older colour variants like silver, gold and rose gold are still present.
                        </p>
                        <h4>4.7-inch Retina Display With Wider Colour-Gamut</h4>
                        <p>The display on the Apple iPhone 7 retains the same IPS LCD technology, but now it comes with wide colour gamut, that means now you can see more colours and the colours are punchier, accurate and differentiable. The 3D touch display further extends the usability and convenience by providing with multiple options and drop down menus based on the pressure applied to the screen.
                        </p>
                        <h4>OIS Equipped 12MP Camera</h4>
                        <p>The Apple iPhone 7 is the first non-plus variant of iPhone to feature OIS (optical image stabilisation) in the primary camera; it was a much-awaited feature in the iPhone. The OIS is helpful in a plentiful of scenarios, such as it corrects the shakes caused by the user while taking image and video, it helps in letting more light into the camera sensor that results in brighter photos in the low-light conditions and much more. The 12MP primary camera now also features dual-tone quad-LED flash which has two more LED’s than its predecessor that will result in 50% more light and it can cover 50% more distance. The surprising update to the iPhone 7 is the bump in the resolution of the secondary camera to 7MP from the 5MP on the iPhone 6s. The 7MP selfie camera now yields sharper, brighter and more detailed images.
                        </p>
                        <h4>iOS 10</h4>
                        <p>Running the Apple iPhone 7 is the new iOS 10 operating system, it comes bearing with tons of new features that are focused on making the usage of the smartphone more convenient and fun. It starts as soon as you pick the device up, yes, the screen turns on itself as you pick the device up and turn it towards yourself, so now you have one less work of pressing the power button to wake the device. Apart from that the iOS 10 comes loaded with plenty of features added to iMessage, Maps, search and camera.
                        </p>
                        <h4>Apple A10 Fusion Chipset</h4>
                        <p>The excellent software is driven with the help of Apple A10 Fusion chipset that now comes with quad-core configuration. The two cores among the four are high-performance cores which will only be working during heavy tasks and games while the other two cores are power-efficient cores that will take care of basic multitasking like calling and messaging. Apple promises 40% faster performance than the Apple A9 chipset found in its predecessor.
                        </p>
                        <h4>Increased Battery Backup</h4>
                        <p>Talking about the battery backup, Apple promises 2 hours of increased web-browsing and video playback time compared to the iPhone 6s, while the standby time and the talk time remain the same. Note that the battery backup for video playback time, talk time, and music playback time are rated while using the Apple’s wireless AirPod earphones.
                        </p>
                        <h4>Connectivity</h4>
                        <p>The connectivity options don’t receive any changes apart from the omission of 3.5mm audio jack as the iPhones already feature all the technologically advanced connectivity features such as 4G, Wi-Fi b/g/n/ac, Bluetooth v4.2, NFC (Only for Apple Pay), GPS, and reversible Lightning connector.
                        </p>
                        <h4>Stereo Speakers</h4>
                        <p>The iPhone 7 and 7 Plus are also the first iPhone’s to feature stereo sound speakers. The bottom mounted speaker now produces more bass while the speaker situated at earpiece is tuned for treble, making the device ideal and more fun for listening music and watching videos. Apple promises 50% louder audio output through these stereo speakers.</p>
                    </div>
                    <hr>
                    <h4 class="f-c f-22 m-t-20 m-b-20 w-400">Apple iPhone 8 Plus 64 GB Specification Analysis</h4>
                    
                    <div class="grey-lighter-3 pad-20">
                        <div class="row">
                            <div class="col">
                                <div class="f-18 f-c w-400 m-t-0 m-b-20">Specification Score</div>
                                
                                <ul class="list">
                                    <li>Price Range: Rs. 55000 to Rs. 75000</li>
                                    <li>Score: Below Average</li>
                                </ul>
                                
                            </div>
                            <div class="col">
                                <div class="single-chart m-l-20">
                                    <svg viewbox="0 0 36 36" class="circular-chart blue-chart">
                                        <path class="circle-bg" d="M18 2.0845
                                          a 15.9155 15.9155 0 0 1 0 31.831
                                          a 15.9155 15.9155 0 0 1 0 -31.831"/>
                                        <path class="circle" stroke-dasharray="30, 100" d="M18 2.0845
                                          a 15.9155 15.9155 0 0 1 0 31.831
                                          a 15.9155 15.9155 0 0 1 0 -31.831"/>
                                        <text x="18" y="20.35" class="percentage">30%</text>
                                    </svg>
                                </div>
                            </div>
                            <div class="col">
                                <div class="f-12">This phone's score</div>
                                <div class="row align-items-center m-b-5"><div class="col"><div class="rating-bar "><div class="rating-value green" style="width: 30%"></div></div></div> <div class="col-auto f-16 bold">61</div></div>
                                
                                <div class="f-12">Average score in this price range</div>
                                <div class="row align-items-center m-b-5"><div class="col"><div class="rating-bar "><div class="rating-value green" style="width: 30%"></div></div></div> <div class="col-auto f-16 bold">67</div></div>
                                 
                                <div class="f-12">Best score in this price range</div>
                                <div class="row align-items-center m-b-5"><div class="col"><div class="rating-bar "><div class="rating-value green" style="width: 30%"></div></div></div> <div class="col-auto f-16 bold">88</div></div>
                            </div>
                        </div>
                    </div>
                    <div class="grey-lighter-2 pad-10">
                        <div class="row">
                            <div class="col"><strong>Group Rank :</strong> <span class="bold orange-text f-20 align-middle">#15</span> out of19 Mobiles In This Price Range</div>
                            <div class="col text-right"><strong>Overall Rank :</strong> <span class="bold orange-text f-20 align-middle">#589</span> out of 1167 Mobiles Overall</div>
                        </div>
                    </div>
                    
                    <hr>
                    <h4 class="f-c f-22 m-t-20 m-b-20 w-400">Analysis <span class="w-300">(Price Range: Rs. 55000 to Rs. 75000)</span></h4>
                    <div class="table-responsive"><table class="table table-bordered">
                    <thead>
                        <tr>

                            <td colspan="2"></td>
                            <td><strong>GROUP BEST</strong> </td>
                            <td><strong>GROUP AVERAGE</strong> </td>
                            <td><strong>THIS PHONE</strong> </td>
                            <td><strong>REMARK</strong>
                            </td>
                        </tr>
                    </thead>

                    <tr>
                        <td rowspan="2" class="grey-lighter-3"><strong>DISPLAY</strong> </td>
                        <td>Screen Resolution</td>
                        <td><a href="#compare-display" class="collapsed" data-toggle="collapse">2960 X 1440 pixels <i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td>1920 X 1080 pixels </td>
                        <td>1920 X 1080 pixels </td>
                        <td>Average</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">
                            <div class="collapse " id="compare-display">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Screen Resolution In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Samsung Galaxy S8+ </td>
                                            <td>2960 X 1440 pixels</td>
                                        </tr>
                                        <tr>
                                            <td>Samsung Galaxy Note 8 </td>
                                            <td>2960 X 1440 pixels</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 XL 128 GB </td>
                                            <td>2880 X 1440 pixels</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 XL 64 GB </td>
                                            <td>2880 X 1440 pixels</td>
                                        </tr>
                                        <tr>
                                            <td>HTC U11 Plus 128 GB </td>
                                            <td>2880 X 1440 pixels</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td rowspan="4" class="grey-lighter-3"><strong>MEMORY</strong> </td>
                        <td>Internal Memory</td>
                        <td><a href="#compare-memory" class="collapsed" data-toggle="collapse">256 GB <i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td>64 GB </td>
                        <td>64 GB </td>
                        <td>Average</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">

                            <div class="collapse " id="compare-memory">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Internal Memory In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Apple iPhone 7 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 256 GB </td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 128 GB </td>
                                            <td> 128 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 7 Plus 128 GB</td>
                                            <td> 128 GB</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td> RAM</td>
                        <td> <a href="#compare-ram" class="collapsed" data-toggle="collapse">8 GB<i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td> 4 GB </td>
                        <td> 3 GB </td>
                        <td> Average</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">

                            <div class="collapse " id="compare-ram">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Internal Memory In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Apple iPhone 7 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 256 GB </td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 128 GB </td>
                                            <td> 128 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 7 Plus 128 GB</td>
                                            <td> 128 GB</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td rowspan="4" class="grey-lighter-3"><strong>CAMERA</strong> </td>
                        <td>Primary Camera</td>
                        <td><a href="#compare-camera-1" class="collapsed" data-toggle="collapse">20 MP<i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td>12 MP </td>
                        <td>12 MP </td>
                        <td>Average</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">

                            <div class="collapse " id="compare-camera-1">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Internal Memory In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Apple iPhone 7 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 256 GB </td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 128 GB </td>
                                            <td> 128 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 7 Plus 128 GB</td>
                                            <td> 128 GB</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>Secondary Camera</td>
                        <td><a href="#compare-camera-2" class="collapsed" data-toggle="collapse">8 MP<i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td>8 MP </td>
                        <td>7 MP </td>
                        <td>Good</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">

                            <div class="collapse " id="compare-camera-2">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Internal Memory In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Apple iPhone 7 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 256 GB </td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 128 GB </td>
                                            <td> 128 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 7 Plus 128 GB</td>
                                            <td> 128 GB</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td rowspan="2" class="grey-lighter-3"><strong>BATTERY</strong> </td>
                        <td>Battery Capacity</td>
                        <td><a href="#compare-battery" class="collapsed" data-toggle="collapse">4000 mAh<i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td>2900 mAh </td>
                        <td>2691 mAh </td>
                        <td>Average</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">

                            <div class="collapse" id="compare-battery">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Internal Memory In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Apple iPhone 7 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 256 GB </td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 128 GB </td>
                                            <td> 128 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 7 Plus 128 GB</td>
                                            <td> 128 GB</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td rowspan="4" class="grey-lighter-3"><strong>BODY</strong> </td>
                        <td>Thickness</td>
                        <td><a href="#compare-thickness" class="collapsed" data-toggle="collapse">7.1 mm<i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td>7.3 mm </td>
                        <td>7.5 mm </td>
                        <td>Very Good</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">

                            <div class="collapse " id="compare-thickness">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Internal Memory In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Apple iPhone 7 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 256 GB </td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 128 GB </td>
                                            <td> 128 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 7 Plus 128 GB</td>
                                            <td> 128 GB</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>Weight</td>
                        <td><a href="#compare-weight" class="collapsed" data-toggle="collapse">143 g<i class="fas fa-fw fa-angle-down"></i></a> </td>
                        <td>188 g </td>
                        <td>202 g </td>
                        <td>Below Average</td>
                    </tr>
                    <tr class="table-reset">
                        <td colspan="3">

                            <div class="collapse " id="compare-weight">
                                <div class="pad-20">
                                    <div class="bold f-13 m-b-5">Products With Best Internal Memory In This Price Range</div>
                                    <table class="table f-12 table-bordered table-compare m-b-0">
                                        <tr>
                                            <td>Apple iPhone 7 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 Plus 256 GB</td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 8 256 GB </td>
                                            <td> 256 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Google Pixel 2 128 GB </td>
                                            <td> 128 GB</td>
                                        </tr>
                                        <tr>
                                            <td>Apple iPhone 7 Plus 128 GB</td>
                                            <td> 128 GB</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>

                </table></div>
                
                    
                    <div class="border">
                        <div class="row">
                            <div class="col-md-auto blue white-text d-flex align-items-center"><div class="pad-15 f-c f-16">Product Score: <span class="f-22">61</span> / 100</div></div>
                            <div class="col-md">
                               <div class="pad-20 ">
                               <div class="row align-items-center">
                                   <div class="col-md"> <div class="row align-items-center no-gutters">
                                        <div class="col">
                                            <div class="input"> Apple iPhone 8 Plus </div>
                                        </div>
                                        <div class="col-auto"><hr class="width-em f-12"></div>
                                        <div class="col-auto"><div class="circle-stroke grey-light-border f-29"><span class="f-14">Vs</span></div></div>
                                        <div class="col-auto"><hr class="width-em f-12"></div>
                                        <div class="col">
                                            <input type="text" class="input w-100" placeholder="Type product name to compare">
                                        </div>
                                    </div></div>

                                    <div class="col-md-auto sm-m-t-10"><button class="btn blue">Compare</button></div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="product-section" id="spec">
                    <h3 class="f-c f-22 m-t-20 m-b-20 w-400">Apple IPhone 7 Specifications</h3>
                    <div class="table-responsive"><table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td colspan="2" class="grey-lighter-2">DISPLAY</td>
                            </tr>
                            <tr>
                                <td>Screen Resolution</td>
                                <td>750x1334 pixels</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="grey-lighter-2">MEMORY</td>
                            </tr>
                            <tr>
                                <td>Internal Memory</td>
                                <td>32GB</td>
                            </tr>
                            
                            <tr>
                                <td>Weight</td>
                                <td><span itemprop="weight">138 grams</span>
                                </td>
                            </tr>
                           
                            <tr>
                                <td colspan="2" class="grey-lighter-2">CAMERA</td>
                            </tr>
                            
                            <tr>
                                <td>Primary Camera </td>
                                <td>12 M.Pixels</td>
                            </tr>
                            <tr>
                                <td>Secondary Camera	</td>
                                <td>7 M.Pixels</td>
                            </tr>
                            
                            
                            <tr>
                                <td>RAM</td>
                                <td>2GB</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="grey-lighter-2">BATTERY</td>
                            </tr>
                            <tr>
                                <td>Battery Capacity</td>
                                <td>	2691 mAh</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="grey-lighter-2">BODY</td>
                            </tr>
                            <tr>
                                <td>Thickness</td>
                                <td><span itemprop="weight"> 7.5 mm</span>
                                </td>
                            </tr>
                        </tbody>
                    </table></div>
                </div>
                <hr>
                <div class="product-section" id="review">
                    <h3 class="f-c f-22 m-t-20 m-b-20 w-400">User Reviews Of Apple IPhone 7</h3>
                         <div class="row m-b-20">
                            <div class="col">
                                <div class="row align-items-center m-b-20">
                                    <div class="col-auto">
                                        <div class="rating-score-all text-center">
                                            <div class="f-30 w-300 l-1-1 green-text">3.9</div>
                                            <div>out of 5</div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="stars-all yellow-text f-23 l-1-0 m-b-5">
                                            <i class="material-icons">star</i>
                                            <i class="material-icons">star</i>
                                            <i class="material-icons">star</i>
                                            <i class="material-icons">star_half</i>
                                            <i class="material-icons">star_border</i>
                                        </div>
                                        <div class="rating-meta-all">
                                            29 Ratings &#8226; 7 Reviews
                                        </div>
                                    </div>

                                </div>
                                <div class="rating-bars-list m-b-20 m-r-20">
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">5 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">4 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">3 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">2 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="bar-star col-auto d-flex align-items-center">1 <i class="material-icons grey-lighter-text f-22 m-l-5 l-1-0">star</i>
                                        </div>
                                        <div class="col">
                                            <div class="rating-bar ">
                                                <div class="rating-value green" style="width: 30%"></div>
                                            </div>
                                        </div>
                                        <div class="bar-total-stars col-auto">12</div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-auto">
                                <div class="well text-center h-100 d-flex align-items-center">
                                    <div class="m-l-10 m-r-10">
                                        <h4>Have you used this product?</h4>
                                        <p>Rate & review this product and help other customers</p>
                                        <a href="#addReview" data-target="#addReview" data-toggle="modal" class="btn blue">Write a product review</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="f-18 f-c w-400 m-0">User Reviews</h4>

                        <div class="grey-light-text f-12">Showing 7 of 7 Reviews</div>
                        <hr>
                        <?php for($i=0;$i<7;$i++){ ?>
                        <div class="user-review-item">
                            <div class="d-flex align-items-center m-b-10">
                                <div class="user-stars yellow-text f-21 l-1-0">
                                    <i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i>
                                </div>
                                <div class="user-rating-value">5/5</div>
                                <h4 class="review-title m-0">No regrets.</h4>
                            </div>

                            <div class="review-body">
                                <p>By MySmartPrice User</p>
                                <p>It's slim and handy. Its performance is excellent, and it also has a very soft touchscreen.</p>
                            </div>
                        </div>
                        <?php }?>
                        
                        
                        
                </div>
                
                <div class="product-section" id="video">
                    <h3 class="f-c f-22 m-t-20 m-b-20 w-400">Video Reviews</h3>
                    <div class="carousel-video">
                      <div class="item video-item">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/zj7FotGbIbE?rel=0&amp;showinfo=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                       <?php for($i=0;$i<4;$i++){ ?>
                        <div class="item video-item">
                            <div class="embed-responsive embed-responsive-16by9">
                                <video class="embed-responsive-item" controls>
                                    <source src="video/new-revised.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
                <hr>
                <div class="product-section" id="pricemore">
                    <h3 class="f-c f-22 m-t-20 m-b-20 w-400">Apple IPhone 7 Price In India</h3>
                    <div class="table-responsive"><table class="table border">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Price</th>
                                        <th>askmeoffer Price</th>
                                        <th class="text-right">
                                            <select name="" id="demo" class="input">
                                                <option value="">Price High to Low</option>
                                                <option value="">Price Low to High</option>
                                                <option value="">Store rating</option>
                                            </select>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src="img/amazon_store.png" alt="" width="100">
                                            <div><a href="">More details <i class="fas fa-angle-right"></i></div>
                                        </td>
                                        <td class="align-middle">
                                            <i class="far fa-check-circle green-text"></i> 2-4 days delivery<br> <i class="far fa-check-circle green-text"></i> EMI: Available
                                        </td>
                                        <td class="f-20 align-middle l-1-3">
                                            <span class="text-strike">₹42,999</span>  <div class="f-14"><span class="green-text">Free</span> Shipping</div>
                                        </td>
                                        <td class="f-20 green-text">₹42,600</td>
                                        <td class="text-right align-middle">
                                            <a href="" class="btn blue">Go to store  <i class="fas fa-angle-right fa-fw"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="img/gadgetsnow_store.png" alt="" width="100">
                                            <div><a href="">More details <i class="fas fa-angle-right"></i></div>
                                        </td>
                                        <td class="align-middle">
                                            <i class="far fa-check-circle green-text"></i> 2-4 days delivery<br> <i class="far fa-check-circle green-text"></i> EMI: Available
                                        </td>
                                        <td class="f-20 align-middle l-1-3">
                                            <span class="text-strike">₹42,999</span>  <div class="f-14"><span class="green-text">Free</span> Shipping</div>
                                        </td>
                                        <td class="f-20 green-text">₹42,600</td>
                                        <td class="text-right align-middle">
                                            <a href="" class="btn blue">Go to store <i class="fas fa-angle-right fa-fw"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="img/ebay_store.png" alt="" width="100">
                                            <div><a href="">More details <i class="fas fa-angle-right"></i></div>
                                        </td>
                                        <td class="align-middle">
                                            <i class="far fa-check-circle green-text"></i> 2-4 days delivery<br> <i class="far fa-check-circle green-text"></i> EMI: Available
                                        </td>
                                        <td class="f-20 align-middle l-1-3">
                                            <span class="text-strike">₹42,999</span>  <div class="f-14"><span class="green-text">Free</span> Shipping</div>
                                        </td>
                                        <td class="f-20 green-text">₹42,600</td>
                                        <td class="text-right align-middle">
                                            <a href="" class="btn blue">Go to store <i class="fas fa-angle-right fa-fw"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="img/flipkart_store.png" alt="" width="100">
                                            <div><a href="">More details <i class="fas fa-angle-right"></i></div>
                                        </td>
                                        <td class="align-middle">
                                            <i class="far fa-check-circle green-text"></i> 2-4 days delivery<br> <i class="far fa-check-circle green-text"></i> EMI: Available
                                        </td>
                                        <td class="f-20 align-middle l-1-3">
                                            <span class="text-strike">₹42,999</span>  <div class="f-14"><span class="green-text">Free</span> Shipping</div>
                                        </td>
                                        <td class="f-20 green-text">₹42,600</td>
                                        <td class="text-right align-middle">
                                            <a href="" class="btn blue">Go to store <i class="fas fa-angle-right fa-fw"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table></div>
                    
                    <div class="text-center  coupons-pagination paginate m-t-15"><span class="paging_no_page first_page">&nbsp;</span> <span class="paging_no_page previous_page">&nbsp; </span> &nbsp; <span class="paging_selected_page paginate_active">1</span> <a href="javascript:void(0);" class="paging_page pages" id="2">2</a> &nbsp;<a href="javascript:void(0);" id="2" class="paging_page next_page">&nbsp;</a> <a href="javascript:void(0);" id="2" class="paging_page last_page">&nbsp;</a>
                        </div>
                            
                               
                               
                               <div class="border blue-border m-t-20">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <div class="pad-20 blue white-text f-30 l-1-0"><i class="far fa-bell m-r-5 m-l-5"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="f-c f-20">Didn't find the right price?<span class="red-text"> Set price drop alert now</span>
                                        </div>
                                        <div>We'll notify you as soon as the price drops</div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="pad-15"><a href="" class="btn blue">Set alert</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <ul class="list">
                                <li>All prices in the above table are in Indian Rupees.</li>
                                <li>Cashback will not be credited for purchases in Amazon.</li>
                                <li>The latest price of Apple iPhone 7 was obtained on 6th February 2018 12:55:19</li>
                                <li>The Apple iPhone 7 is available in Amazon, Croma, Ebay, Flipkart, Gadgets360, Gadgetsnow, Shopclues, Tatacliq</li>
                                <li>The price is valid in all major cities of India including Bangalore, Hyderabad, Chennai, Mumbai, Delhi and Pune. Please check instructions at the specific stores for any deviation.</li>
                                <li>Buy Apple iPhone 7 online in India for the lowest price. You can also avail the options of Cash on Delivery (COD) and EMI on purchase of this product. Apple iPhone 7 is available in Gold, Black, Silver colours across various online stores in India.</li>
                            </ul>
                </div>
                <hr>
                <div class="product-section" id="pricehistory">
                                <h3 class="f-c f-22 m-t-20 m-b-20 w-400">Price History</h3>
                                <div class="grey-lighter-3 pad-20">
                                    <div class="f-16 m-b-10">PERCENT CHANGE IN PRICE</div>
                                    <div class="row m-b-20">
                                        <div class="col-auto">
                                            <div class="f-12">SINCE LAUNCH</div>
                                            <div class="green-text f-20"><i class="f-16 fas fa-fw fa-arrow-circle-down"></i>29.45%</div>
                                        </div>
                                        <div class="col-auto">
                                            <div class="vr grey-lighter-border"></div>
                                        </div>
                                        <div class="col-auto">
                                            <div class="f-12">LAST 30 DAYS</div>
                                            <div class="green-text f-20"><i class="f-16 fas fa-fw fa-arrow-circle-down"></i>31.21%</div>
                                        </div>
                                    </div>




                                    <div class="sctn sctn--no-pdng prc-grph clearfix" data-id="price-history">
                                        <div class="sctn__inr">
                                            <div class="prc-grph__rght">
                                                <div class="prc-grph__rght-chrt c3" id="svg_graph" style="display: block; max-height: 270px; position: relative;"></div>
                                                <div class="prc-grph__btn-wrpr d-flex align-items-center justify-content-end">
                                                    <span class="prc-grph__btn-ttl m-r-10">Filter Graph By</span>
                                                    <div><input type="submit" class="btn btn--no-bg prc-grph__btn js-prc-grph__btn" value="1 month" data-range="30">
                                                        <input type="submit" class="btn btn--no-bg prc-grph__btn js-prc-grph__btn" value="3 month" data-range="90">
                                                        <input type="submit" class="btn btn--no-bg prc-grph__btn js-prc-grph__btn" value="6 month" data-range="183">
                                                        <input type="submit" class="btn btn--no-bg prc-grph__btn js-prc-grph__btn" value="1 Year" data-range="365">
                                                        <input type="submit" class="btn btn--no-bg prc-grph__btn js-prc-grph__btn btn--slctd" value="Till Date">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="text-center m-t-20">
                                    Apple iPhone 7 Price in India is <strong>Rs. 42,999</strong> as on 6th February 2018 16:46:38
                                </div>


                                

                                <h3 class="f-c f-22 m-t-20 m-b-20 w-400"> Popular Mobile Price Lists</h3>
                                <ul class="list cont-2">
                                    <li><a href="">Lenovo Mobile Price </a>
                                    </li>
                                    <li><a href="">Vivo Mobile Price </a>
                                    </li>
                                    <li><a href="">Oppo Mobile Price </a>
                                    </li>
                                    <li><a href="">Lyf Mobile Price </a>
                                    </li>
                                    <li><a href="">Lava Mobiles </a>
                                    </li>
                                    <li><a href="">Leeco Mobile Price</a>
                                    </li>
                                </ul>

                                <hr>

                                <h3 class="f-c f-22 m-t-20 m-b-20 w-400"> Popular Mobiles</h3>
                                <ul class="list cont-2">
                                    <li><a href="">Xiaomi Redmi Y1 </a>
                                    </li>
                                    <li><a href="">OnePlus 5T </a>
                                    </li>
                                    <li><a href="">Xiaomi Redmi 5A </a>
                                    </li>
                                    <li><a href="">Apple iPhone X</a>
                                    </li>
                                </ul>

                            </div>
                
               
                <div class="product-section" id="alt">
                    <h3 class="f-c f-22 m-t-20 m-b-20 w-400">Mobile Phones In Price Range Similar To Price Of Apple IPhone 7</h3>

                    <div class="row">
                        <?php for($i=0;$i<7;$i++){ ?>
                        <div class="col-md-4 col-6 m-b-10">
                            <a href="" class="border d-block pad-15 black-text rounded">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <img src="img/10208-70-1.jpg" width="22" alt="">
                                    </div>
                                    <div class="col">
                                        Apple iPhone 7 128GB
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php }?>
                    </div>
                    <hr>
                    <h4 class="f-c f-22 m-t-20 m-b-20 w-400">Latest News</h4>
                    <div class="blog-lists">
                        <div class="row m-b-20">
                            <div class="col-md-auto">
                                <a href="" class=""><img src="img/Apple-iPhone-7-vs-Apple-iPhone-7-Plus-vs-Samsung-Galaxy-S8-.jpg" width="145" alt=""></a>
                            </div>
                            <div class="col-md">
                                <div class="bold f-15">iPhone 7 vs iPhone 7 Plus vs Samsung Galaxy S8: Price in India, Specifications, Features compared</div>
                                <p>While the high-end smartphones segment is often the most talked about by tech enthusiasts and the general public alike the fact remains that most people still cannot afford the newest high-end flagship-class product when these products are new in the market However that isnt the case one year d</p>
                                <div class="text-right">
                                    <a href="">Read more <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-md-auto">
                                <a href="" class=""><img src="img/Apple-iPhone-7-vs-Apple-iPhone-7-Plus-vs-Samsung-Galaxy-S8-.jpg" width="145" alt=""></a>
                            </div>
                            <div class="col-md">
                                <div class="bold f-15">iPhone 7 vs iPhone 7 Plus vs Samsung Galaxy S8: Price in India, Specifications, Features compared</div>
                                <p>While the high-end smartphones segment is often the most talked about by tech enthusiasts and the general public alike the fact remains that most people still cannot afford the newest high-end flagship-class product when these products are new in the market However that isnt the case one year d</p>
                                <div class="text-right">
                                    <a href="">Read more <i class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <h4 class="f-c f-22 m-t-20 m-b-20 w-400">Best Selling Mobiles</h4>
                    
                    <div class="row">
                       <?php for($i=0;$i<8;$i++){ ?>
                        <div class="col-sm-6 col-lg-4 col-xl-3 m-b-10">
                            <div class="coupon-deal-item store-product-item border">
                                <figure><a href="#"><img alt="img" src="img/i-2.png"></a>
                                </figure>
                                <div class="f-14 w-700 m-b-5">Apple iPhone 6s (32 GB)</div>
                                <div class="stars-all yellow-text f-18 l-1-0 m-b-10">
                                    <i class="material-icons">star</i>
                                    <i class="material-icons">star</i>
                                    <i class="material-icons">star</i>
                                    <i class="material-icons">star_half</i>
                                    <i class="material-icons">star_border</i>
                                </div>
                                <div class="d-flex pad-5 line-sep justify-content-center align-items-center"> <span class="f-14">from </span> <span class="f-17 w-700  m-r-5 m-l-5">₹20.999</span> <span class="f-14">(3 stores) </span>
                                </div>
                                <div class="line-sep">
                                    <div class="pad-10 ">
                                        Disounted price <strong class="green-text f-17">₹20.999</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    
                </div>
                
                
                
            </div>
        </div>
    </div>

    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <div class="modal fade " id="addReview" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
            <div class="modal-content ">
                <div class="modal-header justify-content-center">
                    <h5 class="modal-title f-16 f-c w-400 l-1-0 text-center blue-text m-auto">Add a review</h5>
                    <button type="button" class="close clos-abs" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                


                </div>
                <div class="modal-body pad-0 terms-body">
                    <div class="pad-20">
                        <div class="m-b-10">Adding rating as <strong>Spongebob</strong></div>
                        <div class="m-b-10"><input type="text" placeholder="Review title" class="input w-100"></div>
                        <div class="m-b-10">
                            <div class="star-rater f-32">
                                <label class="material-icons">star<input type="radio" name="rate" value="5"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="4"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="3"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="2"></label>
                                <label class="material-icons">star<input type="radio" name="rate" value="1"></label>
                            </div>
                        </div>
                        <div class="m-b-10"><textarea class="input w-100" rows="5" placeholder="Insert your rewiew here"></textarea></div>
                        
                        <button class="btn blue">Submit Rating</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/d3.min.js"></script>
    <script src="js/c3.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript">
function drawPriceGraph(response) {
	console.log(response);
	var gradient;
    
    graphData = response;
    
    gradient = [
		'<linearGradient id="gradient" x1="50%" y1="0%" x2="50%" y2="100%">',
			'<stop offset="0%" style="stop-color:red;stop-opacity:1" />',
			'<stop offset="100%" style="stop-color:green;stop-opacity:1" />',
		'</linearGradient>'
	].join("");

    chart = c3.generate({
		padding: {
			right: 20,
			top: 10
		},
		bindto: '.prc-grph__rght-chrt',
		data: {
			x: "date",
			json: getPoints(graphData, 6),
		 	keys: {
				value: ['date', 'value'],
			},
			colors: {
		 		value: 'url(#gradient)'
			},
			type: "spline",
		},
		grid: {
			y: {
				show: true
			}
		},
		point: {
			show: true,
			r: 4,
			focus: {
				expand: {
					r: 5
				}
			}
		},
		legend: {
		    show: false
		},
		axis: {
			x: {
				type: 'timeseries', 
			    tick: {
			    	count: 6,
			    	fit: true,
		            type : 'timeseries',
		            format: getXAxisTicks(graphData.length)
			    },
			},
		    y: {	
		        tick : {
		        	count : 6,
		        	fit: true,
		        	format: function (y) { 
		        		y = "Rs. " + parseInt(y, 10).toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
						return y;
					}
		    	}
		    }
		},
		tooltip : {
			format: {
		        title: function(d) { 
		        	return d3.time.format("%d, %B %Y")(d) 
				},
		        name: function(name, ratio, id, index) { return "Price"; }
		    }
		},
		oninit: function() {
			this.svg[0][0].getElementsByTagName('defs')[0].innerHTML += gradient;
		}
	});
}

function getXAxisTicks(days) {
	var format;

	if (days <= 30) {
		format = "%d %b";
	} else if (days <=90) {
		format = "%d %b";
	} else if (days <=365) {
		format = "%b";
	} else if (days >= 365) {
		format = "%b %y";
	}

	return format;
}

function getPoints(arr, pointCount) { 
    var pointArr = [],
    	length = arr.length,
    	j = 0, i, inc;

    inc = Math.floor(arr.length / pointCount); 
    
    for (i = 0; i < arr.length; i = i + inc) {
        pointArr[j] = arr[i]; 
        j++;
    }

    return pointArr;
}
</script>
<script type="text/javascript">
	$(document).on("click", ".prc-grph__btn", function() {
		var newGraphData = $.extend([], graphData),
			days, newGraphData, dataToPlot, ticks;
	    	
	    days = $(this).data("range") || newGraphData.length;
	    newGraphData = newGraphData.splice(newGraphData.length - days,days);
	    dataToPlot = getPoints(newGraphData, 6);
	    ticks = getXAxisTicks(days);

	    $(".js-prc-grph__btn").removeClass("btn--slctd");
	    $(this).addClass("btn--slctd");

	    chart.internal.loadConfig({
			axis: {
				x: { 
					tick: { 
						format: getXAxisTicks(days) 
					} 
				} 
			}
		});

	    chart.load({
	    	json: dataToPlot,
	    	keys: {
				value: ['date', 'value'],
			}
		});
	});
	$.getScript("https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.js", function() {
		$.getScript("https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js",function(){
			var response=[{"date":"2017-02-17","value":"49952"},{"date":"2017-02-18","value":"49891"},{"date":"2017-02-19","value":"49829"},{"date":"2017-02-20","value":"49768"},{"date":"2017-02-21","value":"49729"},{"date":"2017-02-22","value":"49691"},{"date":"2017-02-23","value":"49652"},{"date":"2017-02-24","value":"49614"},{"date":"2017-02-26","value":"49537"},{"date":"2017-02-27","value":"49499"},{"date":"2017-02-28","value":"49499"},{"date":"2017-03-01","value":"49499"},{"date":"2017-03-02","value":"49499"},{"date":"2017-03-03","value":"49499"},{"date":"2017-03-04","value":"49499"},{"date":"2017-03-05","value":"49499"},{"date":"2017-03-06","value":"49499"},{"date":"2017-03-07","value":"49499"},{"date":"2017-03-08","value":"49499"},{"date":"2017-03-09","value":"49499"},{"date":"2017-03-10","value":"49499"},{"date":"2017-03-11","value":"49499"},{"date":"2017-03-12","value":"49499"},{"date":"2017-03-13","value":"49499"},{"date":"2017-03-14","value":"49356"},{"date":"2017-03-15","value":"49213"},{"date":"2017-03-16","value":"49070"},{"date":"2017-03-17","value":"48927"},{"date":"2017-03-18","value":"48784"},{"date":"2017-03-19","value":"48499"},{"date":"2017-03-20","value":"48213"},{"date":"2017-03-21","value":"47992"},{"date":"2017-03-22","value":"47770"},{"date":"2017-03-23","value":"47627"},{"date":"2017-03-24","value":"47485"},{"date":"2017-03-25","value":"47342"},{"date":"2017-03-26","value":"47342"},{"date":"2017-03-27","value":"47342"},{"date":"2017-03-28","value":"47342"},{"date":"2017-03-29","value":"47420"},{"date":"2017-03-30","value":"47499"},{"date":"2017-03-31","value":"47499"},{"date":"2017-04-01","value":"47499"},{"date":"2017-04-02","value":"47499"},{"date":"2017-04-03","value":"47499"},{"date":"2017-04-04","value":"47499"},{"date":"2017-04-05","value":"47499"},{"date":"2017-04-06","value":"47499"},{"date":"2017-04-07","value":"47499"},{"date":"2017-04-08","value":"47499"},{"date":"2017-04-09","value":"47499"},{"date":"2017-04-10","value":"47499"},{"date":"2017-04-11","value":"47499"},{"date":"2017-04-12","value":"47499"},{"date":"2017-04-13","value":"47499"},{"date":"2017-04-14","value":"47499"},{"date":"2017-04-15","value":"47499"},{"date":"2017-04-16","value":"47499"},{"date":"2017-04-17","value":"47499"},{"date":"2017-04-18","value":"47499"},{"date":"2017-04-19","value":"47499"},{"date":"2017-04-20","value":"47499"},{"date":"2017-04-21","value":"47499"},{"date":"2017-04-22","value":"47499"},{"date":"2017-04-23","value":"47413"},{"date":"2017-04-24","value":"47327"},{"date":"2017-04-25","value":"47184"},{"date":"2017-04-26","value":"47041"},{"date":"2017-04-27","value":"46899"},{"date":"2017-04-28","value":"46756"},{"date":"2017-04-29","value":"46527"},{"date":"2017-04-30","value":"46413"},{"date":"2017-05-01","value":"46270"},{"date":"2017-05-02","value":"46184"},{"date":"2017-05-03","value":"45970"},{"date":"2017-05-04","value":"45756"},{"date":"2017-05-05","value":"45542"},{"date":"2017-05-06","value":"45399"},{"date":"2017-05-07","value":"45228"},{"date":"2017-05-08","value":"44956"},{"date":"2017-05-09","value":"44685"},{"date":"2017-05-10","value":"44542"},{"date":"2017-05-11","value":"44399"},{"date":"2017-05-12","value":"43684"},{"date":"2017-05-13","value":"43556"},{"date":"2017-05-14","value":"43284"},{"date":"2017-05-15","value":"43141"},{"date":"2017-05-16","value":"42999"},{"date":"2017-05-17","value":"43127"},{"date":"2017-05-18","value":"43127"},{"date":"2017-05-19","value":"43770"},{"date":"2017-05-20","value":"43842"},{"date":"2017-05-21","value":"44056"},{"date":"2017-05-22","value":"44269"},{"date":"2017-05-23","value":"44482"},{"date":"2017-05-24","value":"44424"},{"date":"2017-05-25","value":"44494"},{"date":"2017-05-26","value":"44437"},{"date":"2017-05-27","value":"44378"},{"date":"2017-05-28","value":"44021"},{"date":"2017-05-29","value":"43964"},{"date":"2017-05-30","value":"43907"},{"date":"2017-05-31","value":"43849"},{"date":"2017-06-01","value":"43779"},{"date":"2017-06-02","value":"43655"},{"date":"2017-06-03","value":"43642"},{"date":"2017-06-04","value":"43500"},{"date":"2017-06-05","value":"43058"},{"date":"2017-06-06","value":"42929"},{"date":"2017-06-07","value":"42801"},{"date":"2017-06-08","value":"42685"},{"date":"2017-06-09","value":"42605"},{"date":"2017-06-10","value":"42415"},{"date":"2017-06-11","value":"42624"},{"date":"2017-06-12","value":"42834"},{"date":"2017-06-13","value":"42730"},{"date":"2017-06-14","value":"42627"},{"date":"2017-06-15","value":"42523"},{"date":"2017-06-16","value":"42494"},{"date":"2017-06-17","value":"42465"},{"date":"2017-06-18","value":"42465"},{"date":"2017-06-19","value":"42441"},{"date":"2017-06-20","value":"42417"},{"date":"2017-06-21","value":"42322"},{"date":"2017-06-22","value":"42227"},{"date":"2017-06-23","value":"42132"},{"date":"2017-06-24","value":"42037"},{"date":"2017-06-25","value":"41942"},{"date":"2017-06-26","value":"41871"},{"date":"2017-06-27","value":"41800"},{"date":"2017-06-28","value":"41800"},{"date":"2017-06-29","value":"41964"},{"date":"2017-06-30","value":"42128"},{"date":"2017-07-01","value":"42292"},{"date":"2017-07-02","value":"42749"},{"date":"2017-07-03","value":"42913"},{"date":"2017-07-04","value":"43499"},{"date":"2017-07-05","value":"43799"},{"date":"2017-07-06","value":"43934"},{"date":"2017-07-07","value":"44070"},{"date":"2017-07-08","value":"44070"},{"date":"2017-07-09","value":"43777"},{"date":"2017-07-10","value":"44326"},{"date":"2017-07-11","value":"44326"},{"date":"2017-07-12","value":"44612"},{"date":"2017-07-13","value":"44963"},{"date":"2017-07-14","value":"45376"},{"date":"2017-07-15","value":"45812"},{"date":"2017-07-16","value":"46248"},{"date":"2017-07-17","value":"46135"},{"date":"2017-07-18","value":"46149"},{"date":"2017-07-19","value":"46163"},{"date":"2017-07-20","value":"46326"},{"date":"2017-07-21","value":"46213"},{"date":"2017-07-22","value":"46213"},{"date":"2017-07-23","value":"46285"},{"date":"2017-07-24","value":"46349"},{"date":"2017-07-25","value":"46413"},{"date":"2017-07-26","value":"46478"},{"date":"2017-07-27","value":"46328"},{"date":"2017-07-28","value":"46392"},{"date":"2017-07-29","value":"46457"},{"date":"2017-08-07","value":"44971"},{"date":"2017-08-08","value":"42335"},{"date":"2017-08-09","value":"39699"},{"date":"2017-08-10","value":"37063"},{"date":"2017-08-11","value":"34427"},{"date":"2017-08-12","value":"32284"},{"date":"2017-08-13","value":"32713"},{"date":"2017-08-14","value":"32569"},{"date":"2017-08-15","value":"34567"},{"date":"2017-08-16","value":"36566"},{"date":"2017-08-17","value":"38565"},{"date":"2017-09-02","value":"39030"},{"date":"2017-09-03","value":"39060"},{"date":"2017-09-04","value":"39322"},{"date":"2017-09-05","value":"39554"},{"date":"2017-09-06","value":"39968"},{"date":"2017-09-07","value":"40382"},{"date":"2017-09-08","value":"40726"},{"date":"2017-09-09","value":"41070"},{"date":"2017-09-10","value":"41414"},{"date":"2017-09-11","value":"41583"},{"date":"2017-09-12","value":"41752"},{"date":"2017-09-13","value":"41752"},{"date":"2017-09-14","value":"41752"},{"date":"2017-09-15","value":"41823"},{"date":"2017-09-16","value":"41523"},{"date":"2017-09-17","value":"41223"},{"date":"2017-09-18","value":"40680"},{"date":"2017-09-19","value":"40136"},{"date":"2017-09-20","value":"39694"},{"date":"2017-09-21","value":"39251"},{"date":"2017-09-22","value":"38808"},{"date":"2017-09-23","value":"38758"},{"date":"2017-09-24","value":"38671"},{"date":"2017-09-25","value":"38770"},{"date":"2017-09-26","value":"38870"},{"date":"2017-09-27","value":"38855"},{"date":"2017-09-28","value":"38841"},{"date":"2017-09-29","value":"38827"},{"date":"2017-09-30","value":"39247"},{"date":"2017-10-01","value":"39419"},{"date":"2017-10-02","value":"39521"},{"date":"2017-10-03","value":"39622"},{"date":"2017-10-04","value":"39723"},{"date":"2017-10-05","value":"39825"},{"date":"2017-10-06","value":"39926"},{"date":"2017-10-07","value":"39356"},{"date":"2017-10-08","value":"39077"},{"date":"2017-10-09","value":"38870"},{"date":"2017-10-10","value":"38624"},{"date":"2017-10-11","value":"38344"},{"date":"2017-10-12","value":"38064"},{"date":"2017-10-13","value":"37784"},{"date":"2017-10-14","value":"37694"},{"date":"2017-10-15","value":"37597"},{"date":"2017-10-16","value":"37544"},{"date":"2017-10-17","value":"37530"},{"date":"2017-10-18","value":"37549"},{"date":"2017-10-19","value":"37569"},{"date":"2017-10-20","value":"37588"},{"date":"2017-10-21","value":"37792"},{"date":"2017-10-22","value":"37996"},{"date":"2017-10-23","value":"38157"},{"date":"2017-10-24","value":"38317"},{"date":"2017-10-25","value":"38478"},{"date":"2017-10-26","value":"38638"},{"date":"2017-10-27","value":"38799"},{"date":"2017-10-28","value":"38799"},{"date":"2017-10-29","value":"38799"},{"date":"2017-10-30","value":"38799"},{"date":"2017-10-31","value":"38799"},{"date":"2017-11-01","value":"38799"},{"date":"2017-11-02","value":"38799"},{"date":"2017-11-03","value":"38799"},{"date":"2017-11-04","value":"38799"},{"date":"2017-11-05","value":"38799"},{"date":"2017-11-06","value":"38799"},{"date":"2017-11-07","value":"38799"},{"date":"2017-11-08","value":"38799"},{"date":"2017-11-09","value":"38799"},{"date":"2017-11-10","value":"38799"},{"date":"2017-11-11","value":"38799"},{"date":"2017-11-12","value":"38799"},{"date":"2017-11-13","value":"38799"},{"date":"2017-11-14","value":"38799"},{"date":"2017-11-15","value":"38799"},{"date":"2017-11-16","value":"38799"},{"date":"2017-11-17","value":"38799"},{"date":"2017-11-18","value":"38799"},{"date":"2017-11-19","value":"38899"},{"date":"2017-11-20","value":"38999"},{"date":"2017-11-21","value":"38813"},{"date":"2017-11-22","value":"38628"},{"date":"2017-11-23","value":"39048"},{"date":"2017-11-24","value":"39469"},{"date":"2017-11-25","value":"39889"},{"date":"2017-11-26","value":"40210"},{"date":"2017-11-27","value":"40530"},{"date":"2017-11-28","value":"41172"},{"date":"2017-11-29","value":"41813"},{"date":"2017-11-30","value":"41848"},{"date":"2017-12-01","value":"41884"},{"date":"2017-12-02","value":"41919"},{"date":"2017-12-03","value":"41954"},{"date":"2017-12-04","value":"41705"},{"date":"2017-12-05","value":"41421"},{"date":"2017-12-06","value":"41136"},{"date":"2017-12-07","value":"40852"},{"date":"2017-12-08","value":"40852"},{"date":"2017-12-09","value":"40852"},{"date":"2017-12-10","value":"40852"},{"date":"2017-12-11","value":"41136"},{"date":"2017-12-12","value":"41421"},{"date":"2017-12-13","value":"41705"},{"date":"2017-12-14","value":"41990"},{"date":"2017-12-15","value":"41990"},{"date":"2017-12-16","value":"41990"},{"date":"2017-12-17","value":"41990"},{"date":"2017-12-18","value":"41990"},{"date":"2017-12-19","value":"41962"},{"date":"2017-12-20","value":"41935"},{"date":"2017-12-21","value":"41908"},{"date":"2017-12-22","value":"41908"},{"date":"2017-12-23","value":"41874"},{"date":"2017-12-24","value":"41840"},{"date":"2017-12-25","value":"41770"},{"date":"2017-12-26","value":"41727"},{"date":"2017-12-27","value":"41684"},{"date":"2017-12-28","value":"41619"},{"date":"2017-12-29","value":"41481"},{"date":"2017-12-30","value":"41378"},{"date":"2017-12-31","value":"41275"},{"date":"2018-01-01","value":"41203"},{"date":"2018-01-02","value":"41230"},{"date":"2018-01-03","value":"41258"},{"date":"2018-01-04","value":"41307"},{"date":"2018-01-05","value":"41401"},{"date":"2018-01-06","value":"41496"},{"date":"2018-01-07","value":"41591"},{"date":"2018-01-08","value":"41690"},{"date":"2018-01-09","value":"41690"},{"date":"2018-01-10","value":"41690"},{"date":"2018-01-11","value":"41690"},{"date":"2018-01-12","value":"41690"},{"date":"2018-01-13","value":"41690"},{"date":"2018-01-14","value":"41690"},{"date":"2018-01-15","value":"41690"},{"date":"2018-01-16","value":"41690"},{"date":"2018-01-17","value":"41690"},{"date":"2018-01-18","value":"41448"},{"date":"2018-01-19","value":"41349"},{"date":"2018-01-20","value":"41251"},{"date":"2018-01-21","value":"41152"},{"date":"2018-01-22","value":"41053"},{"date":"2018-01-23","value":"41053"},{"date":"2018-01-24","value":"41053"},{"date":"2018-01-25","value":"41295"},{"date":"2018-01-26","value":"41393"},{"date":"2018-01-27","value":"41492"},{"date":"2018-01-28","value":"41591"}];
			drawPriceGraph(response);
		});
	});
</script>
</body>

</html>