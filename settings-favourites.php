<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Settings-favourites</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab d-flex xs-tab-2">
                <li><a href="">Account Summary</a></li>
                <li><a href="">Activity</a></li>
                <li><a href="">Payments</a></li>
                <li><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li class="current"><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                <div class="row">
                <div class="col-md-auto">
                    <ul class="list-bordered border user-side-menu">
                        <li><a href="settings.php" class="">General</a></li>
                        <li><a href="settings-security.php">Reset password</a></li>
                        <li><a href="settings-payments.php">Payment</a></li>
                        <li><a href="settings-favourites.php" class="current">Favourites</a></li>
                        <li><a href="settings-instore.php">In-store</a></li>
                        <li><a href="settings-thirdpartyapps.php">Third-party apps</a></li>
                    </ul>
                </div>
                <div class="col-md">
                   <div class="m-l-20 sm-m-l-0 sm-m-t-20">
                    <h1 class="f-22 blue-text m-t-0">Favourites</h1>
                    <div class="settings-list">
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-4">store</div>
                                <div class="col-4">Cashback alerts</div>
                                <div class="col-3">Voucher alerts</div>
                                <div class="col-1"></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-4"><a href="">MORE TH>N Car Insurance</a>
                                </div>
                                <div class="col-4">
                                    Followed

                                </div>
                                <div class="col-3">
                                    Followed
                                </div>
                                <div class="col-1 text-right"><a href="#stng-1" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                            <div class="details collapse" id="stng-1">
                                <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-4"><a href="">Unfollow cashback alerts</a>
                                    </div>
                                    <div class="col-3"><a href="">Unfollow vouchers alerts</a>
                                    </div>
                                    <div class="col-1"></div>
                                </div>
                                <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-8">
                                        <div class="m-t-20">
                                            <button class="btn red sm-m-b-10">Remove favourite</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-1" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-4"><a href="">MORE TH>N Car Insurance</a>
                                </div>
                                <div class="col-4">
                                    Followed

                                </div>
                                <div class="col-3">
                                    Followed
                                </div>
                                <div class="col-1 text-right"><a href="#stng-2" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                            <div class="details collapse" id="stng-2">
                                <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-4"><a href="">Unfollow cashback alerts</a>
                                    </div>
                                    <div class="col-3"><a href="">Unfollow vouchers alerts</a>
                                    </div>
                                    <div class="col-1"></div>
                                </div>
                                <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-8">
                                        <div class="m-t-20">
                                            <button class="btn red sm-m-b-10">Remove favourite</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-2" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>