<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Activity</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab d-flex xs-tab-2">
                <li><a href="">Account Summary</a></li>
                <li><a href="">Activity</a></li>
                <li class="current"><a href="">Payments</a></li>
                <li><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                <h1 class="f-22 blue-text m-0">Payments</h1>
                
                <div class="row m-b-20">
                <div class="col-md"><div class="f-18">You can withdraw your earned cashback in a variety of ways...</div></div>
                <div class="col-md-auto text-right sm-text-left"><div class="f-18 green-text bold">Balance £0.00</div> <div class="f-12">Select payment method
</div></div>
                </div>
                
                
                <div class="table-responsive">
                <table class="table border-bottom">
                    
                    <tr>
                        <td><div class="f-20 green-text">Payment gateway <sup><span class="f-10">DEMO</span></sup></div></td>
                        <td class="border-right">
                            <ul class="list list-angle">
                                <li>Receive payment using an email address</li>
                                <li>Minimum withdrawal £1</li>
                            </ul>
                        </td>

                        <td align="right" class="text-right">
                            Your cashback balance is currently less than £1  
                        </td>
                        <td align="right">
                            <a href="" class="btn blue">Withdraw</a>
                        </td>
                    </tr>
                </table>
                </div>
                <div class="border pad-20 m-b-20">
                        
                        <div class="text-center">
                        <div class="f-24 blue-text">We'll show your paid cashback and payments here <br>when you have cashback to be paid to you</div>
                        <div class="m-b-20">To start earning cashback why not check out out latest offers</div>
                        <a href="" class="btn blue">View offers</a>
                        </div>
                    </div>
                
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>