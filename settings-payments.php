<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Settings-payment</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid">
        
        <div class="dash-body m-b-50 m-t-20">
            
            <div class="text-center m-b-20">
            <div class="user-image"><i class="fas fa-user-circle color-grey-400"></i> <a href="" class="upld-usr-img">Add Picture </a> </div>
            <h1 class="m-b-0 blue-text">Hi, Jane</h1>
            <div>Member since 2018 • Total earned <span class="green-text">£0.00</span></div></div>
            
            <ul class="tab-tab d-flex xs-tab-2">
                <li><a href="">Account Summary</a></li>
                <li><a href="">Activity</a></li>
                <li><a href="">Payments</a></li>
                <li><a href="">Refer askmeoffer</a></li>
                <li><a href="">My reviews</a></li>
                <li class="current"><a href="">Settings</a></li>
                <li><a href=""><span class="red-text">Sign out</span></a></li>
            </ul>
            <div class="border white pad-20 border-top-0">
                <div class="row">
                <div class="col-md-auto">
                    <ul class="list-bordered border user-side-menu">
                        <li><a href="settings.php" class="">General</a></li>
                        <li><a href="settings-security.php">Reset password</a></li>
                        <li><a href="settings-payments.php" class="current">Payment</a></li>
                        <li><a href="settings-favourites.php">Favourites</a></li>
                        <li><a href="settings-instore.php">In-store</a></li>
                        <li><a href="settings-thirdpartyapps.php">Third-party apps</a></li>
                    </ul>
                </div>
                <div class="col-md">
                   <div class="m-l-20 sm-m-l-0 sm-m-t-20">
                    <h1 class="f-22 blue-text m-t-0">Payment methods</h1>
<p>                    Your earned cashback can be withdrawn in a variety of means. All details will be stored safely and securely and can be changed at any time. To request a payment please visit the Payments page.
</p>

<p>If you update these details any requested withdrawals will be cancelled while we undertake security checks.
</p>                        <div class="settings-list">
                       <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Bank account details</strong>
                                </div>
                                <div class="col-md">
                                    <div class="details collapse" id="stng-1">
                                        <div class="m-t-20">
                                            <div class="m-b-10"><input type="text" class="input" size="50" placeholder="Bank account sort code">
                                            </div>
                                            <div class="m-b-10"><input type="text" class="input" size="50" placeholder="Bank account number ">
                                            </div>
                                            <div class="m-b-10"><input type="text" class="input" size="50" placeholder="Account holder's name">
                                            </div>
                                            <div class="m-b-10"><input type="text" class="input" size="50" placeholder="Your askmeoffer password">
                                            </div>
                                            <button class="btn blue">Save changes</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-1" data-toggle="collapse">Cancel</button>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-1" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        
                        <hr>
                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-md-auto"><strong class="acc-stng-lbl">Gift Cards</strong>
                                </div>
                                <div class="col-md">Unlinked
                                    <div class="details collapse" id="stng-2">
                                        <div class="m-t-20">
                                           <p>Use PayPal to purchase from stores.</p>
                                            <button class="btn blue">Link your PayPal account</button>
                                            <button class="btn grey-lighter-2" data-target="#stng-2" data-toggle="collapse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-auto sm-m-t-10">
                                    <a href="#stng-2" class="collapsed plus-trigger" data-toggle="collapse"><i class="fas fa-plus-circle blue-text f-20"></i></a>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class=" m-b-10">
                            <div class="row">
                                <div class="col-auto"><strong class="acc-stng-lbl">Gift Cards</strong>
                                </div>
                                <div class="col">
                                   asis.mailme@gmail.com
                                </div>
                            </div>
                        </div>
                        <hr>
                        

                    </div>
                    </div>
                </div>
                </div>
            </div>
            
        </div>
        
        
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
    <?php include('includes/lang-list.php'); ?>
    <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html>