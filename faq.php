<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Category</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flickity.css">
    <link rel="stylesheet" href="iconfont/material-icons.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include('includes/header.php'); ?>
    <div class="container-fluid m-b-20">
        <div class="white border">
           
        <ul class="idTabs tabs f-17 blue tab-blue  xs-tab-2">
            <li><a href="#idTab1">Why askmeoffer?</a></li>
            <li><a href="#idTab2">How Cashback Works?</a></li>
            <li><a href="#idTab3">askmeoffer Credits</a></li>
            <li><a href="#idTab4">Cashback FAQs</a></li>
            <li><a href="#idTab5">Appreciation Credits</a></li>
            <li><a href="#idTab6">Contact Us</a></li>
        </ul>
        
        <div class="tabs-container pad-15">
            <div id="idTab1">
                <div class="d-none d-md-block"><ul class="tab-tab d-flex xs-tab-2 ">
                    <li><a href="#l1" class="smooth-scroll">Best Cashback Platform</a></li>
                    <li><a href="#l2" class="smooth-scroll">Why askmeoffer Better?</a></li>
                    <li><a href="#l3" class="smooth-scroll">askmeoffer Vs Coupon Websites</a></li>
                    <li><a href="#l4" class="smooth-scroll">Useful Cashback Tools</a></li>
                </ul></div>
               
               <div class="border m-t-15">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Extra Cashback & All Savings on Shopping at one place</div>
                   <div class="pad-15 text-center">
                       <p>askmeoffer.com is the highest paying and trusted Cashback website in India. Here are the exclusive benefits askmeoffer.com offers to our customers.</p>
                       
                       <div class="row m-b-20 m-t-20 gutters-md"> 
                         <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-light-green-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-money-bill-alt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Earn Extra Cashback</div>
                           Same Products, Same Prices, But Earn Extra Cashback from 300+ Online Stores.</div>

                          <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-yellow-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-tag" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Hot Deals & Coupons</div>
                           Find Latest Discount Offers, Exclusive Coupons that would help you save on your Daily Spending.</div>

                          <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-orange-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-shopping-bag" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Freebies & Delights</div>
                           Find a Collection of latest Freebies, Contests & Stuff offered freely over the Internet.</div></div>
                       
                       
                   </div>
               </div>
               <div class="border m-t-15" id="l1">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Best Cashback Platform</div>
                   <div class="pad-15 text-center">
                       <p>Below are the top reasons our customers think askmeoffer.com is the best Cashback Website.</p>
                       
                       <div class="row m-b-20 m-t-20 gutters-md"> <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-brown-400" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-rupee-sign" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Highest Cashback Guarantee</div>
                           askmeoffer Guarantees to pay highest Cashback on all major Online Stores</div>

                          <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-teal-400" data-fa-transform="grow-6"></i>
                            <span class="fa-inverse fa-layers-text" data-fa-transform="shrink-11">100%</span>
                          </span>
                          <div class="f-16 f-c blue-text">No Minimum Limit</div>
                           You can Redeem 100% of your Cashback (Credits) Earned from askmeoffer. Anytime!</div>

    <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-blue-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-magic" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Many Options to Redeem</div>
                           You can Recharge, Pay Bills, Buy E-Gifts or do Bank Transfer using your askmeoffer Credits</div></div>
                       
                       
                   </div>
               </div>
               <div class="border m-t-15" id="l2">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Why askmeoffer a Better Cashback Website?</div>
                   <div class="pad-15 text-center">
                       <p>Below are some more highlights that suggest askmeoffer.com is a better Cashback & Coupons Website.

</p>
                       
                       <div class="row m-b-20 m-t-20 gutters-md"> <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-light-green-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-history" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Quick Tracking</div>
                           Orders at majority of the stores gets tracked Instantly or within a half day.

</div>

                          <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-yellow-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-question" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Best Customer Service
</div>
                           We provide you with best help possible in time so that you can save & earn more easily.

</div>

                        <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-orange-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-bolt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Instant Recharges
</div>
                           Enjoy Instant Recharges and Bill Payments using your Cashback Credits

</div>
                           
                         <div class="col-12 col-md m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-red-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-asterisk" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">No fee or Charges
</div>
                          Its 100% Free. We do not charge money to our customers for our services

</div>  
                        
                        </div>
                       
                       
                   </div>
               </div>
               <div class="border m-t-15" id="l3">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">How askmeoffer is better compare to Coupon websites?</div>
                   
                   <div class="pad-15 text-center">
                       <p>Now let us compare askmeoffer against Coupon Websites and see how you can save more, earn more on your shopping/ spending with askmeoffer.</p>
                       
                       <div class="d-inline-block bg-color-yellow-400 pad-10 m-b-10">Example of how askmeoffer Helps You Save More</div>
                       <div class="table-responsive">
                       <table class="table border text-center m-auto" style="max-width: 800px">
                           <tr>
                               <td width="33.33%">Coupon website</td>
                               <td width="33.33%">vs</td>
                               <td width="33.33%"><img src="img/logo.svg"></td>
                           </tr>
                           <tr>
                               <td>Rs.2500</td>
                               <td class="orange-text">E.G. Price from an Online Store</td>
                               <td>Rs.2500</td>
                           </tr>
                           <tr>
                               <td><div class="bg-color-yellow-100 pad-5 align-middle yellow-border border">20%</div></td>
                               <td class="orange-text align-middle">usual 20% off coupon</td>
                                   <td><div class="bg-color-yellow-100 pad-5 align-middle yellow-border border">20%</div></td>
                           </tr>
                           <tr>
                               <td>Rs.2000</td>
                               <td class="orange-text">After coupon price</td>
                               <td>Rs.2000</td>
                           </tr>
                           <tr>
                               <td>Rs.0</td>
                               <td class="orange-text">E.G. cashback to you (10%)</td>
                               <td>Rs.200</td>
                           </tr>
                           <tr>
                               <td class="align-middle"><span class="btn grey">Rs.2000</span></td>
                               <td class="orange-text align-middle">Price after cashback</td>
                                   <td class="align-middle"><span class="btn green">Rs.1800</span></td>
                           </tr>
                       </table>
                       </div>
                   </div>
               </div>
               
               
               
               
               <div class="row m-t-25" id="l4">
                  <div class="col-12 text-center f-c f-18 m-b-10">
                      askmeoffer Cashback Tools
                  </div>
                   <div class="col-md-6">
                       <div class="white border pad-15">
                           <div class="row align-items-center justify-content-center"><div class="col-auto"><img src="img/app02.png" height="220" alt=""></div>
                           <div class="col text-center">
                               <h3 class="f-18 blue-text">Cashback on the Go</h3>
                               <p>Use askmeoffer Android App to earn extra Cashback and do Recharges on the Go.</p>
                               <a href=""><img src="img/GooglePlay.png"></a>
                           </div></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="white border pad-15">
                           <div class="row align-items-center justify-content-center"><div class="col-auto"><img src="img/chrome-ex.png" height="220" alt=""></div>
                           <div class="col text-center">
                               <h3 class="f-18 blue-text">Never Miss a Cashback</h3>
                               <p>Add this Chrome Browser Extension to get Cashback Reminders whenever you visit an online store.</p>
                               <a href="" class="btn green">Add Chrome Extension</a>
                           </div></div>
                       </div>
                   </div>
               </div>

            </div>
            <div id="idTab2">
                <div class="d-none d-md-block"><ul class="tab-tab d-flex xs-tab-2">
                    <li><a href="#la1" class="smooth-scroll">Steps to Earn Cashback</a></li>
                    <li><a href="#la2" class="smooth-scroll">Ways to Redeem</a></li>
                    <li><a href="#la3" class="smooth-scroll">Cashback Instructions</a></li>
                    <li><a href="#la4" class="smooth-scroll">How Long it takes?</a></li>
                    <li><a href="#la5" class="smooth-scroll">How to Check Cashback Status</a></li>
                </ul></div>
                <div class="border m-t-15">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">How askmeoffer Cashback works?</div>
                    <div class="blue pad-20">
                        <div style="max-width:900px;" class="m-auto"><div class="row ">
                            <div class="col wow bounceInLeft" data-wow-duration="1s" data-wow-delay="0s"><img src="img/shopper.png" class="img-fluid" alt=""></div>
                            <div class="col wow bounceInLeft" data-wow-duration="1s" data-wow-delay="0.5s"><img src="img/line-1.png" class="img-fluid" alt=""></div>
                            <div class="col wow bounceInLeft" data-wow-duration="1s" data-wow-delay="1s"><img src="img/askmeoffer.png" class="img-fluid" alt=""></div>
                            <div class="col wow bounceInLeft" data-wow-duration="1s" data-wow-delay="1.5s"><img src="img/line-2.png" class="img-fluid" alt=""></div>
                            <div class="col wow bounceInLeft" data-wow-duration="1s" data-wow-delay="2s"><img src="img/store.png" class="img-fluid" alt=""></div>
                        </div></div>
                    </div>
               </div>
               <div class="text-center m-t-20">
                   <a href="" class="btn orange f-20 f-c text-uppercase d-inline-flex align-items-center"><span class="fas fa-sign-in-alt fa-fw m-r-5"></span>Sign Up for free</a>
               </div>
               
               <div class="border m-t-15" id="la1">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Steps to Earn Extra Cashback</div>
                   <div class="pad-15 text-center">
                      <p class="orange-text bold m-b-0">Just visit askmeoffer.com before you shop anywhere</p>
                       <p>Earn Extra Cashback on all your Spending online. Same Products, Same Prices, But Extra Cashback. Follow the simple steps given below to enjoy Extra Cashback everytime you shop online.</p>
                       
                       <div class="row m-b-20 m-t-20 gutters-md"> <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-light-green-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-sign-in-alt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">1.Login</div>
                           Login, Select your Favourite Deal or Store from 300+ online Stores</div>

                          <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-yellow-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-mouse-pointer" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">2.Click & Visit store</div>
                           Click 'Shop Now' or any links to visit the store</div>

                            <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-orange-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-shopping-cart" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">3.Purchase</div>
                           Shop & Buy like you do Normally. The store deliver your Products.</div>
                       <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-blue-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-money-bill-alt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">4.Get Paid</div>
                           We track your order and pay askmeoffer Credits within 6 to 12 weeks</div>
                       
                       
                       </div>
                       
                       
                       <div class="text-center">
                           <a href="" class="btn blue">Cashback FAQs</a>
                       </div>
                       
                   </div>
               </div>
               
               <div class="border m-t-15" id="la2">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Ways to Redeem your Cashback/ Rewards?</div>
                   <div class="pad-15 text-center">
                       <p class="orange-text f-16 bold m-b-0"><span>No Minimum Limit.</span> <span class="m-l-10 m-r-10">Many Redeem Options.</span> <span>No Fee or Charges.</span></p>
                       <p>askmeoffer offers multiple ways to withdraw/redeem your askmeoffer Credits without any additional charges or conditions. You can redeem all your credits and no minimum limit required. Below are the ways you can use your askmeoffer Credits.</p>
                       
                       <div class="row m-b-20 m-t-20 gutters-md"> <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-light-green-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-mobile-alt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Mobile</div>
                           You can Recharge or Pay Bill Instantly. No Minimum Credits Required.</div>

                          <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-red-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-bolt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">DTH</div>
                           You can Recharge Instantly. No Minimum Credits Required.</div>

                            <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-blue-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-building" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Bank Transfer</div>
                          Min 250 Cashback Credits Required. Takes 2 to 5 working days.</div>
                       <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-orange-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-money-bill-alt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Flipkart</div>
                          Buy EGift from Flipkart. Takes 1 working day & Minimum 100 Credits required.</div>
                       <div class="col-12 col-md m-b-20 ">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-purple-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fab fa-amazon" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Amazon</div>
                          Buy EGift from Amazon. Takes 1 working day & Minimum 100 Credits required.</div>
                       
                       
                       </div>
                       
                       
                       <div class="text-center">
                           <a href="" class="btn blue">Sign up for free</a>
                       </div>
                       
                   </div>
               </div>
               <div class="border m-t-15" id="la3">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Instructions & Guidelines to earn Cashback</div>
                   <div class="pad-15"><div class=" d-flex justify-content-center">
                       <div class=""><ul class="list-unstyled f-16 m-b-20">
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-sign-in-alt f-23"></i>
                                </div> Please login, Click our Links to visit the store & Place order</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-shopping-cart f-23"></i>
                                </div> Add products to cart only after you visit the store (click out) from askmeoffer.com</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-gift f-23"></i>
                                </div> Do not use Gift Voucher or Wallet Amount or Loyalty Points to place your order</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-clone f-23"></i>
                                </div> After Click out, place order in one session without visiting other websites</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-mouse-pointer f-23"></i>
                                </div> You must do a Click out from askmeoffer everytime you place a new order</li>
                            <li class=" m-b-10">
                                <div class="circle-stroke f-40 blue-border blue-text m-r-5"><i class="fas fa-credit-card f-23"></i>
                                </div> If payment fails in first attempt, please empty cart & click out again & place order</li>
                            <li class=" red-text m-b-10">
                                <div class="circle-stroke red-border f-40 m-r-5"><i class="fas fa-ban f-23"></i>
                                </div> Do not use Ad blockers/ browser extensions that would block tracking your orders</li>
                        </ul>
                        
                       <div class="m-l-20 grey-text"><ul class="list">
                        <li>Please follow specific Cashback Terms given by the store</li>
                       <li>Cashback not available if you return, replace or exchange any item of your order</li>
                       <li>You must do a Click out from askmeoffer everytime you place a new order</li>
                        </ul>
                        <ul class="list">
                       <li>Please wait until askmeoffer pages loaded fully before Clicking out to the store</li>
                       <li>Its advised to clear your browsing history before your visit the store from askmeoffer.</li>
                       <li>If payment fails in the first attempt, please empty cart & click out again & place order</li>
                        </ul></div>
                  </div>
                   </div>
                   
                   <p class="text-center grey-text">Please Note: We do not guarantee Cashback if tracking your order is failed because of any human error like customer did not follow the Cashback Instructions correctly or Technical problems like your browser has malicious extensions/ plugins which manipulate URLs or block tracking.</p></div>
               </div>
               
               <div class="border m-t-15" id="la4">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">How long it takes to Earn Cashback?</div>
                   <div class="pad-15">
                      
                      <div class="row text-center justify-content-center m-b-20"><div class="col-md-8 row align-items-center">
                        <div class="col-12 col-md m-b-20 ">
                            <div class="circle f-20 green white-text mr-auto ml-auto m-b-10">
                                <div class="fas fa-mouse-pointer"></div>
                            </div>
                            Click
                        </div>
                        <div class="col-12 col-md m-b-20 ">
                            <strong>3 days</strong> <hr class="m-t-5 m-b-5 d-none d-md-block"> Trackings
                        </div>
                        <div class="col-12 col-md m-b-20 ">
                            <div class="circle f-20 green white-text mr-auto ml-auto m-b-10">
                                <div class="fas fa-redo"></div>
                            </div>
                            Visit store
                        </div>
                        <div class="col-12 col-md m-b-20 ">
                            <strong>6 - 12 Weeks</strong> <hr class="m-t-5 m-b-5 d-none d-md-block"> Estimated payments
                        </div>
                        <div class="col-12 col-md m-b-20 ">
                            <div class="circle f-20 green white-text mr-auto ml-auto m-b-10">
                                <div class="fas fa-check"></div>
                            </div>
                            Purchase
                        </div>
                    </div></div>
                      
                       <div class="grey-text"><h4>Why it takes 6 to 12 weeks?</h4>

<p>We approve Cashback once we receive Confirmation Report from store(s) with a list of Valid Orders. stores pay us commission only on orders that are valid i.e. the orders which are not returned/ replaced/ exchanged. For this reason, stores wait until the Return period is over i.e. usually one month. Then at the end of next month they pick up all the orders, validate and send us the list of valid orders.
</p>
<p>for example: Customer place order in July. Then store starts validation of all july orders at the end of August. Then send us the Report within 1 to 2 weeks i.e. 2nd week of September. Then we start approving cashback. So the orders placed in July gets cashback approved in September. Its possible we may pay cashback a week early or a week later depending on the store.
</p>
<h4>What is Estimated Payment Date?</h4>

<p>When a Customer Order is tracked, We give Estimated Payment Date for each transaction based on our forecast. We pay Cashback usually before the Estimated Payment Date. You can check Estimated Payment Date for each transaction in your Dashboard.</p></div>
                   </div>
               </div>
               
               <div class="border m-t-15" id="la5">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">How To Check Cashback Status?</div>
                   <div class="pad-15 text-center">
                       <p>Login, Select "Cashback Activity" option from menu, then you can see your "Store visits" or "Tracked Transactions".</p>
                       <img src="img/cashback-status.jpg" alt="" class="img-fluid">
                   </div>
                </div>
                <div class="border m-t-15">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Ways to Redeem Cashback?</div>
                   <div class="pad-15 text-center">
                       <p>Login, Select "Credits & Redeem" option from menu, then select "Recharge/ PayBills" or "E-Gift", fill the form and submit.</p>
                       <img src="img/redeem-1.jpg" alt="" class="img-fluid">
                   </div>
                </div>
                
            </div>
            
            <div id="idTab3">
                <div class="pad-15 border">
                   
                    <div class="row m-b-20 justify-content-center blue-text f-20">
                        <div class="col-auto text-center"><div class="fas fa-circle f-30"></div> <div>1 Credit</div></div>
                        <div class="col-auto">=</div>
                            <div class="col-auto text-center"><div class="fas fa-rupee-sign f-30"></div> <div>1 Rupee</div></div>
                    </div>
                   
                    <p>askmeoffer Credits are like Currency or Money; 1 Credit is equal to 1 Indian Rupee. Therefore, the number of Credits you have in your askmeoffer Account is equivalent to the same amount in Indian Rupees. You can use them to Recharge Mobile/DTH, Pay Bills or Buy e-Gift Vouchers from Flipkart, Amazon etc stores or do a Bank Transfer. Please note Credits earned on Flipkart, Snapdeal shopping can not be transferred to Bank Account. The Credits earned from stores that allow cashback can be transferred to your Bank account.</p>
                </div>
                
                <div class="border m-t-15">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Ways You can Earn askmeoffer Credits</div>
                   <div class="pad-15 ">
                       <p class="text-center">Overall you can Earn askmeoffer Credits in two ways. 1. Do your Shopping via askmeoffer.com 2. Contribute and Help other Shoppers on our website.</p>
                       
                       <h3 class="text-center blue-text">1. Do your Shopping via askmeoffer.com & Earn</h3>
                       
                       
                       <div class="m-b-30"><a href="" class="black-text hover-no-deco m-auto d-block" style="max-width: 680px;"><div class="row no-gutters border">
                           <div class="col-auto ">
                               <div class="pad-15 grey-lighter-2 h-100"><div class="circle f-20 green white-text mr-auto ml-auto">
                                <div class="fas fa-shopping-cart"></div>
                                </div></div>
                           </div>
                           <div class="col">
                               <div class="pad-15"><div class="f-16 w-700">Shop at 300+ Online Stores</div>

<div class="f-13">Find your favourite store, See the Cashback Rates offered by the store, Click 'Shop Now' & Visit the Store and Purchase. We track your order and pay Cashback.</div></div>
                           </div>
                           </div>
                       </a></div>
                       
                       <h3 class="text-center blue-text">2. Contribute & Earn</h3>
                       <div class="row">
                       <div class="col-md col-12 sm-m-b-10"><a href="" class="black-text hover-no-deco m-auto d-block" style="max-width: 680px;"><div class="row no-gutters border">
                           <div class="col-auto ">
                               <div class="pad-15 grey-lighter-2 h-100"><div class="circle f-20 red white-text mr-auto ml-auto">
                                <div class="fas fa-users"></div>
                                </div></div>
                           </div>
                           <div class="col">
                               <div class="pad-15"><div class="f-16 w-700">Refer Friends & Earn</div>

<div class="f-13">Invite your Friends/ followers and Earn 100 Appreciation Credits for each Friend joins.</div></div>
                           </div>
                           </div>
                       </a></div>
                       <div class="col-md col-12"><a href="" class="black-text hover-no-deco m-auto d-block" style="max-width: 680px;"><div class="row no-gutters border">
                           <div class="col-auto ">
                               <div class="pad-15 grey-lighter-2 h-100"><div class="circle f-20 blue white-text mr-auto ml-auto">
                                <div class="fas fa-share-alt"></div>
                                </div></div>
                           </div>
                           <div class="col">
                               <div class="pad-15"><div class="f-16 w-700">Share Coupons, Feedback & Earn</div>

<div class="f-13">Post a Discount Coupon/ Offer, a Freebie or Share your Feedback/ suggestions and win upto 50 Credits.</div></div>
                           </div>
                           </div>
                       </a></div>
                       </div>
                   </div>
                </div>
                
                <div class="border m-t-15">
                   <div class="pad-10 text-center f-c f-18 bg-color-grey-50 border-bottom">Ways to Redeem your Cashback/ Rewards?</div>
                   <div class="pad-15 text-center">
                       <p class="orange-text f-16 bold m-b-0"><span>No Minimum Limit.</span> <span class="m-l-10 m-r-10">Many Redeem Options.</span> <span>No Fee or Charges.</span></p>
                       <p>askmeoffer offers multiple ways to withdraw/redeem your askmeoffer Credits without any additional charges or conditions. You can redeem all your credits and no minimum limit required. Below are the ways you can use your askmeoffer Credits.</p>
                       
                       <div class="row m-b-20 m-t-20 gutters-md"> <div class="col-12 col-md sm-m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-light-green-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-sign-in-alt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Mobile</div>
                           You can Recharge or Pay Bill Instantly. No Minimum Credits Required.</div>

                          <div class="col-12 col-md sm-m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-yellow-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-mouse-pointer" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">DTH</div>
                           You can Recharge Instantly. No Minimum Credits Required.</div>

                            <div class="col-12 col-md sm-m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-orange-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-shopping-cart" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Bank Transfer</div>
                          Min 250 Cashback Credits Required. Takes 2 to 5 working days.</div>
                       <div class="col-12 col-md sm-m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-blue-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fas fa-money-bill-alt" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Flipkart</div>
                          Buy EGift from Flipkart. Takes 1 working day & Minimum 100 Credits required.</div>
                       <div class="col-12 col-md sm-m-b-20">
                          <span class="fa-layers fa-fw f-50 m-b-20">
                            <i class="fas fa-circle color-orange-700" data-fa-transform="grow-6"></i>
                            <i class="fa-inverse fab fa-amazon" data-fa-transform="shrink-6"></i>
                          </span>
                          <div class="f-16 f-c blue-text">Amazon</div>
                          Buy EGift from Amazon. Takes 1 working day & Minimum 100 Credits required.</div>
                       
                       
                       </div>
                       
                       
                       <div class="text-center">
                           <a href="" class="btn blue">Sign up for free</a>
                       </div>
                       
                   </div>
               </div>
            </div>
            
            <div id="idTab4">
                <div class="row">
                    <div class="col">
                        <div id="faqC">
                        
                        <h2 class="blue-text f-22 f-c w-400">Online Cashback Explained</h2>
                         <?php for($i=0;$i<8;$i++){?>
                          <div class="card">
                          <div class="f-15 border border-bottom-0 pad-15 collapsed" data-toggle="collapse" data-target="#faq-<?php echo $i; ?>"><div class="row"><div class="col-auto"><i class="fas fa-fw fa-angle-down "></i></div> <div class="col"> What is Cashback?</div></div></div>
                         <div class="collapse" id="faq-<?php echo $i; ?>" data-parent="#faqC"><div class="pad-15 border bg-color-grey-100">Cashback is the Extra Money that askmeoffer.com gives to its users when they visit store via us & make a Purchase. It is the Extra Cash you can earn from askmeoffer.com on top of any Sale/ Discounts the store may have already declared on a particular product/ service.</div></div>
                            </div>
                          <?php } ?> 
                          <div class="border-top m-b-20"></div>
                           <h2 class="blue-text f-22 f-c w-400">Best Practices to Earn Cashback/ Extra Rewards</h2>
                         <?php for($i=8;$i<12;$i++){?>
                          <div class="card">
                          <div class="f-15 border border-bottom-0 pad-15 collapsed" data-toggle="collapse" data-target="#faq-<?php echo $i; ?>"><div class="row"><div class="col-auto"><i class="fas fa-fw fa-angle-down "></i></div> <div class="col">Must Check Instructions & Guidelines</div></div></div>
                         <div class="collapse" id="faq-<?php echo $i; ?>" data-parent="#faqC"><div class="pad-15 border bg-color-grey-100">Cashback is the Extra Money that askmeoffer.com gives to its users when they visit store via us & make a Purchase. It is the Extra Cash you can earn from askmeoffer.com on top of any Sale/ Discounts the store may have already declared on a particular product/ service.</div></div>
                            </div>
                          <?php } ?>  
                        <div class="border-top m-b-20"></div>
                        <h2 class="blue-text f-22 f-c w-400">Cashback Tracking Explained</h2>
                         <?php for($i=12;$i<22;$i++){?>
                          <div class="card">
                          <div class="f-15 border border-bottom-0 pad-15 collapsed" data-toggle="collapse" data-target="#faq-<?php echo $i; ?>"><div class="row"><div class="col-auto"><i class="fas fa-fw fa-angle-down "></i></div> <div class="col">What is a Click out and Cashback Record?</div></div></div>
                         <div class="collapse" id="faq-<?php echo $i; ?>" data-parent="#faqC"><div class="pad-15 border bg-color-grey-100">Cashback is the Extra Money that askmeoffer.com gives to its users when they visit store via us & make a Purchase. It is the Extra Cash you can earn from askmeoffer.com on top of any Sale/ Discounts the store may have already declared on a particular product/ service.</div></div>
                            </div>
                          <?php } ?>  
                        <div class="border-top m-b-20"></div>
                        </div>
                    </div>
                    <div class="col-md-auto col-12">
                        <div class="sidebar">
                            <div class="border m-b-20"><ul class="list-bordered ">
                            <li><a href="">Online Cashback Explained</a></li>
                            <li><a href="">Best Practices to Earn Cashback/ Rewards</a></li>
                            <li><a href="">Cashback Tracking Explained</a></li>
                            <li><a href="">Cashback Approval & Payment</a></li>
                            <li><a href="">Tracking Missing & Cancellations</a></li>
                            </ul></div>
                            <div class="border m-b-20">
                                <div class="pad-10 bold border-bottom">Earn Extra Cashback/ Rewards</div>
                                
                                <a href="" class="pad-10 d-block border-bottom hover-no-deco">
                                    <div class="row"><div class="col-auto">
                                        <i class="fas fa-globe fa-fw f-30 m-t-5 l-1-0"></i>
                                    </div>
                                    <div class="col">
                                        <div class="bold">Login, Browse</div> <div class="grey-text f-12">Select your Favourite <br>Store or Deal</div>
                                    </div></div>
                                </a>
                                <a href="" class="pad-10 d-block border-bottom hover-no-deco">
                                    <div class="row"><div class="col-auto">
                                        <i class="fas fa-shopping-cart fa-fw f-30 m-t-5 l-1-0"></i>
                                    </div>
                                    <div class="col">
                                        <div class="bold">Click, Visit & Shop</div> <div class="grey-text f-12">Click our links to visit the store & Buy like you do Normally.</div>
                                    </div></div>
                                </a>
                                <a href="" class="pad-10 d-block border-bottom hover-no-deco">
                                    <div class="row"><div class="col-auto">
                                        <i class="fas fa-rupee-sign fa-fw f-30 m-t-5 l-1-0"></i>
                                    </div>
                                    <div class="col">
                                        <div class="bold">Get Cashback paid</div> <div class="grey-text f-12">We track your order and pay askmeoffer Credits in 6 to 12 weeks</div>
                                    </div></div>
                                </a>
                                
                                
                                <div class="text-center grey-text pad-10 f-12">
                                    <i class="far fa-check-circle m-r-5"></i>Same Prices  <i class="far fa-check-circle m-l-10 m-r-5"></i>Same Products<br>  <i class="far fa-check-circle m-l-10 m-r-5"></i>But Extra Cashback/ Rewards
                                </div>
                                
                            </div>
                            
                            <div class="border m-b-20">
                                 <div class="pad-10 bold border-bottom">Popular Cashback Stores</div>
                                 
                                 <a href="" class="d-block hover-no-deco pad-10 border-bottom black-text">
                                     <div class="row"><div class="col"><div class="bold blue-text">Flipkart Offers</div>
                                         <div class="f-12">Upto 10% Rewards</div></div>
                                     
                                     <div class="col-auto align-self-center"><i class="fas fa-angle-right color-grey-400"></i></div></div>
                                 </a>
                                 <a href="" class="d-block hover-no-deco pad-10 border-bottom black-text">
                                     <div class="row"><div class="col"><div class="bold blue-text">Amazon India Offers</div>
                                         <div class="f-12">upto 10% Rewards</div></div>
                                     
                                     <div class="col-auto align-self-center"><i class="fas fa-angle-right color-grey-400"></i></div></div>
                                 </a>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="idTab5">
                <div class="row">
                    <div class="col">
                        <div class="content-body"><p>On top of the Extra Cashback/ Rewards askmeoffer offer, we have introduced Appreciation Credits Program for our valued customers. This is to offer Additional Benefits to Loyal askmeoffer Customers who Buy things online often via askmeoffer App/ Website, who Invite Friends to use askmeoffer.</p>

<h3>How to use Appreciation Credits:</h3>
<p>You need to convert Appreciation Credits to askmeoffer Credits before using them. Once converted, you can use them like normal askmeoffer Credits..
</p>
<h3>How to Convert Appreciation Credits to askmeoffer Credits?</h3>
<p>Earn Extra Cashback (askmeoffer Credits) Rs.10 or more on your Shopping at our designated stores. Then 10 of your Appreciation Credits will be Converted to real askmeoffer Credits.</p>
<p>Your Appreciation Credits get converted to real askmeoffer Credits as soon as your Cashback (/ Reward) transaction is approved.
You can convert only 10 Appreciation Credits for each Cashback Transaction. You will find 'Appreciation Credits' icon  on each designated store page on askmeoffer.com website.</p>
<p class="text-center f-22 orange-text f-c">Use askmeoffer More, Invite More. Earn More & More.</p>

<h3>Ways You can get Appreciation Credits:</h3>
<p>You can earn Appreciation Credits in many ways: Important ones listed below.</p>

<p>1. Shop more, Earn More - askmeoffer Admin will recognise Loyal Customers who regularly shop more via askmeoffer and Add Appreciation Credits directly to their account for being our esteemed customer. The more you shop via askmeoffer, there is a possibility that you will win more Appreciation Credits.</p>

<p>2. Referral Sign Up – Share your Unique Referral Code with your friends on Social Media. Let them Signup with askmeoffer & use your Referral Code. Both you and your friends get Appreciation Credits.</p>

<p>There are other ways you can earn Appreciation Credits, by using our Promo Codes, by sharing our links etc.</p>

<div class="grey-text f-12"><strong>Terms:</strong>
                            <ul>
<li>askmeoffer has the right to debit credits or even suspend the user account if found misusing the system to earn extra credits by wrong means which could damage askmeoffer in any manner.</li>
                            <li>askmeoffer reserves the right to withdraw the Appreciation Credits Program at any time, without giving any prior notice.</li>
                            
</ul></div>
              <hr>    
                  <p class="text-center f-12">Please send us your feedback on this to <a href="#">support@askmeoffer.com</a> and help us improve our service. Thanks.</p>
                   
                   </div>
                    </div>
                    <div class="col-md-auto col-12">
                        <div class="sidebar">
                            
                           
                           
                            <div class="border m-b-20">
                                <div class="pad-10 bold border-bottom">Earn Extra Cashback/ Rewards</div>
                                
                                <a href="" class="pad-10 d-block border-bottom hover-no-deco">
                                    <div class="row"><div class="col-auto">
                                        <i class="fas fa-globe fa-fw f-30 m-t-5 l-1-0"></i>
                                    </div>
                                    <div class="col">
                                        <div class="bold">Login, Browse</div> <div class="grey-text f-12">Select your Favourite <br>Store or Deal</div>
                                    </div></div>
                                </a>
                                <a href="" class="pad-10 d-block border-bottom hover-no-deco">
                                    <div class="row"><div class="col-auto">
                                        <i class="fas fa-shopping-cart fa-fw f-30 m-t-5 l-1-0"></i>
                                    </div>
                                    <div class="col">
                                        <div class="bold">Click, Visit & Shop</div> <div class="grey-text f-12">Click our links to visit the store & Buy like you do Normally.</div>
                                    </div></div>
                                </a>
                                <a href="" class="pad-10 d-block border-bottom hover-no-deco">
                                    <div class="row"><div class="col-auto">
                                        <i class="fas fa-rupee-sign fa-fw f-30 m-t-5 l-1-0"></i>
                                    </div>
                                    <div class="col">
                                        <div class="bold">Get Cashback paid</div> <div class="grey-text f-12">We track your order and pay askmeoffer Credits in 6 to 12 weeks</div>
                                    </div></div>
                                </a>
                                
                                
                                <div class="text-center grey-text pad-10 f-12">
                                    <i class="far fa-check-circle m-r-5"></i>Same Prices  <i class="far fa-check-circle m-l-10 m-r-5"></i>Same Products<br>  <i class="far fa-check-circle m-l-10 m-r-5"></i>But Extra Cashback/ Rewards
                                </div>
                                
                            </div>
                            
                            <div class="border m-b-20">
                                 <div class="pad-10 bold border-bottom">Popular Cashback Stores</div>
                                 
                                 <a href="" class="d-block hover-no-deco pad-10 border-bottom black-text">
                                     <div class="row"><div class="col"><div class="bold blue-text">Flipkart Offers</div>
                                         <div class="f-12">Upto 10% Rewards</div></div>
                                     
                                     <div class="col-auto align-self-center"><i class="fas fa-angle-right color-grey-400"></i></div></div>
                                 </a>
                                 <a href="" class="d-block hover-no-deco pad-10 border-bottom black-text">
                                     <div class="row"><div class="col"><div class="bold blue-text">Amazon India Offers</div>
                                         <div class="f-12">upto 10% Rewards</div></div>
                                     
                                     <div class="col-auto align-self-center"><i class="fas fa-angle-right color-grey-400"></i></div></div>
                                 </a>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="idTab6">
                <div class="row">
                    <div class="col sm-m-b-20">
                       <div style="max-width:700px;">
                        <h2>Contact Us/ Leave Feedback</h2>
                        <div class="row m-b-10"><div class="col-md-3 text-right">Your Name</div><div class="col"><input type="text" class="input w-100"></div></div>
                        <div class="row m-b-10"><div class="col-md-3 text-right">Email</div><div class="col"><input type="text" class="input w-100"></div></div>
                        <div class="row m-b-10"><div class="col-md-3 text-right">Your Phone Number</div><div class="col d-flex align-items-center no-gutters"><div class="col-auto"><div class="btn grey-lighter-2 m-r-10">+91</div></div><div class="col"><input type="text" class="input w-100"></div></div></div>
                        <div class="row m-b-10"><div class="col-md-3 text-right">Subject</div><div class="col"><input type="text" class="input w-100"></div></div>
                           <div class="row m-b-10"><div class="col-md-3 text-right">Message</div><div class="col"><textarea rows="5" class="input w-100"></textarea></div></div>
                           <div class="row m-b-10 align-items-center"><div class="col-md-3 text-right">Security Code</div><div class="col-auto"><em>Five plus twelve =</em></div><div class="col"><input type="text" class="input w-100"></div></div>
                           <div class="text-right">
                               <input type="submit" class="btn blue" value="Send message">
                           </div>
                        </div>
                    </div>
                    <div class="col-md-auto col-12">
                        <div class="sidebar" style="min-width: 250px">
                            <div class="border pad-10 text-center m-b-20 rounded">
                                <div class="f-16 blue-text bold">Call Us</div>
                                <div class="f-16">040 - 6512 6512</div>
                                <div class="f-12">Monday to Saturday</div>
                                <div class="f-12">9am to 8pm</div>
                            </div>
                            <div class="border pad-10 text-center m-b-20 rounded">
                                <div class="f-16 blue-text bold">Email Us</div>
                                <div class="f-12">For any questions or feedback</div>
                                <div class="f-12">please send us an email to</div>
                                <div class="f-16"><a href="">support@askmeoffer.com</a></div>
                            </div>
                            <div class="border pad-10 text-center m-b-20 rounded">
                                <div class="f-16 blue-text bold">Our Address</div>
                                <div class="f-12">Flat No.101, Srinivasa Plaza<br>
                                    Sri Nagar Colony Main Road<br>
                                    Punjagutta, Hyderabad - 500073<br>
                                    Telangana, India.</div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="text-center m-t-20">
                   <a href="" class="btn orange f-20 f-c text-uppercase d-inline-flex align-items-center"><span class="fas fa-sign-in-alt fa-fw m-r-5"></span>Sign Up for free</a>
               </div>
        </div>
        
        </div>
    </div>
    <?php include('includes/footer.php'); ?>
    <!-- Modal -->
     <?php include('includes/lang-list.php'); ?>
     <?php include('includes/login-pop.php'); ?>
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/fontawesome-all.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/flickity.pkgd.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script>
      new WOW().init();
    </script>
</body>

</html>